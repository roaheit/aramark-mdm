<%@page import="com.orchestranetworks.ps.workflow.WorkflowLauncher"%>
<%@page import="com.aramark.mdm.common.constants.AramarkWorkflowConstants"%>
<%@page import="com.aramark.mdm.user.path.UserPaths"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	(new WorkflowLauncher()).execute(request, AramarkWorkflowConstants.WF_KEY_USER_MAINTAIN_MASTER_DATA, UserPaths._Root_User.getPathInSchema().format(), null);
%>
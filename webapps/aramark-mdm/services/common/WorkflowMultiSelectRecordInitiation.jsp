<%@page import="com.orchestranetworks.ps.accessrule.*"%>
<%@page import="com.orchestranetworks.ps.multiselect.*"%>
<%@page import="com.aramark.mdm.vndr.path.*"%>
<%@page import="com.orchestranetworks.ps.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%
	TrackingInfoHelper trackingInfoHelper = new FirstSegmentTrackingInfoHelper(
		WorkflowAccessRule.SEGMENT_WORKFLOW_PERMISSIONS_USERS);
	(new MultiSelectRecordInitiationService(trackingInfoHelper, VendorPaths._Root_VendorDetails_VendorItemCatalogGroup.getPathInSchema())).service(request, response);
%>
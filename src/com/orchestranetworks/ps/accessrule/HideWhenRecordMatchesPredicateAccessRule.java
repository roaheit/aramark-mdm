/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.accessrule;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Session;

/**
 * Use this rule to hide a field within a create context.
 */
public class HideWhenRecordMatchesPredicateAccessRule implements AccessRule {
	private final String predicate;
	private final String sessionKey;
	private final String sessionValue;
	private final boolean match;

	public HideWhenRecordMatchesPredicateAccessRule(String predicate) {
		this(predicate, null, null, true);
	}

	public HideWhenRecordMatchesPredicateAccessRule(String predicate, String sessionKey, String sessionValue,
			boolean match) {
		super();
		this.predicate = predicate;
		this.sessionKey = sessionKey;
		this.sessionValue = sessionValue;
		this.match = match;
	}

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node) {
		boolean hideNonNull = node.getDefaultValue() != null;
		if (adaptation.isTableOccurrence() && (adaptation.get(node) == null || hideNonNull)
				&& adaptation.matches(predicate)) {
			return AccessPermission.getHidden();
		} else if (adaptation.isSchemaInstance() && sessionKey != null) {
			Object val = session.getAttribute(sessionKey);
			if (val != null && match == val.equals(sessionValue))
				return AccessPermission.getHidden();
		}
		return AccessPermission.getReadWrite();
	}
}

package com.orchestranetworks.ps.accessrule;

import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.constants.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;

/**
 * Almost the opposite of HideOnCreationAccessRule, this rule ensures that some fields are only
 * read-write during creation and are otherwise read-only or hidden. The 'otherwise' permission is configurable
 * using an argument to the constructor.  The no-argument constructor uses read-only.
 *
 * @see HideOnCreationAccessRule
 * @author MCH
 */
public class ReadWriteForNewRecordOnlyAccessRule implements AccessRule
{
	/** The role giving permission despite the rule */
	private Role permissiveRole;

	protected AccessPermission notNewRecordPermission;

	public ReadWriteForNewRecordOnlyAccessRule()
	{
		this(AccessPermission.getReadOnly(), CommonConstants.TECH_ADMIN);
	}

	public ReadWriteForNewRecordOnlyAccessRule(final AccessPermission notNewRecordPermission)
	{
		this(notNewRecordPermission, CommonConstants.TECH_ADMIN);
	}

	public ReadWriteForNewRecordOnlyAccessRule(final Role permissiveRole)
	{
		this(AccessPermission.getReadOnly(), permissiveRole);
	}

	public ReadWriteForNewRecordOnlyAccessRule(
		final AccessPermission notNewRecordPermission,
		final Role permissiveRole)
	{
		this.notNewRecordPermission = notNewRecordPermission;
		this.permissiveRole = permissiveRole;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.orchestranetworks.service.AccessRule#getPermission(com.onwbp.adaptation
	 * .Adaptation, com.orchestranetworks.service.Session,
	 * com.orchestranetworks.schema.SchemaNode)
	 */
	@Override
	public AccessPermission getPermission(
		final Adaptation pAdaptation,
		final Session pSession,
		final SchemaNode pNode)
	{
		// If it's a new record or viewing on a table (is schema instance for both cases),
		// or if it's history, or if you're in a permissive role, then return RW
		if (pAdaptation.isSchemaInstance() || pAdaptation.isHistory()
			|| (this.permissiveRole != null && pSession.isUserInRole(this.permissiveRole)))
		{
			return AccessPermission.getReadWrite();
		}
		return this.notNewRecordPermission;
	}

	public Role getPermissiveRole()
	{
		return this.permissiveRole;
	}

	public void setPermissiveRole(final Role permissiveRole)
	{
		this.permissiveRole = permissiveRole;
	}
}

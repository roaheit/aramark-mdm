package com.orchestranetworks.ps.accessrule;

import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.constants.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;

//import com.orchestranetworks.service.directory.*;

/**
 * This rule ensures that some fields are only
 * read-write unitl assigned and are otherwise read-only.
 * Note: the tech-admin role will always have read-write access.
 */
public class ReadWriteUntilValueAssignedAccessRule implements AccessRule
{

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node)
	{
		// Will be data set when called for column permissions
		// or a record not yet saved
		if (adaptation.isSchemaInstance() || adaptation.isHistory())
		{
			return AccessPermission.getReadWrite();
		}
		if (isUserAlwaysReadWrite(session))
		{
			return AccessPermission.getReadWrite();
		}
		else if (adaptation.get(node.getPathInAdaptation()) == null)
		{
			return AccessPermission.getReadWrite();
		}
		else
		{
			return AccessPermission.getReadOnly();
		}
	}

	protected boolean isUserAlwaysReadWrite(Session session)
	{
		return session.isUserInRole(CommonConstants.TECH_ADMIN);
	}
}

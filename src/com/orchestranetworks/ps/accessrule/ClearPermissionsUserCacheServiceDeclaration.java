package com.orchestranetworks.ps.accessrule;

import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;

public class ClearPermissionsUserCacheServiceDeclaration extends TechAdminOnlyServiceDeclaration
{

	public ClearPermissionsUserCacheServiceDeclaration(String moduleName)
	{
		super(
			moduleName == null ? ServiceKey.forName("ClearPermissionsUserCache")
				: ServiceKey.forModuleServiceName(moduleName, "ClearPermissionsUserCache"),
			null,
			"Clear Permissions User Cache",
			null,
			"");
	}

	@Override
	public UserService<DataspaceEntitySelection> createUserService()
	{
		return new UserServiceNoUI<>(new ClearPermissionsUserCacheUserService());
	}

}

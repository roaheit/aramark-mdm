/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.accessrule;

import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;

public class ClearPermissionsUserCacheUserService
	extends
	AbstractUserService<DataspaceEntitySelection>
{

	@Override
	public void execute(Session session) throws OperationException
	{
		if (!session.isUserInRole(Role.ADMINISTRATOR))
		{
			throw OperationException
				.createError("User doesn't have permission to execute service.");
		}

		DefaultPermissionsUserManager.getInstance().clearCache();
	}
}

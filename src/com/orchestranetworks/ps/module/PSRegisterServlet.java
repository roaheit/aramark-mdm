package com.orchestranetworks.ps.module;

import java.util.*;

import com.onwbp.base.text.*;
import com.orchestranetworks.module.*;
import com.orchestranetworks.ps.accessrule.*;
import com.orchestranetworks.ps.admin.cleanworkflows.*;
import com.orchestranetworks.ps.admin.devartifacts.*;
import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.ps.validation.service.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.userservice.declaration.*;

/**
 * A servlet that registers standard Orchestra Networks Professional Services (PS) services, such as Dev Artifacts.
 */
public class PSRegisterServlet extends ModuleRegistrationServlet
{
	private static final long serialVersionUID = 1L;
	private static final String ADMIN_GROUP_DESCRIPTION = "Dev artifacts and other deployment services";

	public static final String ADMIN_GROUP = "Admin";

	public final String moduleName;

	/**
	 * Create the servlet with the given module name
	 * 
	 * @param moduleName the module name
	 */
	public PSRegisterServlet(String moduleName)
	{
		this.moduleName = moduleName;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void handleServiceRegistration(ModuleServiceRegistrationContext aContext)
	{
		super.handleServiceRegistration(aContext);

		// Register the Admin group
		registerServiceGroup(aContext);

		// Create an array and initialize it with the ps services
		ArrayList<UserServiceDeclaration> serviceDeclarations = new ArrayList<>();
		initServiceDeclarations(serviceDeclarations);

		// Reg?ister each service
		for (UserServiceDeclaration serviceDeclaration : serviceDeclarations)
		{
			aContext.registerUserService(serviceDeclaration);
		}
	}

	/**
	 * Register the service group that the services will belong to.
	 * This registers an Admin group with a standard description,
	 * but can be overridden in order to register a different group or none at all.
	 * 
	 * @param aContext the context
	 */
	protected void registerServiceGroup(ModuleServiceRegistrationContext aContext)
	{
		aContext.registerServiceGroup(
			ServiceGroupKey.forServiceGroupInModule(moduleName, ADMIN_GROUP),
			UserMessage.createInfo(ADMIN_GROUP),
			UserMessage.createInfo(ADMIN_GROUP_DESCRIPTION));
	}

	/**
	 * Initialize the given list with the ps service declarations to register.
	 * This adds all the standard services, but can be overridden to remove certain services,
	 * or configure them differently, or add new ones.
	 * 
	 * @param serviceDeclarations the list of service declarations
	 */
	@SuppressWarnings("rawtypes")
	protected void initServiceDeclarations(List<UserServiceDeclaration> serviceDeclarations)
	{
		serviceDeclarations.add(new ImportDevArtifactsDSDeclaration(moduleName));
		serviceDeclarations.add(new ExportDevArtifactsDSDeclaration(moduleName));
		serviceDeclarations.add(new CleanWorkflowsServiceDeclaration(moduleName));
		serviceDeclarations.add(new ClearPermissionsUserCacheServiceDeclaration(moduleName));
		serviceDeclarations.add(new GenerateDataDictionaryDeclaration(moduleName));
		serviceDeclarations
			.add(new GenerateValidationReportDeclaration(moduleName, Severity.WARNING));
		serviceDeclarations.add(new GenerateTableCompareReportDeclaration(moduleName));
		serviceDeclarations.add(new GenerateDataSetCompareReportDeclaration(moduleName));
		serviceDeclarations.add(new GenerateDataSpaceCompareReportDeclaration(moduleName));
		serviceDeclarations.add(new InitializeDefaultFieldValuesDeclaration(moduleName));
	}
}
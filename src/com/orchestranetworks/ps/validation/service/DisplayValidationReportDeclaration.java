package com.orchestranetworks.ps.validation.service;

import com.onwbp.base.text.Severity;
import com.orchestranetworks.ps.service.GenericServiceDeclaration;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.DatasetEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnDataset;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;

public class DisplayValidationReportDeclaration
		extends GenericServiceDeclaration<DatasetEntitySelection, ActivationContextOnDataset>
		implements UserServiceDeclaration.OnDataset {

	private Severity severity;

	public DisplayValidationReportDeclaration(String moduleName, Severity severity) {
		super(ServiceKey.forModuleServiceName(moduleName, "DisplayValidationReport"), null, "Display Validation Report",
				"Display all data set validation messages over the min severity threshold", null);
		this.severity = severity;
	}

	@Override
	public UserService<DatasetEntitySelection> createUserService() {
		return new DisplayValidationReport(GenerateValidationReport.DEFAULT_EXPORT_DIR_NAME, true, true, severity);
	}

}

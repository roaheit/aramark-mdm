package com.orchestranetworks.ps.validation.service;

import com.onwbp.base.text.Severity;
import com.orchestranetworks.ps.service.GenericServiceDeclaration;
import com.orchestranetworks.service.ServiceKey;
import com.orchestranetworks.ui.selection.DatasetEntitySelection;
import com.orchestranetworks.userservice.UserService;
import com.orchestranetworks.userservice.declaration.ActivationContextOnDataset;
import com.orchestranetworks.userservice.declaration.UserServiceDeclaration;

public class GenerateValidationReportDeclaration
		extends GenericServiceDeclaration<DatasetEntitySelection, ActivationContextOnDataset>
		implements UserServiceDeclaration.OnDataset {

	private Severity severity;

	public GenerateValidationReportDeclaration(String moduleName, Severity severity) {
		super(ServiceKey.forModuleServiceName(moduleName, "GenerateValidationReport"), null,
				"Generate Validation Report",
				"Download a CSV file with all data set validation messages over the min severity threshold", null);
		this.severity = severity;
	}

	@Override
	public UserService<DatasetEntitySelection> createUserService() {
		return new GenerateValidationReport(GenerateValidationReport.DEFAULT_EXPORT_DIR_NAME, true, true, severity,
				true);
	}

}

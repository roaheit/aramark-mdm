package com.orchestranetworks.ps.validation.service;

import java.io.*;
import java.text.*;
import java.util.*;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.constants.*;
import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.ps.validation.bean.*;
import com.orchestranetworks.ps.validation.export.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;

public class GenerateValidationReport extends AbstractUserService<DatasetEntitySelection>
{
	public static final String DEFAULT_EXPORT_DIR_NAME = "validation";

	private static final String CSV_SEPARATOR = ",";
	private static final String DEFAULT_PERMISSIONS_TEMPLATE_DATA_SPACE_NAME = "ValidationDataSpacePermissions";

	protected String exportParentDirName;
	protected String exportDirName;
	protected boolean appendTimestamp;
	protected boolean includePK;
	protected Severity minSeverity;
	protected boolean useChildDataSpace;
	protected String permissionsTemplateDataSpaceName = DEFAULT_PERMISSIONS_TEMPLATE_DATA_SPACE_NAME;

	public GenerateValidationReport()
	{
		this(DEFAULT_EXPORT_DIR_NAME, false, false, Severity.ERROR, false);
	}

	public GenerateValidationReport(
		String exportDirName,
		boolean appendTimestamp,
		boolean includePK,
		Severity minSeverity,
		boolean useChildDataSpace)
	{
		this.exportParentDirName = System.getProperty("ebx.home");
		this.exportDirName = exportDirName;
		this.appendTimestamp = appendTimestamp;
		this.includePK = includePK;
		this.minSeverity = minSeverity;
		this.useChildDataSpace = useChildDataSpace;
	}

	@Override
	public void setupDisplay(
		UserServiceSetupDisplayContext<DatasetEntitySelection> aContext,
		UserServiceDisplayConfigurator aConfigurator)
	{
		// Add a close button if it's been submitted
		if (submitted)
		{
			aConfigurator.setLeftButtons(aConfigurator.newCloseButton());
		}
		super.setupDisplay(aContext, aConfigurator);
	}

	@Override
	public void execute(Session session) throws OperationException
	{
		final ValidationReport validationReport;
		try
		{
			validationReport = generateValidationReport(session);
		}
		catch (OperationException e)
		{
			LoggingCategory.getKernel().error("Error occurred generating validation report.", e);
			throw OperationException.createError(e);
		}

		try
		{
			processValidationReport(session, validationReport);
		}
		catch (final IOException e)
		{
			LoggingCategory.getKernel().error("Error occurred processing validation report.", e);
			throw OperationException.createError(e);
		}
	}

	protected ValidationReport generateValidationReport(Session session) throws OperationException
	{
		ValidationReport validationReport;
		Adaptation dataSet = context.getEntitySelection().getDataset();
		AdaptationHome childDataSpace = null;
		try
		{
			if (useChildDataSpace)
			{
				childDataSpace = createChildDataSpace(session, dataSet.getHome());
				dataSet = childDataSpace.findAdaptationOrNull(dataSet.getAdaptationName());
			}
			validationReport = dataSet.getValidationReport();
		}
		finally
		{
			if (useChildDataSpace && childDataSpace != null)
			{
				closeChildDataSpace(session, childDataSpace);
			}
		}
		return validationReport;
	}

	protected AdaptationHome createChildDataSpace(Session session, AdaptationHome dataSpace)
		throws OperationException
	{
		UserReference user = session.getUserReference();

		HomeCreationSpec homeCreationSpec = new HomeCreationSpec();
		SimpleDateFormat dateFormat = new SimpleDateFormat(
			CommonConstants.DATA_SPACE_NAME_DATE_TIME_FORMAT);
		String childDataSpaceDateTimeStr = dateFormat.format(new Date());
		homeCreationSpec.setKey(HomeKey.forBranchName(childDataSpaceDateTimeStr));
		homeCreationSpec.setOwner(user);
		homeCreationSpec.setParent(dataSpace);
		homeCreationSpec.setLabel(
			UserMessage.createInfo(
				"Validation Report for " + user.getLabel() + " at " + childDataSpaceDateTimeStr));

		Repository repo = dataSpace.getRepository();
		if (permissionsTemplateDataSpaceName != null)
		{
			AdaptationHome templateDataSpace = repo
				.lookupHome(HomeKey.forBranchName(permissionsTemplateDataSpaceName));
			if (templateDataSpace == null)
			{
				LoggingCategory.getKernel().error(
					"Permissions template data space " + permissionsTemplateDataSpaceName
						+ " not found.");
			}
			else
			{
				homeCreationSpec.setHomeToCopyPermissionsFrom(templateDataSpace);
			}
		}
		return repo.createHome(homeCreationSpec, session);
	}

	protected void closeChildDataSpace(Session session, AdaptationHome childDataSpace)
		throws OperationException
	{
		childDataSpace.getRepository().closeHome(childDataSpace, session);
	}

	protected void processValidationReport(Session session, ValidationReport validationReport)
		throws IOException
	{
		if (validationReport.hasItemsOfSeverityOrMore(minSeverity))
		{
			List<ValidationErrorElement> list = getValidationErrorList(validationReport);
			generateUI(list);
		}
		else
		{
			context.getPaneWriter().add_cr("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No Validation Error");
		}
	}

	protected List<ValidationErrorElement> getValidationErrorList(ValidationReport validationReport)
	{
		final ValidationReportItemIterator itemsOfSeverity = validationReport
			.getItemsOfSeverityOrMore(minSeverity);
		Locale locale = context.getPaneWriter().getLocale();

		final ArrayList<ValidationErrorElement> list = new ArrayList<>();
		while (itemsOfSeverity.hasNext())
		{
			final ValidationReportItem nextItem = itemsOfSeverity.nextItem();
			if ((nextItem.getSubject() != null))
			{
				final ValidationErrorElement element = new ValidationErrorElement();
				if (nextItem.getSubjectForAdaptation() != null)
				{
					final Adaptation occurence = nextItem.getSubjectForAdaptation().getAdaptation();
					final UserMessage message = nextItem.getMessage();
					final String formatMessage = message.formatMessage(locale);
					element.setMessage(
						formatMessage + " ["
							+ nextItem.getSubjectForAdaptation().getPathInAdaptation().format()
							+ "]");
					element.setSeverity(nextItem.getSeverity().getLabel());
					element.setRecord(occurence);
					list.add(element);
				}
				else if (nextItem.getSubjectForTable() != null)
				{
					Adaptation adaptation = nextItem.getSubjectForTable()
						.getRecords()
						.nextAdaptation();
					{
						final UserMessage message = nextItem.getMessage();
						final String formatMessage = message.formatMessage(locale);
						element.setMessage(formatMessage);
						element.setSeverity(nextItem.getSeverity().getLabel());
						element.setRecord(adaptation);
						list.add(element);
					}
				}
			}
		}
		return list;
	}

	protected void generateUI(List<ValidationErrorElement> list) throws IOException
	{
		// If it's null, then don't do the export at all
		if (exportParentDirName != null)
		{
			String filePath = generateFile(list);
			File file = new File(filePath);
			writeDownloadLink(file);
		}
	}

	@Override
	public void landService()
	{
	}

	protected String generateFile(List<ValidationErrorElement> list) throws IOException
	{
		CSVExporter exporter = new CSVExporter(
			exportParentDirName,
			exportDirName,
			CSV_SEPARATOR,
			context.getPaneWriter().getLocale(),
			CSVExporter.DEFAULT_EXPORT_FILE_NAME,
			CSVExporter.DEFAULT_EXPORT_FILE_EXTENSION,
			appendTimestamp,
			includePK);
		exporter.setValidationList(list);
		return exporter.doExport();
	}

	public String getExportParentDirName()
	{
		return this.exportParentDirName;
	}

	public void setExportParentDirName(String exportParentDirName)
	{
		this.exportParentDirName = exportParentDirName;
	}

	public String getExportDirName()
	{
		return this.exportDirName;
	}

	public void setExportDirName(String exportDirName)
	{
		this.exportDirName = exportDirName;
	}

	public boolean isAppendTimestamp()
	{
		return this.appendTimestamp;
	}

	public void setAppendTimestamp(boolean appendTimestamp)
	{
		this.appendTimestamp = appendTimestamp;
	}

	public boolean isIncludePK()
	{
		return this.includePK;
	}

	public void setIncludePK(boolean includePK)
	{
		this.includePK = includePK;
	}

	public Severity getMinSeverity()
	{
		return this.minSeverity;
	}

	public void setMinSeverity(Severity minSeverity)
	{
		this.minSeverity = minSeverity;
	}

	public boolean isUseChildDataSpace()
	{
		return this.useChildDataSpace;
	}

	public void setUseChildDataSpace(boolean useChildDataSpace)
	{
		this.useChildDataSpace = useChildDataSpace;
	}

	public String getPermissionsTemplateDataSpaceName()
	{
		return this.permissionsTemplateDataSpaceName;
	}

	public void setPermissionsTemplateDataSpace(String permissionsTemplateDataSpaceName)
	{
		this.permissionsTemplateDataSpaceName = permissionsTemplateDataSpaceName;
	}

}

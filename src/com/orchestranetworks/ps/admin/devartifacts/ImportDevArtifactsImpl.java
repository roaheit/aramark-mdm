/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.admin.devartifacts;

import java.io.*;
import java.util.*;

import org.apache.commons.lang.*;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.addon.dataexchange.*;
import com.orchestranetworks.addon.dex.*;
import com.orchestranetworks.addon.dex.configuration.*;
import com.orchestranetworks.addon.dex.result.*;
import com.orchestranetworks.addon.dex.result.ImportResult;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.constants.*;
import com.orchestranetworks.ps.procedure.*;
import com.orchestranetworks.ps.procedure.Procedures.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.service.comparison.*;
import com.orchestranetworks.service.extensions.*;
import com.orchestranetworks.workflow.*;
import com.orchestranetworks.workflow.PublicationSpec.*;

/**
 * Implementation for importing of the dev artifacts
 */
public class ImportDevArtifactsImpl extends DevArtifactsBase
{
	public static final String PARAM_REPLACE_MODE = "replaceMode";
	public static final String PARAM_SKIP_NONEXISTING_FILES = "skipNonexistingFiles";
	public static final String PARAM_SELECT_ALL_WORKFLOWS = "selectAllWorkflows";
	public static final String PARAM_WORKFLOW_PREFIX = "wf_";
	public static final String PARAM_PUBLISH_WORKFLOW_MODELS = "publishWorkflowModels";

	@Override
	public DevArtifactsConfigFactory getConfigFactory()
	{
		return new ImportDevArtifactsConfigFactory();
	}

	private List<Adaptation> getAllDataSetsToProcess(
		Repository repo,
		Session session,
		List<Adaptation> dataSets,
		ImportDevArtifactsConfig importConfig) throws OperationException
	{
		List<Adaptation> newDataSets = createDataSetsFromDataModels(repo, session, importConfig);
		List<Adaptation> allDataSets;
		if (newDataSets.isEmpty())
		{
			allDataSets = dataSets;
		}
		else
		{
			allDataSets = new ArrayList<>(dataSets);
			allDataSets.addAll(newDataSets);
		}
		return allDataSets;
	}

	private List<AdaptationTable> getAllTablesToProcess(
		Repository repo,
		List<AdaptationTable> tables,
		ImportDevArtifactsConfig importConfig) throws OperationException
	{
		Map<DataSetCreationKey, List<String>> createdDataSetTableSpecs = importConfig
			.getCreatedDataSetTableSpecs();
		List<AdaptationTable> allTables;
		if (createdDataSetTableSpecs.isEmpty())
		{
			allTables = tables;
		}
		else
		{
			allTables = new ArrayList<>(tables);
			for (Map.Entry<DataSetCreationKey, List<String>> entry : createdDataSetTableSpecs
				.entrySet())
			{
				DataSetCreationKey key = entry.getKey();
				List<String> tableSpecs = entry.getValue();
				for (String tableSpec : tableSpecs)
				{
					HomeKey dataSpaceKey = key.getDataSpaceKey();
					AdaptationHome dataSpace = repo.lookupHome(dataSpaceKey);
					if (dataSpace == null)
					{
						throw OperationException.createError(
							"Can't find data space " + dataSpaceKey.getName() + " for table "
								+ tableSpec + ".");
					}
					AdaptationName dataSetName = key.getDataSetName();
					Adaptation dataSet = dataSpace.findAdaptationOrNull(dataSetName);
					if (dataSet == null)
					{
						throw OperationException.createError(
							"Can't find data set " + dataSetName.getStringName() + " in data space "
								+ dataSpaceKey.getName() + " for table " + tableSpec + ".");
					}
					// Replace wildcard with all tables for that data set
					if (tableSpec.endsWith(WILDCARD))
					{
						allTables.addAll(AdaptationUtil.getAllTables(dataSet));
					}
					else
					{
						AdaptationTable table = dataSet.getTable(Path.parse(tableSpec));
						if (table == null)
						{
							throw OperationException
								.createError("Can't find table " + tableSpec + ".");
						}
						else
						{
							allTables.add(table);
						}
					}
				}
			}
		}
		return allTables;
	}

	@Override
	protected void processDataTables(
		Repository repo,
		Session session,
		List<AdaptationTable> tables,
		File folder,
		String filePrefix,
		DevArtifactsConfig config) throws OperationException
	{
		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		List<AdaptationTable> allTables = getAllTablesToProcess(repo, tables, importConfig);
		super.processDataTables(repo, session, allTables, folder, filePrefix, importConfig);
	}

	@Override
	protected void processTable(
		Session session,
		ProcedureContext pContext,
		AdaptationTable table,
		File folder,
		String filename,
		String predicate,
		AdaptationFilter filter,
		DevArtifactsConfig config) throws OperationException
	{
		ImportSpec spec = createImportSpec(folder, filename, (ImportDevArtifactsConfig) config);
		if (spec == null)
		{
			return;
		}

		spec.setTargetAdaptationTable(table);

		if (pContext == null)
		{
			ImportXMLProcedure proc = new ImportXMLProcedure(spec, false);
			ProcedureExecutor
				.executeProcedure(proc, session, table.getContainerAdaptation().getHome());
		}
		else
		{
			doImport(pContext, spec);
		}
	}

	private static void doImport(ProcedureContext pContext, ImportSpec spec)
		throws OperationException
	{
		boolean allPrivileges = pContext.isAllPrivileges();
		pContext.setAllPrivileges(true);
		try
		{
			pContext.doImport(spec);
		}
		finally
		{
			pContext.setAllPrivileges(allPrivileges);
		}
	}

	@Override
	protected void processGroup(
		Session session,
		Adaptation dataSet,
		SchemaNode groupNode,
		File folder,
		String filename,
		DevArtifactsConfig config) throws OperationException
	{
		ImportSpec spec = createImportSpec(folder, filename, (ImportDevArtifactsConfig) config);
		if (spec == null)
		{
			return;
		}

		spec.setTargetAdaptation(dataSet);

		ImportXMLProcedure proc = new ImportXMLProcedure(spec, false);
		ProcedureExecutor.executeProcedure(proc, session, dataSet.getHome());
	}

	private static ImportSpec createImportSpec(
		File folder,
		String filename,
		ImportDevArtifactsConfig importConfig) throws OperationException
	{
		File srcFile = new File(folder, filename + ".xml");
		// Only throw an error if they don't want to skip non-existent files
		if (!srcFile.exists())
		{
			if (importConfig.isSkipNonExistingFiles())
			{
				return null;
			}
			throw OperationException
				.createError("Import file doesn't exist: " + srcFile.getAbsolutePath());

		}

		ImportSpec spec = new ImportSpec();
		spec.setImportMode(importConfig.getImportMode());
		spec.setSourceFile(srcFile);
		return spec;
	}

	@Override
	protected void processDataSetDataXML(
		Session session,
		Adaptation dataSet,
		File folder,
		String filePrefix,
		DevArtifactsConfig config,
		boolean suppressTriggers) throws OperationException
	{
		ImportSpec spec = createImportSpec(
			folder,
			filePrefix + dataSet.getAdaptationName().getStringName(),
			(ImportDevArtifactsConfig) config);
		if (spec == null)
		{
			return;
		}

		spec.setTargetAdaptation(dataSet);

		ImportXMLProcedure proc = new ImportXMLProcedure(spec, suppressTriggers);
		ProcedureExecutor.executeProcedure(proc, session, dataSet.getHome());
	}

	private static ImportDataExchangeConfigurationSpec createImportDataExchangeConfigurationSpec(
		Session session,
		File folder,
		String filename,
		ImportDevArtifactsConfig importConfig) throws OperationException
	{
		File srcFile = new File(folder, filename + ".xml");
		// Only throw an error if they don't want to skip non-existent files
		if (!srcFile.exists())
		{
			if (importConfig.isSkipNonExistingFiles())
			{
				return null;
			}
			throw OperationException
				.createError("Import file doesn't exist: " + srcFile.getAbsolutePath());
		}

		ImportDataExchangeConfigurationSpec configurationSpec = new ImportDataExchangeConfigurationSpec();
		configurationSpec.setImportedFile(srcFile);
		configurationSpec.setSession(session);
		configurationSpec
			.setImportMode(getImportModeFromImportSpecMode(importConfig.getImportMode()));
		return configurationSpec;
	}

	private static ImportMode getImportModeFromImportSpecMode(ImportSpecMode importSpecMode)
	{
		if (ImportSpecMode.INSERT.equals(importSpecMode))
		{
			return ImportMode.INSERT_ONLY;
		}
		else if (ImportSpecMode.REPLACE.equals(importSpecMode))
		{
			return ImportMode.REPLACE_ALL_CONTENT;
		}
		else if (ImportSpecMode.UPDATE.equals(importSpecMode))
		{
			return ImportMode.UPDATE_ONLY;
		}
		else if (ImportSpecMode.UPDATE_OR_INSERT.equals(importSpecMode))
		{
			return ImportMode.UPDATE_AND_INSERT;
		}
		return null;
	}

	@Override
	protected void processDataExchangeConfigXML(
		Session session,
		Adaptation dataSet,
		File folder,
		String filePrefix,
		DevArtifactsConfig config,
		boolean suppressTriggers) throws OperationException
	{
		// The Data Exchange import uses the new Import XML data exchange service.
		// However, the Data Exchange export uses the default way of exporting a data set as XML, just like the other data sets, 
		// since the new Export XML data exchange service also outputs the xml in the same format.
		// The new Export XML and Import XML data exchange services are available in Data Exchange version 2.4.2
		ImportDataExchangeConfigurationSpec configurationSpec = createImportDataExchangeConfigurationSpec(
			session,
			folder,
			filePrefix + dataSet.getAdaptationName().getStringName(),
			(ImportDevArtifactsConfig) config);
		if (configurationSpec == null)
		{
			return;
		}

		try
		{
			// perform the import
			DataExchangeResult.XMLImport importResult = DataExchangeServiceFactory
				.getDataExchangeConfigurationService()
				.doImport(configurationSpec);

			StringBuilder resultMessage = new StringBuilder();

			for (Iterator<ImportResult> iterator = (Iterator<ImportResult>) importResult
				.getResults(); iterator.hasNext();)
			{
				Result result = iterator.next();
				// iterate the errors, build a string and throw an Operation Exception
				for (UserMessage userMessage : result.getErrorMessages().values())
				{
					resultMessage.append(userMessage.formatMessage(Locale.getDefault()))
						.append("\n");
				}
				// iterate the warnings and log them in kernel log.
				for (Iterator<UserMessage> iterator2 = result.getWarningMessages(); iterator2
					.hasNext();)
				{
					UserMessage warningMessage = iterator2.next();
					LoggingCategory.getKernel().warn(warningMessage);
				}
			}
			if (resultMessage.length() > 0)
			{
				throw OperationException.createError(resultMessage.toString());
			}
		}
		catch (DataExchangeException e)
		{
			throw OperationException.createError(e);
		}
	}

	@Override
	protected Properties processDataSetDataPropertiesFile(
		Session session,
		AdaptationHome dataSpace,
		AdaptationName dataSetName,
		File folder,
		String filePrefix,
		DevArtifactsConfig config) throws IOException, OperationException
	{
		Properties props = new Properties();
		File propertiesFile = new File(
			folder,
			filePrefix + dataSetName.getStringName() + DATA_SET_PROPERTIES_SUFFIX);
		if (propertiesFile.exists())
		{
			BufferedReader reader = new BufferedReader(new FileReader(propertiesFile));

			try
			{
				props.load(reader);
			}
			finally
			{
				reader.close();
			}
		}
		return props;
	}

	@Override
	protected void processDataSetDataProperties(
		Session session,
		Properties props,
		Adaptation dataSet,
		File folder,
		String filePrefix,
		DevArtifactsConfig config) throws IOException, OperationException
	{
		String label = getPropertyValueOrNull(props, DATA_SET_PROPERTY_LABEL);
		String description = getPropertyValueOrNull(props, DATA_SET_PROPERTY_DESCRIPTION);
		String owner = getPropertyValueOrNull(props, DATA_SET_PROPERTY_OWNER);
		Locale locale = session.getLocale();

		if ((label != null && !label.equals(dataSet.getLabel(locale)))
			|| (!ObjectUtils.equals(description, dataSet.getDescription(locale)))
			|| (owner != null && !owner.equals(dataSet.getOwner().format())))
		{
			SetDataSetPropertiesProcedure setDataSetPropsProc = new SetDataSetPropertiesProcedure(
				dataSet,
				label,
				description,
				owner);
			ProcedureExecutor.executeProcedure(setDataSetPropsProc, session, dataSet);
		}
	}

	private static String getPropertyValueOrNull(Properties props, String key)
	{
		String value = props.getProperty(key);
		if (value != null)
		{
			value = value.trim();
			if ("".equals(value))
			{
				value = null;
			}
		}
		return value;
	}

	@Override
	protected void processDataSetPermissions(
		Session session,
		Adaptation dataSet,
		File folder,
		DevArtifactsConfig config) throws OperationException
	{
		super.processDataSetPermissions(session, dataSet, folder, config);
		if (config.isProcessDataSetPermissionsInChildDataSpaces())
		{
			AdaptationHome dataSpace = dataSet.getHome();
			// Will never handle child of administrative data spaces, so can skip checking for these
			if (!AdminUtil.isAdminDataSpace(dataSpace))
			{
				List<AdaptationHome> snapshots = dataSpace.getVersionChildren();
				for (AdaptationHome snapshot : snapshots)
				{
					List<AdaptationHome> childDataSpaces = snapshot.getBranchChildren();
					for (AdaptationHome childDataSpace : childDataSpaces)
					{
						if (childDataSpace.isOpen())
						{
							Adaptation childDataSet = childDataSpace
								.findAdaptationOrNull(dataSet.getAdaptationName());
							if (childDataSet != null)
							{
								processDataSetPermissions(session, childDataSet, folder, config);
							}
						}
					}
				}
			}
		}
	}

	@Override
	protected void processTasksData(
		Repository repo,
		Session session,
		File folder,
		DevArtifactsConfig config) throws OperationException
	{
		if (config.isProcessTasksData())
		{
			Adaptation taskSchedulerDataSet = AdminUtil.getTaskSchedulerDataSet(repo);

			// Can't use replace mode since there's the built-in purge task
			AdaptationTable tasksTable = AdminUtil.getTasksTable(taskSchedulerDataSet);
			ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
			ImportSpecMode importMode = importConfig.getImportMode();
			if (ImportSpecMode.REPLACE.equals(importMode))
			{
				importConfig.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
			}
			processTable(
				session,
				null,
				tasksTable,
				folder,
				DATA_PREFIX + getTableFilename(tasksTable),
				null,
				null,
				config);
			if (ImportSpecMode.REPLACE.equals(importMode))
			{
				importConfig.setImportMode(importMode);
			}
		}
	}

	@Override
	protected void processViewsData(
		Repository repo,
		Session session,
		File folder,
		DevArtifactsConfig config) throws OperationException
	{
		// Turn off replace mode for views
		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		ImportSpecMode importMode = importConfig.getImportMode();
		if (ImportSpecMode.REPLACE.equals(importMode))
		{
			importConfig.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
		}

		super.processViewsData(repo, session, folder, config);

		if (ImportSpecMode.REPLACE.equals(importMode))
		{
			importConfig.setImportMode(importMode);
		}
	}

	@Override
	protected void copyEnvironment(Repository repo, Session session, DevArtifactsConfig config)
		throws OperationException
	{
		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		ImportSpecMode importMode = importConfig.getImportMode();
		importConfig.setImportMode(ImportSpecMode.REPLACE);

		List<AdaptationTable> tablesForData = config.getTablesForData();

		createDataSetsFromDataModels(repo, session, importConfig);
		List<AdaptationTable> allTables = getAllTablesToProcess(repo, tablesForData, importConfig);

		try
		{
			// TODO: Would like to just include the data sets in the list I process below,
			//       but EBX won't let me export/import archives of some of these, even if
			//       I'm just handling permissions and info
			if (config.isProcessAdminDataSetPermissions())
			{
				processAdminDataSetPermissions(
					repo,
					session,
					config.getCopyEnvironmentFolder(),
					config);
			}
			doEnvCopyDataSpacePermissions(session, config);
			doEnvCopyDirectory(repo, session, config);
			doEnvCopyViews(repo, session, config);
			doEnvCopyTasks(repo, session, config);
			doEnvCopyPerspectives(repo, session, config);
			doEnvCopyWorkflowAdminConfiguration(repo, session, config);
			doEnvCopyDataTables(repo, session, allTables, config);
		}
		finally
		{
			importConfig.setImportMode(importMode);
		}

		Set<AdaptationHome> dataSpacesToCopy = new HashSet<>();

		prepareEnvCopyWFModels(repo, dataSpacesToCopy);
		prepareEnvCopyDMAModels(repo, dataSpacesToCopy);
		prepareEnvCopyGlobalPermissions(repo, dataSpacesToCopy);
		prepareEnvCopyDataSpaces(repo, allTables, config, dataSpacesToCopy);
		prepareEnvCopyAddons(repo, config, dataSpacesToCopy);

		for (AdaptationHome dataSpace : dataSpacesToCopy)
		{
			File srcFile = new File(
				config.getCopyEnvironmentFolder(),
				ENV_COPY_ARCHIVE_PREFIX + dataSpace.getKey().getName() + ".ebx");
			importArchive(session, dataSpace, srcFile, config);
		}

		if (importConfig.isPublishWorkflowModels())
		{
			publishWorkflowModels(
				repo,
				session,
				AdminUtil.getAllWorkflowModelNames(repo),
				importConfig);
		}
	}

	@Override
	protected void removeReferenceDataNodesFromXml(
		Adaptation dataSet,
		File folder,
		String filePrefix,
		String groupName,
		String fieldName,
		String fieldValue) throws OperationException
	{
		// The reference nodes are removed during export
		// For now, there is no import specific implementation
		return;
	}

	@Override
	protected void processArtifacts(Repository repo, Session session, DevArtifactsConfig config)
		throws OperationException
	{
		// For import, if it's an environment copy we skip all other artifacts
		if (!config.isEnvironmentCopy())
		{
			super.processArtifacts(repo, session, config);
		}
	}

	@Override
	protected void processDataSetPermissions(
		Repository repo,
		Session session,
		List<Adaptation> dataSets,
		File folder,
		DevArtifactsConfig config) throws OperationException
	{
		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		List<Adaptation> allDataSets = getAllDataSetsToProcess(
			repo,
			session,
			dataSets,
			importConfig);
		super.processDataSetPermissions(repo, session, allDataSets, folder, config);

		if (!importConfig.getDataSetsToCreate().isEmpty())
		{
			repo.refreshSchemas(true);
		}
	}

	private List<Adaptation> createDataSetsFromDataModels(
		Repository repo,
		Session session,
		ImportDevArtifactsConfig importConfig) throws OperationException
	{
		List<DataSetCreationInfo> dataSetsToCreate = importConfig.getDataSetsToCreate();
		List<Adaptation> dataSets = new ArrayList<>();
		for (DataSetCreationInfo dataSetCreationInfo : dataSetsToCreate)
		{
			DataSetCreationKey key = dataSetCreationInfo.getDataSetCreationKey();
			HomeKey dataSpaceKey = key.getDataSpaceKey();
			AdaptationName dataSetName = key.getDataSetName();

			AdaptationHome dataSpace = repo.lookupHome(dataSpaceKey);
			if (dataSpace == null)
			{
				throw OperationException.createError(
					"Can't create data set " + dataSetName.getStringName() + " because data space "
						+ dataSpaceKey.getName() + " doesn't exist.");
			}

			File folder = importConfig.getPermissionsFolder();
			Properties props;
			try
			{
				props = processDataSetDataPropertiesFile(
					session,
					dataSpace,
					dataSetName,
					folder,
					PERMISSIONS_DATA_SET_PREFIX,
					importConfig);
			}
			catch (IOException ex)
			{
				throw OperationException.createError(ex);
			}
			Adaptation dataSet = getOrCreateDataSetFromProperties(
				session,
				dataSetName.getStringName(),
				props,
				dataSpace,
				SchemaLocation.parse("urn:ebx:module:" + dataSetCreationInfo.getDataModelXSD()));
			if (dataSet != null)
			{
				dataSets.add(dataSet);
			}
		}
		return dataSets;
	}

	@Override
	protected void processDataSpacePermissions(
		ProcedureContext pContext,
		AdaptationHome dataSpace,
		File folder,
		AdaptationTable permissionsTable,
		DevArtifactsConfig config) throws OperationException
	{
		// Data space permissions can't use replace mode so manually delete all rows for
		// this data space that aren't owner, then change mode to update/insert and import
		final ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		ImportSpecMode importMode = importConfig.getImportMode();
		if (ImportSpecMode.REPLACE.equals(importMode))
		{
			// TODO: Should probably define these fields in AdminPermissionUtil.
			final RequestResult requestResult = permissionsTable.createRequestResult(
				"homeKey='" + dataSpace.getKey().format() + "' and not(profile='Bowner')");
			try
			{
				// Delete all but the owner row for this data space
				DeleteRecordProcedure drp = new DeleteRecordProcedure();
				drp.setAllPrivileges(true);
				Adaptation record;
				while ((record = requestResult.nextAdaptation()) != null)
				{
					drp.setAdaptation(record);
					drp.execute(pContext);
				}
				try
				{
					importConfig.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
					super.processDataSpacePermissions(
						pContext,
						dataSpace,
						folder,
						permissionsTable,
						config);
				}
				finally
				{
					importConfig.setImportMode(importMode);
				}
			}
			finally
			{
				requestResult.close();
			}
		}
		else
		{
			super.processDataSpacePermissions(
				pContext,
				dataSpace,
				folder,
				permissionsTable,
				config);
		}
	}

	@Override
	protected void processUsersRolesTable(
		Session session,
		final AdaptationTable usersRolesTable,
		File folder,
		String filePrefix,
		DevArtifactsConfig config) throws OperationException
	{
		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		ImportUsersRolesProcedure replaceUsersRolesProc = new ImportUsersRolesProcedure(
			usersRolesTable,
			importConfig,
			folder,
			filePrefix);
		ProcedureExecutor.executeProcedure(
			replaceUsersRolesProc,
			session,
			usersRolesTable.getContainerAdaptation());
	}

	private Adaptation processDataSetFromFile(
		Session session,
		String dataSetName,
		Properties dataSetProps,
		File folder,
		String filePrefix,
		DevArtifactsConfig config,
		AdaptationHome dataSpace,
		SchemaLocation schemaLocation) throws OperationException
	{

		Adaptation dataSet = getOrCreateDataSetFromProperties(
			session,
			dataSetName,
			dataSetProps,
			dataSpace,
			schemaLocation);

		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		ImportSpecMode importMode = importConfig.getImportMode();
		importConfig.setImportMode(ImportSpecMode.REPLACE);

		processDataSetDataXML(session, dataSet, folder, filePrefix, config, false);

		importConfig.setImportMode(importMode);

		try
		{
			processDataSetDataProperties(
				session,
				dataSetProps,
				dataSet,
				folder,
				filePrefix,
				config);
		}
		catch (IOException ex)
		{
			throw OperationException.createError(ex);
		}
		return dataSet;
	}

	private Adaptation getOrCreateDataSetFromProperties(
		Session session,
		String dataSetName,
		Properties dataSetProps,
		AdaptationHome dataSpace,
		SchemaLocation schemaLocation) throws OperationException
	{
		AdaptationReference adaptationRef = AdaptationReference.forPersistentName(dataSetName);
		Adaptation dataSet = dataSpace.findAdaptationOrNull(adaptationRef);
		if (dataSet == null)
		{
			String parentDataSetName = getPropertyValueOrNull(
				dataSetProps,
				DATA_SET_PROPERTY_PARENT_DATA_SET);
			CreateDataSetProcedure createDataSetProc;
			if (parentDataSetName == null)
			{
				createDataSetProc = new CreateRootDataSetProcedure(adaptationRef, schemaLocation);
			}
			else
			{
				createDataSetProc = new CreateChildDataSetProcedure(
					adaptationRef,
					AdaptationName.forName(parentDataSetName));
			}
			ProcedureExecutor.executeProcedure(createDataSetProc, session, dataSpace);
			dataSet = createDataSetProc.getDataSet();
		}
		return dataSet;
	}

	@Override
	protected void processWorkflowModels(
		Repository repo,
		Session session,
		List<String> wfModels,
		File folder,
		String filePrefix,
		DevArtifactsConfig config) throws OperationException
	{
		super.processWorkflowModels(repo, session, wfModels, folder, filePrefix, config);

		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		if (importConfig.isPublishWorkflowModels())
		{
			publishWorkflowModels(repo, session, wfModels, importConfig);
		}
	}

	@Override
	protected void processWorkflowModel(
		Session session,
		String wfModel,
		File folder,
		String filePrefix,
		DevArtifactsConfig config,
		AdaptationHome wfDataSpace) throws OperationException
	{
		Properties props;
		try
		{
			props = processDataSetDataPropertiesFile(
				session,
				wfDataSpace,
				AdaptationName.forName(wfModel),
				folder,
				filePrefix,
				config);
		}
		catch (IOException ex)
		{
			throw OperationException.createError(ex);
		}
		processDataSetFromFile(
			session,
			wfModel,
			props,
			folder,
			filePrefix,
			config,
			wfDataSpace,
			AdminUtil.getWorkflowModelsSchemaLocation());
	}

	private static void publishWorkflowModels(
		Repository repo,
		Session session,
		List<String> wfModelNames,
		ImportDevArtifactsConfig importConfig) throws OperationException
	{
		if (!wfModelNames.isEmpty())
		{
			Map<String, Set<String>> masterWorkflowModels = importConfig.getMasterWorkflowModels();
			List<String> wfModelsToNotPublish = importConfig.getWorkflowModelsToNotPublish();
			// The map is keyed by master workflow, but for our processing, we also need to lookup by sub-wf
			// so create another map that's the reverse
			Map<String, Set<String>> reverseMap = getReverseMasterWorkflowModelMap(
				masterWorkflowModels);
			HashMap<String, Set<String>> publications = new HashMap<>();

			for (String wfModelName : wfModelNames)
			{
				// Never publish the configuration data set or any of the wf models that were specified to never publish
				if (!(CommonConstants.WORKFLOW_MODEL_CONFIGURATION.equals(wfModelName)
					|| wfModelsToNotPublish.contains(wfModelName)))
				{
					// Get the master workflows that this workflow is used by
					Set<String> masterWFModelNames = reverseMap.get(wfModelName);
					// If no master workflows, then it needs to be published itself
					if (masterWFModelNames == null || masterWFModelNames.isEmpty())
					{
						// Get the wf models already specified for this workflow (or create if none specified yet)
						Set<String> publicationWFModels = publications.get(wfModelName);
						if (publicationWFModels == null)
						{
							publicationWFModels = new HashSet<>();
							publications.put(wfModelName, publicationWFModels);
						}
						// Add this workflow to the list of wf models to include for this publication
						publicationWFModels.add(wfModelName);
					}
					// It belongs to a master
					else
					{
						// Loop over the master wf models that it's used by
						Iterator<String> masterWFModelIter = masterWFModelNames.iterator();
						while (masterWFModelIter.hasNext())
						{
							String masterWFModelName = masterWFModelIter.next();
							// Get the wf models already specified for the master wf model publication (or create if not specified yet)
							Set<String> publicationWFModels = publications.get(masterWFModelName);
							if (publicationWFModels == null)
							{
								publicationWFModels = new HashSet<>();
								publications.put(masterWFModelName, publicationWFModels);
							}
							// Add both the master wf model and this wf model to the list of wf models to publish for the master.
							// It's a set so we don't need to check if it's already there - it will prevent duplicates
							publicationWFModels.add(masterWFModelName);
							publicationWFModels.add(wfModelName);
						}
					}
				}
			}

			AdaptationHome workflowModelsDataSpace = AdminUtil.getWorkflowModelsDataSpace(repo);

			// Dev artifacts is not internationalized at this point so just uses default locale
			Locale locale = Locale.getDefault();
			WorkflowEngine wfEngine = WorkflowEngine.getFromRepository(repo, session);
			// Master workflow publications need to be treated differently than others.
			// As with publishing via the UI, you need to publish masters one at a time. Others you can publish at once.
			List<PublicationSpec> masterPublicationSpecs = new ArrayList<>();
			List<PublicationSpec> nonMasterPublicationSpecs = new ArrayList<>();

			// There is no method for looking up if a published process key exists, so we need to get all and keep in a map for easy lookup
			Map<String, PublishedProcessKey> publishedProcessKeys = getPublishedProcessKeyMap(
				wfEngine);
			// Loop over all the publications we need to publish
			for (String publicationName : publications.keySet())
			{
				// Get the data sets (i.e. wf models) to include in the publication
				Set<String> dataSetNames = publications.get(publicationName);
				if (dataSetNames != null)
				{
					PublicationMode publicationMode = null;
					PublishedProcessKey publishedProcessKey = publishedProcessKeys
						.get(publicationName);
					// If its published process key isn't found, then it's a new publication and we always want to publish it
					if (publishedProcessKey == null)
					{
						publicationMode = PublicationMode.NEW;
					}
					// Otherwise it's an update and we only want to publish it if there were changes to any of the data sets
					else
					{
						PublishedProcess publishedProcess = wfEngine
							.getPublishedProcess(publishedProcessKey);
						// Get the snapshot that the publication uses
						AdaptationHome publicationSnapshot = repo
							.lookupHome(publishedProcess.getVersionKey());
						Iterator<String> dataSetNamesIter = dataSetNames.iterator();
						// Loop over the data sets for the publication until we have determined we need to publish
						while (publicationMode == null && dataSetNamesIter.hasNext())
						{
							// If the workflow model has changed then we need to publish in update mode (and end the loop)
							if (hasWorkflowModelChanged(
								dataSetNamesIter.next(),
								workflowModelsDataSpace,
								publicationSnapshot,
								locale))
							{
								publicationMode = PublicationMode.UPDATE;
							}
						}
					}
					// If it's not null, it means we need to publish (either new or update)
					if (publicationMode != null)
					{
						// Create the spec and add it to the appropriate list based on whether it's a master or not
						PublicationSpec spec = new PublicationSpec(
							AdaptationName.forName(publicationName),
							publicationName,
							publicationMode);
						if (masterWorkflowModels.containsKey(publicationName))
						{
							masterPublicationSpecs.add(spec);
						}
						else
						{
							nonMasterPublicationSpecs.add(spec);
						}
					}
				}
			}

			// Loop over all master specs and publish each one separately
			for (PublicationSpec spec : masterPublicationSpecs)
			{
				WorkflowPublisher publisher = WorkflowEngineBridge.getWorkflowPublisher(wfEngine);
				publisher.addPublicationSpec(spec);
				publisher.publish();
			}
			// Loop over all non master specs and publish them all at once
			if (!nonMasterPublicationSpecs.isEmpty())
			{
				WorkflowPublisher publisher = WorkflowEngineBridge.getWorkflowPublisher(wfEngine);
				for (PublicationSpec spec : nonMasterPublicationSpecs)
				{
					publisher.addPublicationSpec(spec);
				}
				publisher.publish();
			}
		}
	}

	// Helper method that takes in a map with key of master wf model, value of set of sub-wf models and reverses it
	// to return a map with key of sub-wf model and value of set of master wf models
	private static Map<String, Set<String>> getReverseMasterWorkflowModelMap(
		Map<String, Set<String>> masterWFModels)
	{
		HashMap<String, Set<String>> reverseMap = new HashMap<>();
		Iterator<String> masterIter = masterWFModels.keySet().iterator();
		while (masterIter.hasNext())
		{
			String masterWFModelName = masterIter.next();
			Set<String> subWFModels = masterWFModels.get(masterWFModelName);
			Iterator<String> subIter = subWFModels.iterator();
			while (subIter.hasNext())
			{
				String subWFModelName = subIter.next();
				Set<String> reverseSet = reverseMap.get(subWFModelName);
				if (reverseSet == null)
				{
					reverseSet = new HashSet<>();
					reverseMap.put(subWFModelName, reverseSet);
				}
				reverseSet.add(masterWFModelName);
			}
		}
		return reverseMap;
	}

	// This just puts all the enabled published process keys into a map.
	// We do this so that we can check if it exists since simply calling getPublishedProcess and passing in a non-existent key throws an exception.
	private static Map<String, PublishedProcessKey> getPublishedProcessKeyMap(
		WorkflowEngine workflowEngine)
	{
		HashMap<String, PublishedProcessKey> map = new HashMap<>();

		List<PublishedProcessKey> publishedProcessKeys = workflowEngine.getPublishedKeys();
		for (PublishedProcessKey key : publishedProcessKeys)
		{
			map.put(key.getName(), key);
		}
		return map;
	}

	private static boolean hasWorkflowModelChanged(
		String dataSetName,
		AdaptationHome workflowModelsDataSpace,
		AdaptationHome publicationSnapshot,
		Locale locale)
	{
		AdaptationReference adaptationRef = AdaptationReference.forPersistentName(dataSetName);
		Adaptation publicationDataSet = publicationSnapshot.findAdaptationOrNull(adaptationRef);
		if (publicationDataSet == null)
		{
			return true;
		}

		Adaptation dataSet = workflowModelsDataSpace.findAdaptationOrNull(adaptationRef);
		// Compare label, description, and owner because these aren't included in data set comparison
		if (!ObjectUtils.equals(publicationDataSet.getLabel(locale), dataSet.getLabel(locale))
			|| !ObjectUtils
				.equals(publicationDataSet.getDescription(locale), dataSet.getDescription(locale))
			|| !ObjectUtils.equals(publicationDataSet.getOwner(), dataSet.getOwner()))
		{
			return true;
		}

		// Compare the data sets and it's changed if there are any differences
		DifferenceBetweenInstances wfDiffs = DifferenceHelper
			.compareInstances(publicationDataSet, dataSet, false);
		return !wfDiffs.isEmpty();
	}

	@Override
	protected void processPerspectivesData(
		Repository repo,
		Session session,
		File folder,
		DevArtifactsConfig config) throws OperationException
	{
		super.processPerspectivesData(repo, session, folder, config);

		if (config.isProcessPerspectivesData())
		{
			Adaptation perspectivesDataSet = AdminUtil.getPerspectivesDataSet(repo);

			AdaptationTreeOptimizerSpec_RemoveDuplicates optimizer = new AdaptationTreeOptimizerSpec_RemoveDuplicates(
				perspectivesDataSet,
				true);
			AdaptationTreeOptimizerIterator iter = optimizer.createOptimizerIterator(session);
			iter.executeAll();
		}
	}

	@Override
	protected void doProcessPerspectivesDataSet(
		Session session,
		File folder,
		AdaptationHome dataSpace,
		String dataSetName,
		DevArtifactsConfig config) throws OperationException
	{
		AdaptationName perspectiveName = AdaptationName.forName(dataSetName);
		Properties props;
		try
		{
			props = processDataSetDataPropertiesFile(
				session,
				dataSpace,
				perspectiveName,
				folder,
				PERSPECTIVE_PREFIX,
				config);
		}
		catch (IOException ex)
		{
			throw OperationException.createError(ex);
		}
		Adaptation dataSet = getOrCreateDataSetFromProperties(
			session,
			dataSetName,
			props,
			dataSpace,
			null);

		processPerspectivesDataGroups(session, folder, dataSet, config);
		try
		{
			processDataSetDataProperties(
				session,
				props,
				dataSet,
				folder,
				PERSPECTIVE_PREFIX,
				config);
		}
		catch (IOException ex)
		{
			throw OperationException.createError(ex);
		}

		processPerspectivesChildDataSets(session, folder, config, dataSet, props);
	}

	private void processPerspectivesChildDataSets(
		Session session,
		File folder,
		DevArtifactsConfig config,
		Adaptation parentDataSet,
		Properties parentDataSetProperties) throws OperationException
	{
		AdaptationHome perspectivesDataSpace = parentDataSet.getHome();
		String childDataSetsStr = getPropertyValueOrNull(
			parentDataSetProperties,
			DATA_SET_PROPERTY_CHILD_DATA_SETS);
		if (childDataSetsStr != null)
		{
			String[] childPerspectives = childDataSetsStr
				.split(DATA_SET_PROPERTIES_FILE_CHILD_DATA_SETS_SEPARATOR);
			for (String childPerspective : childPerspectives)
			{
				doProcessPerspectivesDataSet(
					session,
					folder,
					perspectivesDataSpace,
					childPerspective,
					config);
			}
		}
	}

	// Overridden in order to simulate replace mode when a filter is provided (otherwise would
	// delete other records as well)
	@Override
	protected void processDataTable(
		ProcedureContext pContext,
		AdaptationTable table,
		File folder,
		String filename,
		DevArtifactsConfig config) throws OperationException
	{
		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		if (ImportSpecMode.REPLACE.equals(importConfig.getImportMode()))
		{
			AdaptationFilter filter = getFilterForDataTable(table);
			if (filter == null)
			{
				super.processDataTable(pContext, table, folder, filename, config);
			}
			else
			{
				// Delete all existing records that match the filter
				Request request = table.createRequest();
				request.setSpecificFilter(filter);
				RequestResult requestResult = request.execute();
				try
				{
					DeleteRecordProcedure drp = new DeleteRecordProcedure();
					drp.setAllPrivileges(true);
					Adaptation record;
					while ((record = requestResult.nextAdaptation()) != null)
					{
						drp.setAdaptation(record);
						drp.execute(pContext);
					}
				}
				finally
				{
					requestResult.close();
				}

				// Import in update or insert mode
				importConfig.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
				try
				{
					super.processDataTable(pContext, table, folder, filename, config);
				}
				finally
				{
					importConfig.setImportMode(ImportSpecMode.REPLACE);
				}
			}
		}
		else
		{
			super.processDataTable(pContext, table, folder, filename, config);
		}
	}

	private void doEnvCopyDataSpacePermissions(Session session, DevArtifactsConfig config)
		throws OperationException
	{
		List<AdaptationHome> dataSpacesForPerm = config.getDataSpacesForPermissions();
		processDataSpacesPermissions(
			session,
			dataSpacesForPerm,
			config.getCopyEnvironmentFolder(),
			config);
	}

	private void doEnvCopyDirectory(Repository repo, Session session, DevArtifactsConfig config)
		throws OperationException
	{
		processDirectoryData(repo, session, config.getCopyEnvironmentFolder(), config);
	}

	private void doEnvCopyViews(Repository repo, Session session, DevArtifactsConfig config)
		throws OperationException
	{
		processViewsData(repo, session, config.getCopyEnvironmentFolder(), config);
	}

	// TODO: Why aren't I doing these at parent level because both import/export seem to be the same
	private void doEnvCopyTasks(Repository repo, Session session, DevArtifactsConfig config)
		throws OperationException
	{
		processTasksData(repo, session, config.getCopyEnvironmentFolder(), config);
	}

	private void doEnvCopyPerspectives(Repository repo, Session session, DevArtifactsConfig config)
		throws OperationException
	{
		processPerspectivesData(repo, session, config.getCopyEnvironmentFolder(), config);
	}

	private void doEnvCopyWorkflowAdminConfiguration(
		Repository repo,
		Session session,
		DevArtifactsConfig config) throws OperationException
	{
		processWorkflowAdminConfigurationData(
			repo,
			session,
			config.getCopyEnvironmentFolder(),
			config);
	}

	private void prepareEnvCopyWFModels(Repository repo, Set<AdaptationHome> dataSpacesToCopy)
		throws OperationException
	{
		AdaptationHome wfDataSpace = AdminUtil.getWorkflowModelsDataSpace(repo);
		dataSpacesToCopy.add(wfDataSpace);
	}

	private void prepareEnvCopyDMAModels(Repository repo, Set<AdaptationHome> dataSpacesToCopy)
		throws OperationException
	{
		AdaptationHome dmaDataSpace = AdminUtil.getDMADataSpace(repo);
		dataSpacesToCopy.add(dmaDataSpace);
	}

	private void prepareEnvCopyGlobalPermissions(
		Repository repo,
		Set<AdaptationHome> dataSpacesToCopy) throws OperationException
	{
		AdaptationHome globalPermissionsDataSpace = AdminUtil.getGlobalPermissionsDataSpace(repo);
		dataSpacesToCopy.add(globalPermissionsDataSpace);
	}

	private void prepareEnvCopyDataSpaces(
		Repository repo,
		List<AdaptationTable> tablesForData,
		DevArtifactsConfig config,
		Set<AdaptationHome> dataSpacesToCopy) throws OperationException
	{
		ImportDevArtifactsConfig importConfig = (ImportDevArtifactsConfig) config;
		List<Adaptation> dataSetsForPerm = importConfig.getDataSetsForPermissions();
		for (Adaptation dataSet : dataSetsForPerm)
		{
			dataSpacesToCopy.add(dataSet.getHome());
		}

		List<DataSetCreationInfo> dataSetsToCreate = importConfig.getDataSetsToCreate();
		for (DataSetCreationInfo dataSetCreationInfo : dataSetsToCreate)
		{
			DataSetCreationKey key = dataSetCreationInfo.getDataSetCreationKey();
			HomeKey dataSpaceKey = key.getDataSpaceKey();
			AdaptationHome dataSpace = repo.lookupHome(dataSpaceKey);
			if (dataSpace != null)
			{
				dataSpacesToCopy.add(dataSpace);
			}
		}

		// Only process archive of data sets that we're processing all tables from
		// and if we're not processing data set permissions in child data spaces.
		// Otherwise, they'll be processed as individual xml files.
		if (!importConfig.isProcessDataSetPermissionsInChildDataSpaces())
		{
			// key = data set name, value = tables to process from data set
			Map<Adaptation, List<AdaptationTable>> dataSetTableMap = createMapOfDataSetTables(
				tablesForData);

			for (Map.Entry<Adaptation, List<AdaptationTable>> entry : dataSetTableMap.entrySet())
			{
				Adaptation dataSet = entry.getKey();
				List<AdaptationTable> tableList = entry.getValue();
				List<AdaptationTable> allTables = AdaptationUtil.getAllTables(dataSet);
				if (tableList.size() == allTables.size())
				{
					dataSpacesToCopy.add(dataSet.getHome());
				}
			}
		}
	}

	private void prepareEnvCopyAddons(
		Repository repo,
		DevArtifactsConfig config,
		Set<AdaptationHome> dataSpacesToCopy) throws OperationException
	{
		if (config.isProcessAddonAdixData())
		{
			dataSpacesToCopy.add(AdminUtil.getAddonAdixDataExchangeDataSpace(repo));
			dataSpacesToCopy.add(AdminUtil.getAddonAdixDataModelingDataSpace(repo));
		}

		if (config.isProcessAddonDaqaData())
		{
			dataSpacesToCopy.add(AdminUtil.getAddonDaqaDataSpace(repo));
		}

		if (config.isProcessAddonDqidData())
		{
			dataSpacesToCopy.add(AdminUtil.getAddonDqidDataSpace(repo));
		}

		if (config.isProcessAddonHmfhData())
		{
			dataSpacesToCopy.add(AdminUtil.getAddonHmfhConfigurationDataSpace(repo));
		}
	}

	private void importArchive(
		Session session,
		AdaptationHome dataSpace,
		File srcFile,
		DevArtifactsConfig config) throws OperationException
	{
		if (!srcFile.exists())
		{
			throw OperationException
				.createError("Import file doesn't exist: " + srcFile.getAbsolutePath());
		}

		ArchiveImportSpec spec = new ArchiveImportSpec();
		Archive archive = Archive.forFile(srcFile);
		spec.setArchive(archive);

		ImportArchiveProcedure proc = new ImportArchiveProcedure(spec);
		ProcedureExecutor.executeProcedure(proc, session, dataSpace);
		if (config.isProcessDataSetPermissionsInChildDataSpaces())
		{
			List<AdaptationHome> snapshots = dataSpace.getVersionChildren();
			for (AdaptationHome snapshot : snapshots)
			{
				List<AdaptationHome> childDataSpaces = snapshot.getBranchChildren();
				for (AdaptationHome childDataSpace : childDataSpaces)
				{
					if (childDataSpace.isOpen())
					{
						importArchive(session, childDataSpace, srcFile, config);
					}
				}
			}
		}
	}

	// Import the users again in update mode.
	// This is because the password last update date gets changed when importing a new record
	// but doing the import again will update it to the original value from the file.
	private void fixUserPasswordUpdateField(
		ProcedureContext pContext,
		AdaptationTable usersTable,
		File folder,
		String usersFilename,
		ImportDevArtifactsConfig config) throws OperationException
	{
		ImportSpecMode importMode = config.getImportMode();
		config.setImportMode(ImportSpecMode.UPDATE);
		try
		{
			processTable(null, pContext, usersTable, folder, usersFilename, null, null, config);
		}
		finally
		{
			config.setImportMode(importMode);
		}
	}

	private class ImportXMLProcedure implements Procedure
	{
		private final ImportSpec spec;
		private final boolean suppressTriggers;

		public ImportXMLProcedure(ImportSpec spec, boolean suppressTriggers)
		{
			this.spec = spec;
			this.suppressTriggers = suppressTriggers;
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			boolean triggerActivation = pContext.isTriggerActivation();
			pContext.setTriggerActivation(!suppressTriggers);
			try
			{
				doImport(pContext, spec);
			}
			finally
			{
				pContext.setTriggerActivation(triggerActivation);
			}
		}
	}

	private class ImportArchiveProcedure implements Procedure
	{
		private ArchiveImportSpec spec;

		public ImportArchiveProcedure(ArchiveImportSpec spec)
		{
			this.spec = spec;
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			pContext.doImportArchive(spec);
		}
	}

	/**
	 * This procedure handles importing usersRoles & users.
	 * When in replace mode, it simulates replace mode but only for the predicate specified.
	 * It also does extra processing because the import file might contain more users than what match
	 * the predicate, so those need to be ignored.
	 */
	private class ImportUsersRolesProcedure implements Procedure
	{
		private AdaptationTable usersRolesTable;
		private ImportDevArtifactsConfig config;
		private File folder;
		private String filePrefix;

		public ImportUsersRolesProcedure(
			AdaptationTable usersRolesTable,
			ImportDevArtifactsConfig config,
			File folder,
			String filePrefix)
		{
			this.usersRolesTable = usersRolesTable;
			this.config = config;
			this.folder = folder;
			this.filePrefix = filePrefix;
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			AdaptationTable usersTable = AdminUtil
				.getDirectoryUsersTable(usersRolesTable.getContainerAdaptation());
			String usersRolesFilename = filePrefix + getTableFilename(usersRolesTable);
			String usersFilename = filePrefix + getTableFilename(usersTable);
			String usersRolesPredicate = config.getUsersRolesPredicate();

			// It's unlikely that all users would want to be processed, but if for some reason they
			// wish to, they'll all be processed
			if (usersRolesPredicate == null)
			{
				processTable(
					null,
					pContext,
					usersRolesTable,
					folder,
					usersRolesFilename,
					null,
					null,
					config);
				processTable(null, pContext, usersTable, folder, usersFilename, null, null, config);

				fixUserPasswordUpdateField(pContext, usersTable, folder, usersFilename, config);
			}
			else
			{
				ImportSpecMode origImportMode = config.getImportMode();
				if (ImportSpecMode.REPLACE.equals(origImportMode))
				{
					// Disable replace mode for import of users since each server has different users
					config.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
				}

				Date dateOfImport = new Date();
				try
				{
					// Do the import on usersRoles
					processTable(
						null,
						pContext,
						usersRolesTable,
						folder,
						usersRolesFilename,
						null,
						null,
						config);

					// Add all of the usersRoles records matching the predicate to a set.
					// This is NOT necessarily the same as all records that were just imported.
					// The set may contain less values than what was actually imported, since
					// the import file could have contained data that was exported from a different
					// predicate than what's being imported. It could also contain records that
					// already existed in EBX that aren't in the file since it was imported in
					// insert/update mode.
					RequestResult requestResult = usersRolesTable
						.createRequestResult(usersRolesPredicate);
					Set<String> usersRolesRecordPKs = new HashSet<>();
					HashSet<String> users = new HashSet<>();
					for (Adaptation usersRolesRecord; (usersRolesRecord = requestResult
						.nextAdaptation()) != null;)
					{
						usersRolesRecordPKs
							.add(usersRolesRecord.getOccurrencePrimaryKey().format());
						// Also store its user for use later
						String user = usersRolesRecord
							.getString(AdminUtil.getDirectoryUsersRolesUserPath());
						users.add(user);
					}

					// Read the file and store the usersRoles pks from it.
					Set<String> fileUsersRolesPKs = new HashSet<>();
					String usersRolesTag = "<"
						+ usersRolesTable.getTablePath().getLastStep().format() + ">";
					String roleTag = "<"
						+ AdminUtil.getDirectoryUsersRolesRolePath().getLastStep().format() + ">";
					String userTag = "<"
						+ AdminUtil.getDirectoryUsersRolesUserPath().getLastStep().format() + ">";

					BufferedReader reader = new BufferedReader(
						new FileReader(new File(folder, usersRolesFilename + ".xml")));
					try
					{
						for (String line; (line = reader.readLine()) != null;)
						{
							String trimmedLine = line.trim();
							if (trimmedLine.startsWith(usersRolesTag))
							{
								line = reader.readLine();
								if (line == null)
								{
									throw OperationException
										.createError("Incomplete " + usersRolesTag + " entry.");
								}
								trimmedLine = line.trim();
								String role = trimmedLine
									.substring(roleTag.length(), trimmedLine.lastIndexOf("<"));

								line = reader.readLine();
								if (line == null)
								{
									throw OperationException
										.createError("Incomplete " + usersRolesTag + " entry.");
								}
								trimmedLine = line.trim();
								String user = trimmedLine
									.substring(userTag.length(), trimmedLine.lastIndexOf("<"));
								fileUsersRolesPKs.add(role + PrimaryKey.SEPARATOR + user);
							}
						}
					}
					finally
					{
						reader.close();
					}

					if (ImportSpecMode.REPLACE.equals(origImportMode))
					{
						// This filter will only allow a record if it's in the users set
						AdaptationFilter usersRolesFilter = getUsersFilter(
							AdminUtil.getDirectoryUsersRolesUserPath(),
							users);
						// Loop through all current usersRoles for the predicate's users
						Request req = usersRolesTable.createRequest();
						req.setSpecificFilter(usersRolesFilter);
						requestResult = req.execute();
						try
						{
							Adaptation usersRolesRecord;
							while ((usersRolesRecord = requestResult.nextAdaptation()) != null)
							{
								// If the usersRole isn't in the input file then it should be deleted.
								// This would normally be handled by a replace.
								if (!fileUsersRolesPKs
									.contains(usersRolesRecord.getOccurrencePrimaryKey().format()))
								{
									Delete.execute(pContext, usersRolesRecord);
								}
							}
						}
						finally
						{
							requestResult.close();
						}
					}

					// Import the users. As with usersRoles above, the file could contain more users than what
					// match the predicate. We'll handle that below.
					processTable(
						null,
						pContext,
						usersTable,
						folder,
						usersFilename,
						null,
						null,
						config);

					fixUserPasswordUpdateField(pContext, usersTable, folder, usersFilename, config);

					// TODO: I think there's a bug in here somewhere. When a user has an &amp; in
					// its name, it breaks this.

					// Loop through the file, similar to how we did for usersRoles.
					// If the user exists in the file but not in EBX matching the predicate,
					// and its create timestamp indicates it was created by dev artifacts,
					// then it's an extra user and should be deleted. Otherwise add it to a set for later use.
					// When the user is deleted, it will delete all its usersRoles records via the table's trigger.
					HashSet<String> fileUsers = new HashSet<>();
					String usersTag = "<" + usersTable.getTablePath().getLastStep().format() + ">";
					String loginTag = "<"
						+ AdminUtil.getDirectoryUsersLoginPath().getLastStep().format() + ">";

					reader = new BufferedReader(
						new FileReader(new File(folder, usersFilename + ".xml")));
					try
					{
						for (String line; (line = reader.readLine()) != null;)
						{
							String trimmedLine = line.trim();
							if (trimmedLine.startsWith(usersTag))
							{
								line = reader.readLine();
								if (line == null)
								{
									throw OperationException
										.createError("Incomplete " + usersTag + " entry.");
								}
								trimmedLine = line.trim();
								String user = trimmedLine
									.substring(loginTag.length(), trimmedLine.lastIndexOf("<"));

								if (users.contains(user))
								{
									fileUsers.add(user);
								}
								else
								{
									Adaptation userRecord = usersTable
										.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(user));
									if (!userRecord.getTimeOfCreation().before(dateOfImport))
									{
										Delete.execute(pContext, userRecord);
									}
								}
							}
						}
					}
					finally
					{
						reader.close();
					}

					if (ImportSpecMode.REPLACE.equals(origImportMode))
					{
						// Now get all users that matched the predicate and if any is
						// not in the input file, delete it
						AdaptationFilter usersFilter = getUsersFilter(
							AdminUtil.getDirectoryUsersLoginPath(),
							users);
						Request req = usersTable.createRequest();
						req.setSpecificFilter(usersFilter);
						requestResult = req.execute();
						try
						{
							Adaptation usersRecord;
							while ((usersRecord = requestResult.nextAdaptation()) != null)
							{
								// If the user isn't in the input file then it should be deleted.
								// This would normally be handled by a replace.
								if (!fileUsers
									.contains(usersRecord.getOccurrencePrimaryKey().format()))
								{
									Delete.execute(pContext, usersRecord);
								}
							}
						}
						finally
						{
							requestResult.close();
						}
					}
				}
				finally
				{
					if (ImportSpecMode.REPLACE.equals(origImportMode))
					{
						// Set the mode back to what it should be
						config.setImportMode(origImportMode);
					}
				}
			}
		}
	}

	private abstract class CreateDataSetProcedure implements Procedure
	{
		protected AdaptationReference dataSetName;
		protected Adaptation dataSet;

		protected CreateDataSetProcedure(AdaptationReference dataSetName)
		{
			this.dataSetName = dataSetName;
		}

		public Adaptation getDataSet()
		{
			return this.dataSet;
		}
	}

	private class CreateRootDataSetProcedure extends CreateDataSetProcedure
	{
		private SchemaLocation schemaLocation;

		public CreateRootDataSetProcedure(
			AdaptationReference dataSetName,
			SchemaLocation schemaLocation)
		{
			super(dataSetName);
			this.schemaLocation = schemaLocation;
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			dataSet = pContext.doCreateRoot(
				schemaLocation,
				dataSetName,
				pContext.getSession().getUserReference());
		}
	}

	private class CreateChildDataSetProcedure extends CreateDataSetProcedure
	{
		private AdaptationName parentDataSetName;

		public CreateChildDataSetProcedure(
			AdaptationReference dataSetName,
			AdaptationName parentDataSetName)
		{
			super(dataSetName);
			this.parentDataSetName = parentDataSetName;
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			dataSet = pContext.doCreateChild(
				parentDataSetName,
				dataSetName,
				pContext.getSession().getUserReference());
		}
	}

	private class SetDataSetPropertiesProcedure implements Procedure
	{
		private Adaptation dataSet;
		private String label;
		private String description;
		private String owner;

		public SetDataSetPropertiesProcedure(
			Adaptation dataSet,
			String label,
			String description,
			String owner)
		{
			this.dataSet = dataSet;
			this.label = label;
			this.description = description;
			this.owner = owner;
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			pContext.setAllPrivileges(true);

			Locale locale = pContext.getSession().getLocale();
			if (!ObjectUtils.equals(label, dataSet.getLabel(locale)))
			{
				pContext.setInstanceLabel(
					dataSet,
					label == null ? null : UserMessage.createInfo(label));
			}
			if (!ObjectUtils.equals(description, dataSet.getDescription(locale)))
			{
				pContext.setInstanceDescription(
					dataSet,
					description == null ? null : UserMessage.createInfo(description));
			}
			// Unlike label & description, owner can't be set to null so just ignore if it's null
			if (owner != null && !owner.equals(dataSet.getOwner().format()))
			{
				Profile ownerProfile = Profile.parse(owner);
				pContext.setInstanceOwner(dataSet, ownerProfile);
			}
		}
	}
}

/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.admin.devartifacts;

import com.orchestranetworks.service.*;

/**
 * Constants for Dev Artifacts
 */
public interface DevArtifactsConstants
{
	static final String DATA_PREFIX = "Data_";
	static final String DATA_SET_ARCHIVE_PREFIX = "DSet_";
	static final String PERMISSIONS_DATA_SET_PREFIX = "DSet_Perm_";
	static final String PERMISSIONS_DATA_SPACE_PREFIX = "DSpc_Perm_";
	static final String WORKFLOW_PREFIX = "WF_";
	static final String PERSPECTIVE_PREFIX = "Persp_";
	static final String DATA_SET_PROPERTIES_SUFFIX = ".properties";
	static final String DATA_SET_PROPERTY_LABEL = "label";
	static final String DATA_SET_PROPERTY_DESCRIPTION = "description";
	static final String DATA_SET_PROPERTY_OWNER = "owner";
	static final String DATA_SET_PROPERTY_PARENT_DATA_SET = "parentDataSet";
	static final String DATA_SET_PROPERTY_CHILD_DATA_SETS = "childDataSets";
	static final String ENV_COPY_ARCHIVE_PREFIX = "EnvCopy_";
	static final String ADDON_ADIX_PREFIX = "Addon_Adix_";
	static final String ADDON_DAQA_PREFIX = "Addon_Daqa_";
	static final String ADDON_DAMA_PREFIX = "Addon_Dama_";
	static final String ADDON_DQID_PREFIX = "Addon_Dqid_";
	static final String ADDON_HMFH_PREFIX = "Addon_Hmfh_";
	static final String ADDON_RPFL_PREFIX = "Addon_Rpfl_";

	static final String SERVICE_COMPLETE_MSG = "Service complete.";

	static final String DATA_SET_PROPERTIES_FILE_CHILD_DATA_SETS_SEPARATOR = "\\|";

	static final LoggingCategory LOG = LoggingCategory.getKernel();

	static final String PROPERTY_DATA_FOLDER = "dataFolder";
	static final String PROPERTY_PERMISSIONS_FOLDER = "permissionsFolder";
	static final String PROPERTY_WORKFLOWS_FOLDER = "workflowsFolder";
	static final String PROPERTY_ADMIN_FOLDER = "adminFolder";
	static final String PROPERTY_COPY_ENVIRONMENT_FOLDER = "copyEnvironmentFolder";
	static final String PROPERTY_LINE_SEPARATOR = "lineSeparator";
	static final String PROPERTY_DEFAULT_ENVIRONMENT_COPY = "defaultEnvironmentCopy";
	static final String PROPERTY_DEFAULT_PUBLISH_WORKFLOW_MODELS = "defaultPublishWorkflowModels";
	static final String PROPERTY_DEFAULT_REPLACE_MODE = "defaultReplaceMode";
	static final String PROPERTY_DEFAULT_SKIP_NON_EXISTING_FILES = "defaultSkipNonExistingFiles";
	static final String PROPERTY_CREATE_DATA_SPACES = "createDataSpaces";
	static final String PROPERTY_CREATE_DATA_SETS = "createDataSets";
	static final String PROPERTY_TABLES_FOR_DATA = "tablesForData";
	static final String PROPERTY_DATA_SET_PERMISSIONS_IN_CHILD_DATA_SPACES = "dataSetPermissionsInChildDataSpaces";
	static final String PROPERTY_DATA_SETS_FOR_PERMISSIONS = "dataSetsForPermissions";
	static final String PROPERTY_DATA_SPACES_FOR_PERMISSIONS = "dataSpacesForPermissions";
	static final String PROPERTY_ADMIN_DATA_SET_PERMISSIONS = "adminDataSetPermissions";
	static final String PROPERTY_DIRECTORY = "directory";
	static final String PROPERTY_GLOBAL_PERMISSIONS = "globalPermissions";
	static final String PROPERTY_VIEWS = "views";
	static final String PROPERTY_PUBLISHED_VIEWS_ONLY = "publishedViewsOnly";
	static final String PROPERTY_TASKS = "tasks";
	static final String PROPERTY_PERSPECTIVES = "perspectives";
	static final String PROPERTY_USERS_ROLES_PREDICATE = "usersRolesPredicate";
	static final String PROPERTY_MASTER_WORKFLOW_MODELS = "masterWorkflowModels";
	static final String PROPERTY_WORKFLOW_MODELS_TO_NOT_PUBLISH = "workflowModelsToNotPublish";

	static final String PROPERTY_ADDON_ADIX = "addonAdix";
	static final String PROPERTY_ADDON_DAQA = "addonDaqa";
	static final String PROPERTY_ADDON_DAMA = "addonDama";
	static final String PROPERTY_ADDON_DQID = "addonDqid";
	static final String PROPERTY_ADDON_HMFH = "addonHmfh";
	static final String PROPERTY_ADDON_RPFL = "addonRpfl";

	static final String WILDCARD = "*";
	static final String LINE_SEP_TYPE_WINDOWS = "windows";
	static final String LINE_SEP_TYPE_UNIX = "unix";

	static final String ADDON_REFERENCE_DATA_GROUP = "ReferenceData";
	static final String ADDON_REFERENCE_DATA_FIELD_NAME = "uuid";
	static final String ADDON_REFERENCE_DATA_FIELD_VALUE = "[ON]";
}

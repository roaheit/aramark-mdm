package com.orchestranetworks.ps.admin.devartifacts;

import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;

public class ImportDevArtifactsDSDeclaration extends TechAdminOnlyServiceDeclaration
{

	public ImportDevArtifactsDSDeclaration(String moduleName)
	{
		super(
			moduleName == null ? ServiceKey.forName("ImportDevArtifactsDS")
				: ServiceKey.forModuleServiceName(moduleName, "ImportDevArtifactsDS"),
			null,
			"Import Dev Artifacts",
			null,
			null);
	}

	@Override
	public UserService<DataspaceEntitySelection> createUserService()
	{
		return new ImportDevArtifactsUserService<>();
	}

}

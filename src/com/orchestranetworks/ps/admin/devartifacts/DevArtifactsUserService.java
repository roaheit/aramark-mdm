/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.admin.devartifacts;

import java.util.*;

import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.schema.dynamic.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.base.*;
import com.orchestranetworks.ui.form.*;
import com.orchestranetworks.ui.form.widget.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;

/**
 * An abstract servlet class for presenting a form to the user, allowing them to configure the
 * import/export
 * before launching it
 */
public abstract class DevArtifactsUserService<S extends DataspaceEntitySelection>
	extends
	AbstractUserService<S>
	implements DevArtifactsConstants
{
	private String propertiesFile = DevArtifactsPropertyFileHelper.DEFAULT_PROPERTIES_FILE;
	// This service defines a single object named "input".
	private final static ObjectKey _InputObjectKey = ObjectKey.forName("input");
	private final Path envCopyPath = Path.SELF
		.add(Path.parse(DevArtifactsBase.PARAM_ENVIRONMENT_COPY));

	private Map<String, String[]> paramMap = new HashMap<>();
	private DevArtifactsBase impl = createImpl();

	protected DevArtifactsConfig config;

	@Override
	public void writeInputPane(UserServicePaneContext paneContext, UserServicePaneWriter aWriter)
	{
		String info = getInformation();
		if (info != null)
		{
			aWriter.add_cr("<div style='margin: 5px'>");
			aWriter.add_cr("  <p>" + info + "</p>");
			aWriter.add_cr("</div>");
		}
		writeForm(paneContext, aWriter, _InputObjectKey);
	}

	protected String getInformation()
	{
		return null;
	}

	@Override
	public void setupObjectContext(
		UserServiceSetupObjectContext<S> aContext,
		UserServiceObjectContextBuilder aBuilder)
	{
		if (aContext.isInitialDisplay())
		{
			try
			{
				config = impl.getConfigFactory()
					.createConfig(aContext.getRepository(), aContext.getSession(), paramMap);
			}
			catch (OperationException ex)
			{
				String msg = "Error creating config.";
				LOG.error(msg, ex);
				throw new RuntimeException(ex);
			}
			BeanDefinition def = context.defineObject(aBuilder, _InputObjectKey);
			defineElement(
				def,
				envCopyPath,
				"Environment Copy",
				SchemaTypeName.XS_BOOLEAN,
				Boolean.valueOf(config.isEnvironmentCopy()));
			addExtraInputs(aContext, def, _InputObjectKey);
		}
	}

	protected void addExtraInputs(
		UserServiceSetupObjectContext<S> aContext,
		BeanDefinition def,
		ObjectKey key)
	{
		// do nothing
	}

	protected List<Path> addInformationFields(BeanDefinition def)
	{
		return null;
	}

	public String getPropertiesFile()
	{
		return this.propertiesFile;
	}

	public void setPropertiesFile(String propertiesFile)
	{
		this.propertiesFile = propertiesFile;
	}

	@Override
	public void landService()
	{
		alert(SERVICE_COMPLETE_MSG);
		super.landService();
	}

	/**
	 * Get the factory to use to create the configuration
	 * 
	 * @return the factory
	 */
	public abstract DevArtifactsBase createImpl();

	@Override
	public void execute(Session session) throws OperationException
	{
		// Make sure only admins can execute
		if (!session.isUserInRole(Role.ADMINISTRATOR))
		{
			String msg = "User doesn't have permission to execute service.";
			LOG.error(msg);
			alert(msg);
			return;
		}

		try
		{
			Repository repo = context.getPaneContext().getRepository();
			impl.getConfigFactory().updateConfig(config, repo, session, paramMap);
			if (config.isEnvironmentCopy())
			{
				impl.copyEnvironment(repo, session, config);
			}
			// Note that for imports, this will do nothing but for exports, we want to additionally export the normal artifacts
			impl.processArtifacts(repo, session, config);
		}
		catch (OperationException ex)
		{
			String msg = "Error processing artifacts.";
			LOG.error(msg, ex);
			alert(msg + ": " + ex.getMessage());
			return;
		}
	}

	protected static void writeCheckBox(
		UserServicePaneWriter aWriter,
		Path path,
		String rowId,
		String jsFunction)
	{
		UICheckBox checkBox = aWriter.newCheckBox(path);
		if (jsFunction != null)
		{
			checkBox.setActionOnAfterValueChanged(JsFunctionCall.on(jsFunction));
		}
		UIFormRow formRow = aWriter.newFormRow(path);
		if (rowId != null)
		{
			formRow.setRowId(rowId);
		}
		aWriter.addFormRow(formRow, checkBox);
	}

	@Override
	protected UserServiceEventOutcome readValues(UserServiceObjectContext fromContext)
	{
		for (ObjectKey key : context.getObjectKeys())
		{
			readValues(fromContext, key, null);
		}
		return super.readValues(fromContext);
	}

	private void readValues(UserServiceObjectContext fromContext, ObjectKey key, SchemaNode node)
	{
		if (node == null)
			node = fromContext.getValueContext(key).getNode();
		if (node.isComplex())
		{
			SchemaNode[] children = node.getNodeChildren();
			for (SchemaNode childNode : children)
			{
				readValues(fromContext, key, childNode);
			}
		}
		else
		{
			Path path = Path.SELF.add(node.getPathInAdaptation().getSubPath(1));
			paramMap.put(
				path.getLastStep().format(),
				new String[] { String.valueOf(fromContext.getValueContext(key, path).getValue()) });
		}
	}
}

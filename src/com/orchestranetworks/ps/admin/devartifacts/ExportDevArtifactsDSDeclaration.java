package com.orchestranetworks.ps.admin.devartifacts;

import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;

public class ExportDevArtifactsDSDeclaration extends TechAdminOnlyServiceDeclaration
{

	public ExportDevArtifactsDSDeclaration(String moduleName)
	{
		super(
			moduleName == null ? ServiceKey.forName("ExportDevArtifactsDS")
				: ServiceKey.forModuleServiceName(moduleName, "ExportDevArtifactsDS"),
			null,
			"Export Dev Artifacts",
			null,
			null);
	}

	@Override
	public UserService<DataspaceEntitySelection> createUserService()
	{
		return new ExportDevArtifactsUserService<>();
	}

}

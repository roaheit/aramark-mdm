/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.admin.devartifacts;

/**
 * A servlet for exporting of the dev artifacts.
 * This is no longer used by the User Service but it still needed for the command line interface.
 */
public class ExportDevArtifactsService extends DevArtifactsService
{
	private static final long serialVersionUID = 1L;

	@Override
	protected DevArtifactsBase createImpl()
	{
		return new ExportDevArtifactsImpl();
	}
}

/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.admin.devartifacts;

import com.orchestranetworks.schema.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;

/**
 * A user service for exporting of the dev artifacts
 */
public class ExportDevArtifactsUserService<S extends DataspaceEntitySelection>
	extends
	DevArtifactsUserService<S>
{
	@Override
	protected String getInformation()
	{
		return "Artifacts specified in properties file will be exported, as well as all workflows.";
	}

	@Override
	public DevArtifactsBase createImpl()
	{
		return new ExportDevArtifactsImpl();
	}

	@Override
	protected void writeNode(UserServicePaneWriter aWriter, SchemaNode node, boolean top)
	{
		Step lastStep = node.getPathInAdaptation().getLastStep();
		String paramStr = lastStep.format();
		if (ImportDevArtifactsImpl.PARAM_ENVIRONMENT_COPY.equals(paramStr))
		{
			writeCheckBox(aWriter, lastStep.toSelfPath(), paramStr, null);
		}
		else
		{
			super.writeNode(aWriter, node, top);
		}
	}
}

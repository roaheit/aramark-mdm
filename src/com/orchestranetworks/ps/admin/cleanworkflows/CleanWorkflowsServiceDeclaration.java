package com.orchestranetworks.ps.admin.cleanworkflows;

import com.orchestranetworks.ps.admin.servicepermission.*;
import com.orchestranetworks.ps.constants.*;
import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;
import com.orchestranetworks.userservice.declaration.*;

/**
 * Declares the {@link CleanWorkflowsUserService}.
 * 
 * This service is not available in production environments.
 * See {@link #defineActivation(ActivationContextOnDataspace)}.
 */
public class CleanWorkflowsServiceDeclaration extends TechAdminOnlyServiceDeclaration
{
	private String backendModesToDisable;

	/**
	 * Create the service declaration, using the given module name,
	 * and make it not available in production environments.
	 * 
	 * This is equivalent of calling {@link #CleanWorkflowsServiceDeclaration(String, String)}
	 * with {@link com.orchestranetworks.ps.constants.CommonConstants#BACKEND_MODE_PRODUCTION}.
	 * 
	 * @param moduleName the module name
	 */
	public CleanWorkflowsServiceDeclaration(String moduleName)
	{
		this(moduleName, CommonConstants.BACKEND_MODE_PRODUCTION);
	}

	/**
	 * Create the service declaration, using the given module name
	 * 
	 * @param moduleName the module name
	 * @param backendModesToDisable the backend modes to disable the service for
	 */
	public CleanWorkflowsServiceDeclaration(String moduleName, String backendModesToDisable)
	{
		super(
			moduleName == null ? ServiceKey.forName("CleanWorkflows")
				: ServiceKey.forModuleServiceName(moduleName, "CleanWorkflows"),
			null,
			"Clean Workflows",
			null,
			"");
		this.backendModesToDisable = backendModesToDisable;
	}

	@Override
	public UserService<DataspaceEntitySelection> createUserService()
	{
		// Wrap the service in a NoUI since there's no UI associated with it
		return new UserServiceNoUI<>(new CleanWorkflowsUserService());
	}

	@Override
	public void defineActivation(ActivationContextOnDataspace context)
	{
		super.defineActivation(context);
		// Make this service not available based on the specified backend modes
		if (backendModesToDisable != null)
		{
			context
				.setPermissionRule(new BackendModeServicePermissionRule<>(backendModesToDisable));
		}
	}
}

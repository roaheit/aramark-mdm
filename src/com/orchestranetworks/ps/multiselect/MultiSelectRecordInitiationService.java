/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.multiselect;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.*;

/**
 */
@SuppressWarnings("serial")
public class MultiSelectRecordInitiationService extends HttpServlet
{

	private int joinTableParentPKPosition = 0;
	private Path joinTableFKPath;
	private String selectLabel = "Select";
	private String selectionServiceName = MultiSelectRecordSelectionService.DEFAULT_SERVICE_NAME;

	private static final String FRAME_ID = "initiate_records_frame";

	private TrackingInfoHelper trackingInfoHelper;
	private Path joinTablePath;

	public MultiSelectRecordInitiationService()
	{
		this(null, null);
	}

	public MultiSelectRecordInitiationService(
		TrackingInfoHelper baseTrackingInfoHelper,
		Path joinTablePath)
	{
		this.trackingInfoHelper = MultiSelectUtil.createTrackingInfoHelper(baseTrackingInfoHelper);
		this.joinTablePath = joinTablePath;
	}

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{

		ServiceContext sContext = ServiceContext.getServiceContext(request);
		Session session = sContext.getSession();
		//		if (session.getInteraction(true) == null)
		//		{
		//			return;
		//		}
		@SuppressWarnings("unchecked")
		final List<Adaptation> records = sContext.getSelectedOccurrences();
		AdaptationTable table = records.get(0).getContainerTable();
		String pks = "";
		for (Adaptation record : records)
		{
			pks = pks + record.getOccurrencePrimaryKey().format() + ":";
		}
		pks = pks.substring(0, pks.lastIndexOf(":"));

		AdaptationTable joinTable = records.get(0).getContainer().getTable(joinTablePath);

		String trackingInfo = session.getTrackingInfo();
		if (trackingInfo != null)
		{
			trackingInfoHelper.initTrackingInfo(trackingInfo);
		}

		UIServiceComponentWriter writer = sContext.getUIComponentWriter();

		UIHttpManagerComponent uiMgr = sContext.createWebComponentForSubSession();
		uiMgr.selectNode(joinTable.getContainerAdaptation(), joinTable.getTablePath());
		//uiMgr.setTrackingInfo(session.getTrackingInfo());
		uiMgr.setTrackingInfo(trackingInfoHelper.createTrackingInfo());
		uiMgr.setService(ServiceKey.forName("MultiSelectServiceTypeSelection"));
		String url = createURL(uiMgr.getURIWithParameters(), table, pks, request);

		//writer.addJS_cr("window.location.href='" + url + "';");

		writer.add_cr("<iframe id='" + FRAME_ID + "' name='" + FRAME_ID
			+ "' src='' style='border: none; width:100%'></iframe>");
		writer.addJS_addResizeWorkspaceListener("resizeFrame");
		// Setting the url directly in the iframe declaration causes 2 subsessions to be created,
		// and some unpredictable behavior. So it should be set via js code after creating the iframe.
		writer.addJS_cr("document.getElementById('" + FRAME_ID + "').src='" + url + "';");

		// This is a hack to remove the header since EBX provides no way to do it.
		writer.addJS_cr("window.onload=removeHeader();");

		writer.addJS_cr("function resizeFrame(size)");
		writer.addJS_cr("{");
		writer.addJS_cr("  var frameElement = document.getElementById('" + FRAME_ID + "');");
		writer.addJS_cr("  frameElement.style.height = size.h + 'px';");
		writer.addJS_cr("}");

		writer.addJS_cr("function removeHeader()");
		writer.addJS_cr("{");
		writer.addJS_cr("  document.getElementById('ebx_WorkspaceHeader').style.display='none';");
		writer.addJS_cr("}");
	}

	private String createURL(
		String serviceURL,
		AdaptationTable table,
		String pks,
		HttpServletRequest request)
	{
		Adaptation dataSet = table.getContainerAdaptation();

		StringBuilder bldr = new StringBuilder(serviceURL);
		bldr.append("&");
		bldr.append(MultiSelectRecordSelectionService.PARAM_PARENT_TABLE_DATA_SPACE);
		bldr.append("=");
		bldr.append(dataSet.getHome().getKey().getName());
		bldr.append("&");
		bldr.append(MultiSelectRecordSelectionService.PARAM_PARENT_TABLE_DATA_SET);
		bldr.append("=");
		bldr.append(dataSet.getAdaptationName().getStringName());
		bldr.append("&");
		bldr.append(MultiSelectRecordSelectionService.PARAM_PARENT_TABLE_PATH);
		bldr.append("=");
		bldr.append(table.getTablePath().format());
		bldr.append("&");
		bldr.append(MultiSelectRecordSelectionService.PARAM_PARENT_RECORD_PK);
		bldr.append("=");
		bldr.append(pks);
		bldr.append("&");
		bldr.append(MultiSelectRecordSelectionService.PARAM_JOIN_TABLE_PARENT_PK_POSITION);
		bldr.append("=");
		bldr.append(joinTableParentPKPosition);
		bldr.append("&");
		bldr.append(MultiSelectRecordSelectionService.PARAM_JOIN_TABLE_FK_PATH);
		bldr.append("=");
		bldr.append("/catalogGroup");
		return bldr.toString();
	}

	public int getJoinTableParentPKPosition()
	{
		return joinTableParentPKPosition;
	}

	public void setJoinTableParentPKPosition(int joinTableParentPKPosition)
	{
		this.joinTableParentPKPosition = joinTableParentPKPosition;
	}

	public Path getJoinTableFKPath()
	{
		return joinTableFKPath;
	}

	public void setJoinTableFKPath(Path joinTableFKPath)
	{
		this.joinTableFKPath = joinTableFKPath;
	}

	public String getSelectLabel()
	{
		return selectLabel;
	}

	public void setSelectLabel(String selectLabel)
	{
		this.selectLabel = selectLabel;
	}

	public String getSelectionServiceName()
	{
		return selectionServiceName;
	}

	public void setSelectionServiceName(String selectionServiceName)
	{
		this.selectionServiceName = selectionServiceName;
	}

}

package com.orchestranetworks.ps.trigger;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.trigger.TriggerActionValidator.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.schema.trigger.*;
import com.orchestranetworks.service.*;

/**
 * Prevents creations and modifications of the parent field when the parent hierarchy contains a reference
 * to this record. This can be used in conjunction with a {@link com.orchestranetworks.ps.constraint.AcyclicConstraint}.
 * The constraint essentially does the same thing, but when importing data, it will get imported anyway even though there
 * are errors. In cases where this could cause a serious issue (like an infinite loop), this trigger can be used as a
 * precaution against that.
 * 
 * This assumes a single-table hierarchy where there is one field that represents the foreign key to the parent.
 * If that isn't sufficient, then it will need to be modified to support more elaborate requirements.
 */
public class AcyclicTableTrigger extends BaseTableTrigger
{
	// Acyclic Constraint shows the full path of the cycle but here we're just doing something simple.
	private static final String SELF_REFERENTIAL_MESSAGE = "Record can't have itself as its parent.";
	private static final String CYCLE_FOUND_MESSAGE = "Cycle detected in record hierarchy.";

	private Path parentPath;

	@Override
	protected TriggerActionValidator createTriggerActionValidator(TriggerAction triggerAction)
	{
		return new TriggerActionValidator()
		{
			@Override
			public UserMessage validateTriggerAction(
				Session session,
				ValueContext valueContext,
				ValueChanges valueChanges,
				TriggerAction action)
				throws OperationException
			{
				// Don't need to do anything for deletes
				if (action == TriggerAction.DELETE)
				{
					return null;
				}
				// If it's a create or if the parent was changed (from a modify)
				if (action == TriggerAction.CREATE || valueChanges.getChange(parentPath) != null)
				{
					String parentPK = (String) valueContext.getValue(parentPath);
					if (parentPK != null)
					{
						String thisPK = valueContext.getAdaptationTable()
							.computePrimaryKey(valueContext)
							.format();
						// Detect if this record has itself as the parent
						if (parentPK.equals(thisPK))
						{
							return UserMessage.createError(SELF_REFERENTIAL_MESSAGE);
						}

						// Look up the tree to see if it's referenced there.
						AdaptationTable table = valueContext.getAdaptationTable();
						boolean found = false;
						while (!found && parentPK != null)
						{
							Adaptation parentRecord = table
								.lookupAdaptationByPrimaryKey(PrimaryKey.parseString(parentPK));
							if (parentRecord == null)
							{
								parentPK = null;
							}
							else
							{
								parentPK = parentRecord.getString(parentPath);
								if (parentPK != null && parentPK.equals(thisPK))
								{
									found = true;
								}
							}
						}
						if (found)
						{
							return UserMessage.createError(CYCLE_FOUND_MESSAGE);
						}
					}
				}
				return null;
			}
		};
	}

	public Path getParentPath()
	{
		return parentPath;
	}

	public void setParentPath(Path parentPath)
	{
		this.parentPath = parentPath;
	}
}

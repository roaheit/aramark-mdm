package com.orchestranetworks.ps.logging;

import java.util.*;

import com.onwbp.base.misc.*;
import com.onwbp.org.apache.log4j.*;
import com.orchestranetworks.ps.admin.*;

public class CustomLogger
{
	//	@SuppressWarnings("unused")
	private static CustomLogger customLogger = new CustomLogger();
	private Category category;
	private String ebxHome;

	public static Category newInstance()
	{
		return customLogger.category;
	}

	private CustomLogger()
	{
		try
		{
			String propertiesFile = System.getProperty(
				LoggingConstants.EBX_SYSTEM_PROPERTY_NAME,
				LoggingConstants.EBX_PROPERTIES_FILE_NAME);

			if (propertiesFile != null)
			{
				PropertyFileHelper propertyHelper = new PropertyFileHelper(propertiesFile);
				Properties props = propertyHelper.getProperties();
				String categoryName = props.getProperty(
					LoggingConstants.LOGGING_CATEGORY_NAME_PROPERTY,
					LoggingConstants.CATEGORY_DEFAULT_NAME);

				String logFilename = props.getProperty(LoggingConstants.LOGGING_FILE_NAME_PROPERTY);
				String treshold = props.getProperty(LoggingConstants.LOGGING_THRESHOLD_PROPERTY);
				String layout = props.getProperty(LoggingConstants.LOGGING_LAYOUT_PROPERTY);

				String appenderType = props.getProperty(LoggingConstants.LOGGING_APPENDER_TYPE);
				if (StringUtils.isEmpty(appenderType))
				{
					appenderType = "File";
				}

				Appender appender = null;
				PatternLayout patterLayout = StringUtils.isEmpty(layout)
					? new PatternLayout("%d %-5p [%c{1}] %m%n")
					: new PatternLayout(layout);

				String ebxLogFolder = props
					.getProperty(LoggingConstants.EBX_LOG_DIRECTORY_PROPERTY);

				String fileName = StringUtils.isEmpty(logFilename)
					? LoggingConstants.LOGGING_DEFAULT_FILE_NAME
					: logFilename;

				if (!StringUtils.isEmpty(ebxLogFolder))
				{
					fileName = StringUtils.replace(ebxLogFolder, "${ebx.home}", getEbxHome(props))
						.concat("/")
						.concat(fileName);
				}

				Priority priority = StringUtils.isEmpty(treshold) ? Priority.INFO
					: Priority.toPriority(treshold);

				category = Category.getInstance(categoryName);
				if (LoggingConstants.APPENDER_FILE.equalsIgnoreCase(appenderType))
				{
					FileAppender fileAppender = new FileAppender();
					fileAppender.setFile(fileName);
					fileAppender.setLayout(patterLayout);
					fileAppender.setThreshold(priority);
					fileAppender.setAppend(true);
					fileAppender.activateOptions();
					appender = fileAppender;
				}
				else
				{
					ConsoleAppender consoleAppender = new ConsoleAppender();
					consoleAppender.setName(categoryName);
					consoleAppender.setThreshold(priority);
					consoleAppender.setLayout(patterLayout);
					appender = consoleAppender;
				}
				category.addAppender(appender);
				category.setPriority(Priority.toPriority(treshold));
			}
			else
			{
				category = Category.getInstance(LoggingConstants.CATEGORY_DEFAULT_NAME);
			}
		}
		catch (Exception ex)
		{
			throw new RuntimeException(ex);
		}
	}

	private String getEbxHome(Properties props)
	{
		if (ebxHome == null)
		{
			ebxHome = System.getProperty(LoggingConstants.EBX_HOME_PROPERTY);
			if (StringUtils.isEmpty(ebxHome))
			{
				ebxHome = props.getProperty(LoggingConstants.EBX_HOME_PROPERTY);
			}
		}
		return ebxHome;
	}

	public boolean isDebug()
	{
		return customLogger.category.isDebugEnabled();
	}

	public void debug(String message)
	{
		if (customLogger.category.isDebugEnabled())
		{
			customLogger.category.debug(message);
		}
	}

	public void debug(String message, Exception exp)
	{
		if (customLogger.category.isDebugEnabled())
		{
			customLogger.category.debug(message, exp);
		}
	}

	public void info(String message)
	{
		if (customLogger.category.isInfoEnabled())
		{
			customLogger.category.info(message);
		}
	}

	public void info(String message, Exception exp)
	{
		if (customLogger.category.isInfoEnabled())
		{
			customLogger.category.info(message, exp);
		}
	}

}

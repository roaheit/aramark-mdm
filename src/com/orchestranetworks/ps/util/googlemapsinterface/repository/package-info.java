/**
 * Provides utility class to deal with data spaces, data sets and records.<br>
 *
 * @since 1.0.0
 */
package com.orchestranetworks.ps.util.googlemapsinterface.repository;
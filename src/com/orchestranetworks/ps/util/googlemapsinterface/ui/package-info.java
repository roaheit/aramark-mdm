/**
 * Provides an utility class and other useful classes related to the UI.<br>
 *
 * @since 1.0.0
 */
package com.orchestranetworks.ps.util.googlemapsinterface.ui;
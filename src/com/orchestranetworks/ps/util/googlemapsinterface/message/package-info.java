/**
 * Provides a Messages class to get localized String or UserMessage from a properties file.<br>
 * 
 * @since 1.1.0
 */
package com.orchestranetworks.ps.util.googlemapsinterface.message;
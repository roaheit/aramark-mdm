/**
 * Provides a User Service to normalize addresses of selected records thanks to the Google Maps API.
 *
 * @since 1.0.0
 */
package com.orchestranetworks.ps.util.googlemapsinterface.googlemaps.addressnormalization;
package com.orchestranetworks.ps.util;

import java.util.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.schema.info.*;
import com.orchestranetworks.service.*;

/**
 *
 * Utility class to manipulate records and instances.
 * @author MCH
 */
@Deprecated
// TODO: Most of this functionality already exists in AdaptationUtil with either the same method
// names or alternate names. Need to merge in any missing functionality
public class AdaptationUtils
{

	/**
	 * @author US-Team
	 *
	 * Gets all of the tables for a data set.
	 * This is equivalent of <code>getAllTables(dataSet, dataSet.getSchemaNode())</code>
	 *
	 * @param pDataSet the data set
	 * @return the tables for the data set, or an empty list if none exist
	 */
	public static List<AdaptationTable> getAllTables(final Adaptation pDataSet)
	{
		return getAllTables(pDataSet, pDataSet.getSchemaNode());
	}

	/**
	 * @author US-Team
	 *
	 * Gets all of the tables for a data set under the given node.
	 *
	 * @param pDataSet the data set
	 * @param pFromNode the schema node to look under
	 * @return the tables for the data set under the given node, or an empty list if none exist
	 */
	public static List<AdaptationTable> getAllTables(
		final Adaptation pDataSet,
		final SchemaNode pFromNode)
	{
		ArrayList<AdaptationTable> tables = new ArrayList<>();
		for (SchemaNode child : pFromNode.getNodeChildren())
		{
			if (child.isTableNode())
			{
				tables.add(pDataSet.getTable(child.getPathInSchema()));
			}
			else
			{
				tables.addAll(getAllTables(pDataSet, child));
			}
		}
		return tables;
	}

	/**
	 * Gets the associated records.
	 *
	 * @author MCH
	 * @param pRecord
	 *            the record
	 * @param pPath
	 *            path to an association from the occurrence root
	 * @return the list of records linked by the association
	 * @throws OperationException
	 *             the operation exception
	 */
	public static List<Adaptation> getAssociatedRecords(final Adaptation pRecord, final Path pPath)
		throws OperationException
	{
		if (pRecord.isSchemaInstance())
		{
			throw OperationException
				.createError("The adaptation in parameters must be an occurrence");
		}

		SchemaNode association = getNode(pRecord, pPath);
		AssociationLink link = association.getAssociationLink();
		if (link == null)
		{
			throw OperationException
				.createError("The node at path " + pPath.format() + " is not an association");
		}

		RequestResult result = link.getAssociationResult(pRecord);
		return getListOfRecordFromRequestResult(result);
	}

	/**
	 * Gets the data set.
	 *
	 * @param pDataSpace the data space
	 * @param pDataSet the data set
	 * @return the data set
	 */
	public static Adaptation getDataSet(final AdaptationHome pDataSpace, final String pDataSet)
	{
		return pDataSpace.findAdaptationOrNull(AdaptationName.forName(pDataSet));
	}

	/**
	 * Gets the data set.
	 *
	 * @author MCH
	 * @param pRepository the repository
	 * @param pDataSpace the data space
	 * @param pDataSet the data set
	 * @return the data set
	 * @throws OperationException the operation exception
	 */
	public static Adaptation getDataSet(
		final Repository pRepository,
		final String pDataSpace,
		final String pDataSet) throws OperationException
	{
		AdaptationHome home = getDataSpace(pRepository, pDataSpace);

		if (home == null)
		{
			throw OperationException.createError("Data space '" + pDataSpace + "' not found");
		}

		Adaptation instance = getDataSet(home, pDataSet);

		return instance;
	}

	/**
	 * Gets the data set.
	 *
	 * @author MCH
	 *
	 *         Get a data set from a ValueContext, a data space name and a data
	 *         set name.
	 *
	 *         If no data space name is specified, the data set will be searched
	 *         in the current data space.
	 *
	 *         If no data set name is specified, the current data set name will
	 *         be searched.
	 * @param pContext            The value context
	 * @param pDataSpace            the data space where to find the data set, null if in current
	 *            data space
	 * @param pDataSet            the data set to find, if null current data set name will be
	 *            searched.
	 * @return the data set
	 * @throws OperationException             if data space or data set not found
	 */
	public static Adaptation getDataSet(
		final ValueContext pContext,
		final String pDataSpace,
		final String pDataSet) throws OperationException
	{

		Adaptation instance = pContext.getAdaptationInstance();
		AdaptationName name = instance.getAdaptationName();

		AdaptationHome home = pContext.getHome();
		if (pDataSpace != null)
		{
			home = getDataSpace(home.getRepository(), pDataSpace);
		}

		if (home == null)
		{
			throw OperationException.createError("Data space '" + pDataSpace + "' not found");
		}

		if (pDataSet != null)
		{
			name = AdaptationName.forName(pDataSet);
			instance = home.findAdaptationOrNull(name);
			if (instance == null)
			{
				throw OperationException.createError(
					"Data set '" + pDataSet + "' not found in data space '" + pDataSpace);
			}
		}
		else
		{
			instance = home.findAdaptationOrNull(name);
		}

		return instance;
	}

	/**
	 * Gets the data space.
	 *
	 * @author MCH.
	 * @param pRepository the repository
	 * @param pDataSpace the data space
	 * @return the adaptation home
	 */
	public static AdaptationHome getDataSpace(final Repository pRepository, final String pDataSpace)
	{
		return pRepository.lookupHome(HomeKey.forBranchName(pDataSpace));
	}

	/**
	 * Gets the list of records from an association or a selection node.
	 *
	 * @author MCH
	 * @throws OperationException
	 *
	 * @param pRecord the record
	 * @param pPath the xpath of an association or a selection node.
	 * @return the request result attached to the association or the selection node.
	 */
	public static List<Adaptation> getLinkedRecords(final Adaptation pRecord, final Path pPath)
		throws OperationException
	{
		RequestResult result = requestLinkedRecords(pRecord, pPath);
		try
		{
			return getListOfRecordFromRequestResult(result);
		}
		finally
		{
			result.close();
		}
	}

	/**
	 * Gets the list of record from request result.
	 *
	 * @author MCH
	 * @param pResult
	 *            a request result
	 * @return The complete list of adaptations referenced by the request
	 *         result.
	 */
	public static List<Adaptation> getListOfRecordFromRequestResult(final RequestResult pResult)
	{
		List<Adaptation> records = new ArrayList<>();
		Adaptation record = null;
		pResult.refreshIfNeeded();
		while ((record = pResult.nextAdaptation()) != null)
		{
			records.add(record);
		}
		return records;
	}

	/**
	 * Gets the node.
	 *
	 * @author MCH
	 * @param pAdaptation
	 *            either a record or an instance
	 * @param pPath
	 *            absolute path from the adaptation root
	 * @return the schema node located a the given path
	 */
	public static SchemaNode getNode(final Adaptation pAdaptation, final Path pPath)
	{
		return pAdaptation.getSchemaNode().getNode(pPath);
	}

	/**
	 * Gets the node.
	 *
	 * @author MCH
	 * @param pValueContext
	 *             a value context
	 * @param pPath
	 *            absolute path from the adaptation root
	 * @return the schema node located a the given path
	 */
	public static SchemaNode getNode(final ValueContext pValueContext, final Path pPath)
	{
		return pValueContext.getNode().getNode(pPath);
	}

	/**
	 * Gets the record.
	 *
	 * @author MCH
	 * @param pDataSet the instance
	 * @param pXPathExpression the x path expression
	 * @return the record
	 * @throws OperationException the operation exception
	 */
	public static Adaptation getRecord(final Adaptation pDataSet, final String pXPathExpression)
		throws OperationException
	{
		Request request = XPathExpressionHelper.createRequestForXPath(pDataSet, pXPathExpression);
		RequestResult result = request.execute();
		try
		{
			Adaptation record = result.nextAdaptation();
			if (record != null && result.nextAdaptation() != null)
			{
				throw OperationException.createError("The xpath expression is not discriminating");
			}
			return record;
		}
		finally
		{
			result.close();
		}
	}

	/**
	 * Gets the record.
	 *
	 * @author MCH
	 *
	 * @param pDataSpace the data space
	 * @param pDataSet the data set
	 * @param pXPathExpression the xpath expression to the record
	 * @return the record
	 * @throws OperationException the operation exception
	 */
	public static Adaptation getRecord(
		final AdaptationHome pDataSpace,
		final String pDataSet,
		final String pXPathExpression) throws OperationException
	{
		Adaptation instance = getDataSet(pDataSpace, pDataSet);
		if (instance == null)
		{
			throw OperationException
				.createError("Data set '" + pDataSet + "' not found in data space '" + pDataSpace);
		}
		return getRecord(instance, pXPathExpression);
	}

	/**
	 * Gets the record.
	 *
	 * @author MCH
	 * @param pRepository the repository
	 * @param pDataSpace the data space
	 * @param pDataSet the data set
	 * @param pXPathExpression the x path expression
	 * @return the record
	 * @throws OperationException the operation exception
	 */
	public static Adaptation getRecord(
		final Repository pRepository,
		final String pDataSpace,
		final String pDataSet,
		final String pXPathExpression) throws OperationException
	{
		Adaptation instance = getDataSet(pRepository, pDataSpace, pDataSet);
		if (instance == null)
		{
			throw OperationException
				.createError("Data set '" + pDataSet + "' not found in data space '" + pDataSpace);
		}
		return getRecord(instance, pXPathExpression);
	}

	/**
	 * Gets a record.
	 *
	 * @author MCH
	 * @param pContext the context
	 * @param pDataSpace the data space
	 * @param pDataSet the data set
	 * @param pXPathExpression the x path expression
	 * @return the record
	 * @throws OperationException the operation exception
	 */
	public static Adaptation getRecord(
		final ValueContext pContext,
		final String pDataSpace,
		final String pDataSet,
		final String pXPathExpression) throws OperationException
	{
		Adaptation instance = getDataSet(pContext, pDataSpace, pDataSet);
		if (instance == null)
		{
			throw OperationException
				.createError("Data set '" + pDataSet + "' not found in data space '" + pDataSpace);
		}

		return getRecord(instance, pXPathExpression);
	}

	/**
	 * Get a record from a value context.
	 *
	 * @author MCH
	 *
	 * @param pValueContext a value context
	 */
	public static Adaptation getRecordForValueContext(final ValueContext pValueContext)
	{
		return pValueContext.getAdaptationTable().lookupAdaptationByPrimaryKey(pValueContext);
	}

	/**
	 * Get the same record from the initial version of the data space.
	 * @author MCH
	 * @param pRecord the record to look for
	 * @return the record from the parent data space, Null if he record, its table or its data set does not exist.
	 */
	public static Adaptation getRecordFromInitialVersion(final Adaptation pRecord)
	{
		AdaptationHome parentDataSpace = pRecord.getHome().getParentBranch();
		if (parentDataSpace == null)
		{
			return null;
		}
		return getRecordFromOtherDataSpace(pRecord, parentDataSpace);
	}

	/**
	 * Get the same record from a different data space.
	 *
	 * @author MCH
	 *
	 * @param pRecord the record to look for
	 * @param pDataSpace the other data space to find it in
	 * @return the record from the other data space. Null if he record, its table or its data set does not exist.
	 */
	public static Adaptation getRecordFromOtherDataSpace(
		final Adaptation pRecord,
		final AdaptationHome pDataSpace)
	{
		Adaptation dataSet = pRecord.getContainer();
		Adaptation otherDataSet = pDataSpace.findAdaptationOrNull(dataSet.getAdaptationName());
		if (otherDataSet == null)
		{
			return null;
		}
		AdaptationTable table = pRecord.getContainerTable();
		AdaptationTable otherTable = otherDataSet.getTable(table.getTablePath());
		if (otherTable == null)
		{
			return null;
		}
		return otherTable.lookupAdaptationByPrimaryKey(pRecord.getOccurrencePrimaryKey());
	}

	/**
	 * Gets the record referenced by a foreign key.
	 *
	 * @author MCH
	 * @param pRecord
	 *            a record or a data set.
	 * @param pPath
	 *            path to a foreign key from the occurrence or data set root
	 * @return the record referenced by the foreign key
	 * @throws OperationException
	 *             the operation exception
	 */
	public static Adaptation getReferencedRecord(final Adaptation pRecord, final Path pPath)
		throws OperationException
	{
		SchemaNode fk = getNode(pRecord, pPath);
		SchemaFacetTableRef tableRef = fk.getFacetOnTableReference();
		if (tableRef == null)
		{
			throw OperationException
				.createError("The node at path " + pPath.format() + " is not a foreign key");
		}

		if (SchemaUtils.isList(fk))
		{
			throw OperationException.createError(
				"The node at path " + pPath.format()
					+ " is a list. please use method getReferencedRecords instead.");
		}

		return tableRef.getLinkedRecord(pRecord);
	}

	/**
	 * Gets the record reference by a foreign key.
	 *
	 * @author MCH
	 * @param pValueContext
	 *            a value context
	 * @param pPath
	 *             relative path to a foreign key from the value context
	 * @return the record referenced by the foreign key
	 * @throws OperationException
	 *             the operation exception
	 */
	public static Adaptation getReferencedRecord(final ValueContext pValueContext, final Path pPath)
		throws OperationException
	{
		SchemaNode fk = getNode(pValueContext, pPath);
		SchemaFacetTableRef tableRef = fk.getFacetOnTableReference();
		if (tableRef == null)
		{
			throw OperationException
				.createError("The node at path " + pPath.format() + " is not a foreign key");
		}

		if (SchemaUtils.isList(fk))
		{
			throw OperationException.createError(
				"The node at path " + pPath.format()
					+ " is a list. please use method getReferencedRecords instead.");
		}

		return tableRef.getLinkedRecord(pValueContext);
	}

	/**
	 * Gets the referenced records.
	 *
	 * @author MCH
	 * @param pRecord
	 *            the record or data set
	 * @param pPath
	 *            path to a foreign key from the occurrence or data set root
	 * @return the records referenced by the foreign key
	 * @throws OperationException
	 *             the operation exception
	 */
	public static List<Adaptation> getReferencedRecords(final Adaptation pRecord, final Path pPath)
		throws OperationException
	{
		SchemaNode fk = getNode(pRecord, pPath);
		if (fk == null)
		{
			throw OperationException.createError("No node found at path '" + pPath.format() + "'");
		}
		SchemaFacetTableRef tableRef = fk.getFacetOnTableReference();
		if (tableRef == null)
		{
			throw OperationException
				.createError("The node at path " + pPath.format() + " is not a foreign key");
		}

		if (SchemaUtils.isList(fk))
		{
			return tableRef.getLinkedRecords(pRecord);
		}
		else
		{
			return Arrays.asList(tableRef.getLinkedRecord(pRecord));
		}
	}

	/**
	 * Gets the referenced records.
	 *
	 * @author MCH
	 * @param pValueContext
	 *            a value context
	 * @param pPath
	 *            relative path to a foreign key from the value context
	 * @return the records referenced by the foreign key
	 * @throws OperationException
	 *             the operation exception
	 */
	public static List<Adaptation> getReferencedRecords(
		final ValueContext pValueContext,
		final Path pPath) throws OperationException
	{
		SchemaNode fk = getNode(pValueContext, pPath);
		if (fk == null)
		{
			throw OperationException.createError("No node found at path '" + pPath.format() + "'");
		}
		SchemaFacetTableRef tableRef = fk.getFacetOnTableReference();
		if (tableRef == null)
		{
			throw OperationException
				.createError("The node at path " + pPath.format() + " is not a foreign key");
		}

		if (SchemaUtils.isList(fk))
		{
			return tableRef.getLinkedRecords(pValueContext);
		}
		else
		{
			return Arrays.asList(tableRef.getLinkedRecord(pValueContext));
		}
	}

	/**
	 * Gets the table.
	 *
	 * @author MCH
	 * @param pDataSpace the data space
	 * @param pDataSet the data set
	 * @param pTablePath the table path
	 * @return the table
	 * @throws OperationException the operation exception
	 */
	public static AdaptationTable getTable(
		final AdaptationHome pDataSpace,
		final String pDataSet,
		final String pTablePath) throws OperationException
	{
		Adaptation instance = getDataSet(pDataSpace, pDataSet);
		if (instance == null)
		{
			throw OperationException
				.createError("Data set '" + pDataSet + "' not found in data space '" + pDataSpace);
		}

		return instance.getTable(Path.parse(pTablePath));
	}

	/**
	 * Gets the table.
	 *
	 * @author MCH
	 * @param pRepository the repository
	 * @param pDataSpace the data space
	 * @param pDataSet the data set
	 * @param pTablePath the table path
	 * @return the table
	 * @throws OperationException the operation exception
	 */
	public static AdaptationTable getTable(
		final Repository pRepository,
		final String pDataSpace,
		final String pDataSet,
		final String pTablePath) throws OperationException
	{
		Adaptation instance = getDataSet(pRepository, pDataSpace, pDataSet);
		if (instance == null)
		{
			throw OperationException
				.createError("Data set '" + pDataSet + "' not found in data space '" + pDataSpace);
		}

		return instance.getTable(Path.parse(pTablePath));
	}

	/**
	 * @author US-team
	 * @throws OperationException
	 *
	 */
	public static boolean isLinkedRecordListEmpty(final Adaptation pRecord, final Path pPath)
		throws OperationException
	{
		RequestResult result = requestLinkedRecords(pRecord, pPath);
		return result.isEmpty();
	}

	/**
	 * Gets a request result from an association, a selection node or a foreign key.
	 *
	 * @author MCH
	 * @throws OperationException
	 *
	 * @param pRecord the record
	 * @param pPath the xpath of an association or a selection node.
	 * @return the request result attached to the association or the selection node.
	 */
	public static RequestResult requestLinkedRecords(final Adaptation pRecord, final Path pPath)
		throws OperationException
	{
		if (pRecord.isSchemaInstance())
		{
			throw OperationException
				.createError("The adaptation in parameters must be an occurrence");
		}
		final SchemaNode node = pRecord.getSchemaNode().getNode(pPath);
		if (node.isAssociationNode())
		{
			return node.getAssociationLink().getResult(pRecord, null);
		}
		if (node.isSelectNode())
		{
			return node.getSelectionLink().getSelectionResult(pRecord);
		}
		throw OperationException
			.createError("The specified path must lead to an association or a selection node.");
	}
}

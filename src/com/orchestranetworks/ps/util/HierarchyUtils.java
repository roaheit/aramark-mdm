package com.orchestranetworks.ps.util;

import java.util.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.addon.hmfh.constants.*;
import com.orchestranetworks.ps.util.functional.*;
import com.orchestranetworks.schema.*;

public class HierarchyUtils
{
	public static Adaptation getParentRel(Adaptation dimensionRecord, Hierarchy hierarchy)
	{
		return hierarchy.getFirstParentRel(dimensionRecord);
	}

	public static boolean isDescendantOf(
		Adaptation record,
		UnaryPredicate<Adaptation> predicate,
		Hierarchy hierarchy)
	{
		if (record == null)
			return false;
		List<Adaptation> p = hierarchy.getParents(record);
		if (p == null)
			return false;
		Set<Adaptation> parents = new HashSet<>();
		for (Adaptation parent : p)
		{
			Adaptation parentRecord = AdaptationUtil.followFK(parent, hierarchy.dp.parentPath);
			if (predicate.test(parentRecord))
				return true;
			else
			{
				parents.add(parentRecord);
			}
		}
		for (Adaptation parent : parents)
			if (isDescendantOf(parent, predicate, hierarchy))
				return true;
		return false;
	}

	public static List<Adaptation> getParents(Adaptation dimensionRecord, Hierarchy hierarchy)
	{
		List<Adaptation> parentRels = hierarchy.getParents(dimensionRecord);
		if (parentRels == null)
			return null;
		List<Adaptation> result = new ArrayList<>();
		for (Adaptation parentRel : parentRels)
		{
			Adaptation parent = AdaptationUtil.followFK(parentRel, hierarchy.dp.parentPath);
			if (parent != null)
				result.add(parent);
		}
		return result;
	}

	public static List<Adaptation> getChildren(Adaptation dimensionRecord, Hierarchy hierarchy)
	{
		List<Adaptation> childRels = hierarchy.getChildren(dimensionRecord);
		if (childRels == null)
			return null;
		List<Adaptation> result = new ArrayList<>();
		for (Adaptation childRel : childRels)
		{
			Adaptation child = AdaptationUtil.followFK(childRel, hierarchy.dp.childPath);
			if (child != null)
				result.add(child);
		}
		return result;
	}

	public static int getChildCount(Adaptation dimensionRecord, Hierarchy hierarchy)
	{
		return CollectionUtils.size(hierarchy.getChildren(dimensionRecord));
	}

	public static AdaptationTable getTable(Adaptation dataSet, String tableName)
	{
		if (tableName == null)
			return null;
		try
		{
			return dataSet.getTable(Path.parse(tableName));
		}
		catch (PathAccessException e)
		{
			return null;
		}
	}

	public static Path getParentPath(Path dimPath, Hierarchy hierarchy)
	{
		// iterate over the hierarchy levels -- find the parent path for the node for the dimpath
		// table
		return null;
	}

	public static Path getChildPath(Path dimPath, String dimName)
	{
		// iterate over the hierarchy levels -- find the child path for the node for the dimpath
		// table
		return null;
	}

	public static class Hierarchy
	{
		private final Map<String, List<Adaptation>> childrenByParent;
		private final Map<String, List<Adaptation>> parentsByChild;
		private final DimensionPaths dp;

		public Hierarchy(AdaptationTable dimTable, DimensionPaths dp)
		{
			super();
			this.dp = dp;
			AdaptationTable hierTable = dimTable.getContainerAdaptation()
				.getTable(this.dp.hierarchyPath);
			this.childrenByParent = new HashMap<>();
			this.parentsByChild = new HashMap<>();
			buildHierarchy(hierTable);
		}

		public Adaptation getFirstParentRel(Adaptation member)
		{
			List<Adaptation> parents = parentsByChild
				.get(member.getOccurrencePrimaryKey().format());
			return parents == null ? null : CollectionUtils.getFirstOrNull(parents);
		}

		public List<Adaptation> getChildren(Adaptation member)
		{
			return childrenByParent.get(member.getOccurrencePrimaryKey().format());
		}

		public List<Adaptation> getParents(Adaptation member)
		{
			return parentsByChild.get(member.getOccurrencePrimaryKey().format());
		}

		public Set<String> getAllChildren()
		{
			return parentsByChild.keySet();
		}

		private void buildHierarchy(AdaptationTable hierarchyTable)
		{
			Request rq = hierarchyTable.createRequest();
			rq.setSortCriteria(new RequestSortCriteria().add(dp.sortPath));
			RequestResult rr = rq.execute();
			try
			{
				Adaptation next;
				while ((next = rr.nextAdaptation()) != null)
				{
					String parent = next.getString(dp.parentPath);
					List<Adaptation> children = childrenByParent.get(parent);
					if (children == null)
					{
						children = new ArrayList<>();
						childrenByParent.put(parent, children);
					}
					children.add(next);
					String child = next.getString(dp.childPath);
					List<Adaptation> parents = parentsByChild.get(child);
					if (parents == null)
					{
						parents = new ArrayList<>();
						parentsByChild.put(child, parents);
						parents.add(next);
					}
					else
					{
						Adaptation fparent = parents.get(0);
						if (fparent.get_int(
							HmfhDefaultPaths._Root_Hyperion_Financial_AccountHierarchy._ParentOrder) > next
								.get_int(
									HmfhDefaultPaths._Root_Hyperion_Financial_AccountHierarchy._ParentOrder))
							parents.add(0, next);
						else
							parents.add(next);
					}
				}
			}
			finally
			{
				rr.close();
			}
		}
		public DimensionPaths getDp()
		{
			return dp;
		}

	}

	public static class DimensionPaths
	{
		public final Path parentPath;
		public final Path childPath;
		public final Path dimensionPath;
		public final Path hierarchyPath;
		public final Path sortPath;

		public DimensionPaths(
			Path parentPath,
			Path childPath,
			Path dimensionPath,
			Path hierarchyPath,
			Path sortPath)
		{
			super();
			this.parentPath = parentPath;
			this.childPath = childPath;
			this.dimensionPath = dimensionPath;
			this.hierarchyPath = hierarchyPath;
			this.sortPath = sortPath;
		}

	}

	public static boolean isParent(Adaptation member)
	{
		return getChildCount(member, null) > 0;
	}

}

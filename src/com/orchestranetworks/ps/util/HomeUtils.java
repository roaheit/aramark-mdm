package com.orchestranetworks.ps.util;

import java.util.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.service.*;

/**
 * Utility class to manipulate data spaces and snapshots.
 * 
 * @author MCH
 */
public class HomeUtils
{

	/**
	 * Gets the ancestors.
	 *
	 * @author MCH
	 * 
	 *         Get all the data spaces, ancestors of a data space or a snapshot
	 *         
	 * @param pHome the home to get the ancestors from
	 * @return data spaces, the ancestors
	 */
	public static List<AdaptationHome> getAncestors(final AdaptationHome pHome)
	{
		List<AdaptationHome> ancestors = new ArrayList<>();

		AdaptationHome initialVersion = pHome;
		if (!pHome.isBranch())
		{
			initialVersion = pHome.getParent();
		}

		if (initialVersion == null)
		{
			return ancestors;
		}

		AdaptationHome home = initialVersion.getParent();
		if (home == null)
		{
			return ancestors;
		}

		ancestors.add(home);
		ancestors.addAll(getAncestors(home));

		return ancestors;
	}

	public static String formatHomeLabel(AdaptationHome aHome, Locale aLocale)
	{
		StringBuffer buffer = new StringBuffer();
		int depth = HomeUtils.getHomeDepth(aHome);

		for (int i = 0; i < depth * 3; i++)
		{
			buffer.append("&nbsp;");
		}

		if (aHome.isInitialVersion())
		{
			for (int i = 0; i < 3; i++)
			{
				buffer.append("&nbsp;");
			}

			buffer.append((aHome.getBranchChildren().get(0)).getLabelOrName(aLocale))
				.append("&nbsp;&lt;initial version&gt;");
		}
		else
		{
			if (aHome.isVersion())
				buffer.append("(V)");
			buffer.append(aHome.getLabelOrName(aLocale));
		}
		return buffer.toString();
	}

	public static int getHomeDepth(AdaptationHome aHome)
	{

		AdaptationHome parentBranch = aHome.getParentBranch();

		if (parentBranch == null)
			return 0;

		return getHomeDepth(parentBranch) + 1;
	}

	public static void mergeDataSpaceToParent(
		final Session session,
		final AdaptationHome childDataSpaceHome)
		throws OperationException
	{
		// Merge of child data space procedure
		final Procedure merge = new Procedure()
		{
			@Override
			public void execute(final ProcedureContext pContext) throws Exception
			{
				closeDataSpaceChildren(childDataSpaceHome, session);
				pContext.setAllPrivileges(true);
				pContext.doMergeToParent(childDataSpaceHome);
				pContext.setAllPrivileges(false);
			}
		};

		// Merge of child data space execution
		final ProgrammaticService mergeService = ProgrammaticService
			.createForSession(session, childDataSpaceHome.getParentBranch());
		ProcedureResult result = mergeService.execute(merge);
		OperationException resultException = result.getException();
		if (resultException != null)
		{
			throw resultException;
		}
		if (result.hasFailed())
		{
			throw OperationException.createError("Dataspace Merge execution failed.");
		}
	}

	public static void closeDataSpaceChildren(AdaptationHome dataSpace, Session session)
		throws OperationException
	{
		// Closing a Data Space Child will recursively Close all its Ancestors
		List<AdaptationHome> dataSpaceChildList = dataSpace.getVersionChildren();
		for (AdaptationHome dataSpaceChild : dataSpaceChildList)
		{
			if (dataSpaceChild.isOpen())
			{
				dataSpaceChild.getRepository().closeHome(dataSpaceChild, session);
			}

		}

	}

}

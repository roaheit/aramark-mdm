/*
 * Copyright Orchestra Networks 2016. All rights reserved.
 */
package com.orchestranetworks.ps.constraint;

import java.util.*;

import com.orchestranetworks.instance.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.service.directory.*;

/**
 * This constraint enumeration can be used to create a selection field for
 * choosing a data space or snapshot (or both)
 */
public class UserConstraintEnumeration implements ConstraintEnumeration<String>
{
	private static final String MESSAGE = "Specify a user.";
	private boolean relaxed = true;

	@Override
	public void checkOccurrence(String aValue, ValueContextForValidation aValidationContext)
		throws InvalidSchemaException
	{
		if (!relaxed)
		{
			UserReference user = UserReference.forUser(aValue);
			DirectoryHandler directory = DirectoryHandler
				.getInstance(aValidationContext.getHome().getRepository());
			if (!directory.isUserDefined(user))
				aValidationContext.addError("User " + aValue + " does not exist.");
		}
	}

	@Override
	public void setup(ConstraintContext aContext)
	{
	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}

	@Override
	public String displayOccurrence(String aValue, ValueContext aContext, Locale aLocale)
		throws InvalidSchemaException
	{
		return getUserLabel(aContext.getHome().getRepository(), aLocale, aValue);
	}

	public static String getUserLabel(Repository repo, Locale locale, String userId)
	{
		DirectoryHandler directory = DirectoryHandler.getInstance(repo);
		UserReference user = UserReference.forUser(userId);
		if (directory.isUserDefined(user))
			return directory.displayUser(user, locale);
		return user.getLabel();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List getValues(ValueContext aContext) throws InvalidSchemaException
	{
		Set<String> result = new LinkedHashSet<>();
		String curr = (String) aContext.getValue();
		if (curr != null)
			result.add(curr);
		DirectoryHandler directory = DirectoryHandler
			.getInstance(aContext.getHome().getRepository());
		List<UserReference> users = directory.getUsersInRole(Role.EVERYONE);
		for (UserReference user : users)
		{
			result.add(user.getUserId());
		}
		return new ArrayList<>(result);
	}

	public boolean isRelaxed()
	{
		return relaxed;
	}

	public void setRelaxed(boolean relaxed)
	{
		this.relaxed = relaxed;
	}

}

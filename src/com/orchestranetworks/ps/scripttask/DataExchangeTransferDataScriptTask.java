package com.orchestranetworks.ps.scripttask;

import java.util.*;

import org.apache.commons.lang3.*;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.addon.dataexchange.transformation.*;
import com.orchestranetworks.addon.dex.*;
import com.orchestranetworks.addon.dex.common.generation.*;
import com.orchestranetworks.addon.dex.configuration.*;
import com.orchestranetworks.addon.dex.mapping.*;
import com.orchestranetworks.addon.dex.result.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.*;

public class DataExchangeTransferDataScriptTask extends ScriptTaskBean
{
	private String sourceApplicationLogicalName;
	private String targetApplicationLogicalName;
	private String dataSpaceName;
	private String dataSetName;
	private String tableXPath;
	private String targetDataSpaceName;
	private String targetDataSetName;
	// /root/sourceTable1,/root/targetTable1;/root/sourceTable2,/root/targetTable2
	private String sourceTargetTablePaths;
	private String delimiter = DELIMITER;
	private String tablePairDelimiter = TABLE_PAIR_DELIMITER;

	private static String DELIMITER = ",";
	private static String TABLE_PAIR_DELIMITER = ";";

	@Override
	public void executeScript(ScriptTaskBeanContext context) throws OperationException
	{
		try
		{
			Repository repository = context.getRepository();
			AdaptationHome dataSpace = AdaptationUtil.getDataSpaceOrThrowOperationException(
				repository,
				dataSpaceName);
			Adaptation dataSet = AdaptationUtil.getDataSetOrThrowOperationException(
				dataSpace,
				dataSetName);
			// TODO might need to modify tableXPath to allow multiple tables
			// TODO add a check for table xpath - it can't be empty
			AdaptationTable table = dataSet.getTable(Path.parse(tableXPath));

			if (table != null)
			{
				List<Path> tablePaths = new ArrayList<>();
				tablePaths.add(table.getTablePath());

				com.orchestranetworks.addon.dex.DataExchangeSpec dataExchangeSpec = new com.orchestranetworks.addon.dex.DataExchangeSpec();

				AdaptationHome targetDataSpace = AdaptationUtil.getDataSpaceOrThrowOperationException(
					repository,
					targetDataSpaceName);
				Adaptation targetDataSet = AdaptationUtil.getDataSetOrThrowOperationException(
					targetDataSpace,
					targetDataSetName);

				Map<String, String> parsedSourceTargetTablePaths = getParsedSourceTargetTablePaths();

				if (parsedSourceTargetTablePaths == null || parsedSourceTargetTablePaths.isEmpty())
				{
					throw OperationException.createError("sourceTargetTablePaths cannot be null or empty string.");
				}

				TableMappingList<EBXField, EBXField> tableMappingList = new TableMappingList<>();
				List<EBXTable> sourceEBXTables = new ArrayList<>();
				Set<Path> sourceTablePaths = new LinkedHashSet<>();

				for (Iterator<Map.Entry<String, String>> iter = parsedSourceTargetTablePaths.entrySet()
					.iterator(); iter.hasNext();)
				{
					Map.Entry<String, String> entry = iter.next();
					// add the source table paths to generate the source EBX tables for the Transfer
					// Config
					sourceTablePaths.add(Path.parse(entry.getKey()));
					// get the source and target adaptation table
					AdaptationTable sourceTable = dataSet.getTable(Path.parse(entry.getKey()));
					AdaptationTable targetTable = targetDataSet.getTable(Path.parse(entry.getValue()));
					// get the source and target EBX tables to create a TableMappingList to be used
					// in the applicationMapping
					EBXTable srcEBXTable = new EBXTable(sourceTable);
					EBXTable targetEBXTable = new EBXTable(targetTable);
					tableMappingList.add(new TableMapping<>(srcEBXTable, targetEBXTable));
				}

				CommonApplication sourceApplication = new CommonApplication(
					getSourceApplicationLogicalName(),
					ApplicationType.EBX);
				CommonApplication targetApplication = new CommonApplication(
					getTargetApplicationLogicalName(),
					ApplicationType.EBX);

				EBXTableGeneration genTable = TableGenerationFactory.getEBXTableGeneration();
				TableGenerationResult<EBXField, EBXTable> genTableResult = genTable.generateTables(
					dataSet,
					sourceTablePaths,
					context.getSession());
				sourceEBXTables = genTableResult.getTables();

				TransferConfigurationSpec transferConfig = new TransferConfigurationSpec(
					dataSet,
					sourceEBXTables,
					targetDataSet,
					context.getSession());
				transferConfig.setImportMode(ImportMode.UPDATE_AND_INSERT);

				ApplicationMapping<EBXField, EBXField> applicationMapping = ApplicationMappingHelperFactory.getApplicationMappingForTransferHelper()
					.getApplicationMapping(
						transferConfig,
						sourceApplication,
						targetApplication,
						tableMappingList);

				dataExchangeSpec.setApplicationMapping(applicationMapping);
				dataExchangeSpec.setConfigurationSpec(transferConfig);
				DataExchangeResult transferResults = DataExchangeServiceFactory.getDataExchangeService()
					.execute(dataExchangeSpec);
				StringBuilder resultMessage = new StringBuilder();

				for (@SuppressWarnings("unchecked")
				Iterator<TransferResult> iterator = (Iterator<TransferResult>) transferResults.getResults(); iterator.hasNext();)
				{
					Result result = iterator.next();
					// iterate the errors and build a string with them. later throw it an an operational exception
					for (UserMessage userMessage : result.getErrorMessages().values())
					{
						resultMessage.append(userMessage.formatMessage(Locale.getDefault()))
							.append("\n");

					}

					// iterating the warnings and logging them to the kernel log.
					for (Iterator<UserMessage> iterator2 = result.getWarningMessages(); iterator2.hasNext();)
					{
						UserMessage warningMessage = iterator2.next();
						LoggingCategory.getKernel().warn(warningMessage);
					}

				}
				if (resultMessage.length() > 0)
				{
					throw OperationException.createError(resultMessage.toString());
				}

			}
		}
		catch (Throwable e)
		{
			throw OperationException.createError(e);
		}

	}
	private Map<String, String> getParsedSourceTargetTablePaths()
	{
		Map<String, String> parsedSourceTargetTablePaths = new HashMap<>();
		String sourceTargetTablePaths = getSourceTargetTablePaths();
		if (StringUtils.isEmpty(sourceTargetTablePaths))
		{
			return parsedSourceTargetTablePaths;
		}

		String[] sourceTargetTablePathPairs = sourceTargetTablePaths.split(TABLE_PAIR_DELIMITER);
		if (sourceTargetTablePathPairs != null)
		{
			for (int i = 0; i < sourceTargetTablePathPairs.length; i++)
			{
				String[] sourceTargetTablePathPair = sourceTargetTablePathPairs[i].split(DELIMITER);
				if (sourceTargetTablePathPair != null && sourceTargetTablePathPair.length == 2)
				{
					String sourceTablePath = sourceTargetTablePathPair[0];
					String targetTablePath = sourceTargetTablePathPair[1];
					parsedSourceTargetTablePaths.put(sourceTablePath, targetTablePath);
				}
			}
		}
		return parsedSourceTargetTablePaths;
	}

	public String getSourceApplicationLogicalName()
	{
		return sourceApplicationLogicalName;
	}

	public void setSourceApplicationLogicalName(String sourceApplicationLogicalName)
	{
		this.sourceApplicationLogicalName = sourceApplicationLogicalName;
	}

	public String getTargetApplicationLogicalName()
	{
		return targetApplicationLogicalName;
	}

	public void setTargetApplicationLogicalName(String targetApplicationLogicalName)
	{
		this.targetApplicationLogicalName = targetApplicationLogicalName;
	}

	public String getDataSpaceName()
	{
		return dataSpaceName;
	}

	public void setDataSpaceName(String dataSpaceName)
	{
		this.dataSpaceName = dataSpaceName;
	}

	public String getDataSetName()
	{
		return dataSetName;
	}

	public void setDataSetName(String dataSetName)
	{
		this.dataSetName = dataSetName;
	}

	public String getTableXPath()
	{
		return tableXPath;
	}

	public void setTableXPath(String tableXPath)
	{
		this.tableXPath = tableXPath;
	}

	public String getTargetDataSpaceName()
	{
		return targetDataSpaceName;
	}

	public void setTargetDataSpaceName(String targetDataSpaceName)
	{
		this.targetDataSpaceName = targetDataSpaceName;
	}

	public String getTargetDataSetName()
	{
		return targetDataSetName;
	}

	public void setTargetDataSetName(String targetDataSetName)
	{
		this.targetDataSetName = targetDataSetName;
	}

	public String getSourceTargetTablePaths()
	{
		return sourceTargetTablePaths;
	}

	public void setSourceTargetTablePaths(String sourceTargetTablePaths)
	{
		this.sourceTargetTablePaths = sourceTargetTablePaths;
	}

	public String getDelimiter()
	{
		return delimiter;
	}

	public void setDelimiter(String delimiter)
	{
		this.delimiter = delimiter;
	}

	public String getTablePairDelimiter()
	{
		return tablePairDelimiter;
	}

	public void setTablePairDelimiter(String tablePairDelimiter)
	{
		this.tablePairDelimiter = tablePairDelimiter;
	}

}

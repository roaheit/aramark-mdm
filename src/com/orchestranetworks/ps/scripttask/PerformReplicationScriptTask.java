/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.scripttask;

import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.service.extensions.*;
import com.orchestranetworks.workflow.*;

/**
 */
public class PerformReplicationScriptTask extends ScriptTaskBean
{
	private String dataspace;
	private String dataset;
	private String replication;

	@Override
	public void executeScript(ScriptTaskBeanContext aContext) throws OperationException
	{
		AdaptationHome branch = aContext.getRepository()
			.lookupHome(HomeKey.forBranchName(dataspace));
		Adaptation dataSet = branch.findAdaptationOrNull(AdaptationName.forName(dataset));
		ReplicationUnit replicationUnit = ReplicationUnit
			.newReplicationUnit(ReplicationUnitKey.forName(replication), dataSet);
		replicationUnit.performRefresh(aContext.getSession());
	}
	public String getDataspace()
	{
		return this.dataspace;
	}

	public void setDataspace(String dataspace)
	{
		this.dataspace = dataspace;
	}

	public String getDataset()
	{
		return this.dataset;
	}

	public void setDataset(String dataset)
	{
		this.dataset = dataset;
	}
	public String getReplication()
	{
		return replication;
	}
	public void setReplication(String replication)
	{
		this.replication = replication;
	}

}

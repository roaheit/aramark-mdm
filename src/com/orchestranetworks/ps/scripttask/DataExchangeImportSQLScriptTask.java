package com.orchestranetworks.ps.scripttask;

import java.util.*;

import org.apache.commons.lang.*;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.addon.dataexchange.*;
import com.orchestranetworks.addon.dex.*;
import com.orchestranetworks.addon.dex.DataExchangeSpec;
import com.orchestranetworks.addon.dex.common.generation.*;
import com.orchestranetworks.addon.dex.configuration.*;
import com.orchestranetworks.addon.dex.mapping.*;
import com.orchestranetworks.addon.dex.mapping.TableMapping;
import com.orchestranetworks.addon.dex.result.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.*;

public class DataExchangeImportSQLScriptTask extends ScriptTaskBean
{
	private String sqlDataSourceName;
	private String dataSpaceName;
	private String dataSetName;
	private String tableXPath;
	private String schemaName;
	private String sqlTableOrViewName;
	private String sqlTableOrViewPredicate;

	@Override
	public void executeScript(ScriptTaskBeanContext context) throws OperationException
	{
		try
		{
			Repository repository = context.getRepository();
			AdaptationHome dataSpace = AdaptationUtil.getDataSpaceOrThrowOperationException(
				repository,
				dataSpaceName);
			Adaptation dataSet = AdaptationUtil.getDataSetOrThrowOperationException(
				dataSpace,
				dataSetName);
			AdaptationTable table = dataSet.getTable(Path.parse(tableXPath));

			LoggingCategory.getKernel().debug(
				"[Import SQL] Performed on dataspace: " + dataSpace.getKey().format()
					+ " and dataset: " + dataSet.getAdaptationName().getStringName());

			DataExchangeSpec spec = new DataExchangeSpec();

			EBXTableGeneration ebxTableGen = TableGenerationFactory.getEBXTableGeneration();
			Set<Path> ebxTablePaths = new LinkedHashSet<>();
			ebxTablePaths.add(Path.parse(tableXPath));
			List<EBXTable> ebxTables = ebxTableGen.generateTables(
				dataSet,
				ebxTablePaths,
				context.getSession()).getTables();

			if (ebxTables == null || ebxTables.isEmpty())
			{
				throw OperationException.createError("Cannot find EBX table!");
			}

			SQLTableGeneration sqlTableGen = TableGenerationFactory.getSQLTableGeneration();
			Set<String> sqlTableNames = new LinkedHashSet<>();
			sqlTableNames.add(getSqlTableOrViewName());
			List<SQLTable> sqlTables = sqlTableGen.generateTables(
				getSqlDataSourceName(),
				getSchemaName(),
				null,
				sqlTableNames,
				ServiceType.SQL_IMPORT).getTables();

			SQLTable sqlTable = new SQLTable(
				getSchemaName(),
				getSqlTableOrViewName(),
				sqlTables.get(0).getFields());

			SQLImportConfigurationSpec importSpec = null;
			// if the predicate is empty string or null we don't set it
			if (StringUtils.isEmpty(getSqlTableOrViewPredicate()))
			{
				importSpec = new SQLImportConfigurationSpec(
					table,
					getSqlDataSourceName(),
					sqlTable,
					context.getSession());
			}
			else
			{
				TableFilter tableFilter = new TableFilter();
				tableFilter.setPredicate(getSqlTableOrViewPredicate());
				importSpec = new SQLImportConfigurationSpec(
					table,
					getSqlDataSourceName(),
					sqlTable,
					tableFilter,
					context.getSession());
			}

			importSpec.setImportMode(ImportMode.UPDATE_AND_INSERT);
			importSpec.setImportForced(false);
			importSpec.setEmptyOrNullPrimaryKeyChecked(false);
			importSpec.setEmptyOrNullValueIgnored(false);

			@SuppressWarnings("rawtypes")
			Map<TableMapping, FieldMapperDefinition> tableMappingDef = new LinkedHashMap<>();
			TableMapping<SQLField, EBXField> tableMapping = new TableMapping<>(
				sqlTables.get(0),
				ebxTables.get(0));
			tableMappingDef.put(tableMapping, FieldMapperDefinition.MAP_NAME_WITH_INSENSITIVE);
			importSpec.setFieldMapperDefinitions(tableMappingDef);

			@SuppressWarnings("rawtypes")
			ApplicationMapping applicationMapping = ApplicationMappingHelperFactory.getApplicationMappingForSQLImportHelper()
				.getApplicationMapping(importSpec);
			spec.setApplicationMapping(applicationMapping);
			spec.setConfigurationSpec(importSpec);

			DataExchangeResult result = DataExchangeServiceFactory.getDataExchangeService()
				.execute(spec);

			StringBuilder resultMessage = new StringBuilder();
			for (Iterator<UserMessage> iterator = result.getErrorMessages(); iterator.hasNext();)
			{
				UserMessage userMessage = iterator.next();
				resultMessage.append(userMessage.formatMessage(Locale.getDefault())).append("\n");
			}
			if (resultMessage.length() > 0)
			{
				throw OperationException.createError(resultMessage.toString());
			}

			for (Iterator<UserMessage> iterator = result.getWarningMessages(); iterator.hasNext();)
			{
				UserMessage userMessage = iterator.next();
				LoggingCategory.getKernel().warn(userMessage);
			}

		}
		catch (DataExchangeException e)
		{
			throw OperationException.createError(e);
		}
	}
	public String getSqlDataSourceName()
	{
		return sqlDataSourceName;
	}

	public void setSqlDataSourceName(String datasourceName)
	{
		this.sqlDataSourceName = datasourceName;
	}

	public String getDataSpaceName()
	{
		return dataSpaceName;
	}

	public void setDataSpaceName(String dataSpaceName)
	{
		this.dataSpaceName = dataSpaceName;
	}

	public String getDataSetName()
	{
		return dataSetName;
	}

	public void setDataSetName(String dataSetName)
	{
		this.dataSetName = dataSetName;
	}

	public String getTableXPath()
	{
		return tableXPath;
	}

	public void setTableXPath(String tableXPath)
	{
		this.tableXPath = tableXPath;
	}

	public String getSchemaName()
	{
		return schemaName;
	}

	public void setSchemaName(String schemaName)
	{
		this.schemaName = schemaName;
	}

	public String getSqlTableOrViewName()
	{
		return sqlTableOrViewName;
	}

	public void setSqlTableOrViewName(String sqlTableOrViewName)
	{
		this.sqlTableOrViewName = sqlTableOrViewName;
	}

	public String getSqlTableOrViewPredicate()
	{
		return sqlTableOrViewPredicate;
	}

	public void setSqlTableOrViewPredicate(String sqlTableOrViewPredicate)
	{
		this.sqlTableOrViewPredicate = sqlTableOrViewPredicate;
	}

}

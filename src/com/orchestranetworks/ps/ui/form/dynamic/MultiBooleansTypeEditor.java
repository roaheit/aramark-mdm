package com.orchestranetworks.ps.ui.form.dynamic;

import com.orchestranetworks.schema.*;
import com.orchestranetworks.ui.*;

@Deprecated
public class MultiBooleansTypeEditor extends UIBeanEditor
{
	private static final String ILLEGAL_STATE_MESSAGE = "Type should be a complex type composed by boolean fields";

	@Override
	public void addForDisplay(final UIResponseContext pContext)
	{
		pContext.addWidget(pContext.newBestMatching(Path.SELF));
	}

	@Override
	public void addForDisplayInTable(final UIResponseContext pContext)
	{
		StringBuilder display = new StringBuilder();
		SchemaNode[] nodes = pContext.getNode().getNodeChildren();
		if (nodes.length == 0)
		{
			throw new IllegalStateException(ILLEGAL_STATE_MESSAGE);
		}

		for (SchemaNode node : nodes)
		{
			if (!node.isTerminalValueDescendant()
				|| !SchemaTypeName.XS_BOOLEAN.equals(node.getXsTypeName()))
			{
				throw new IllegalStateException(ILLEGAL_STATE_MESSAGE);
			}

			if (((Boolean) pContext.getValue(Path.SELF.add(node.getPathInSchema().getLastStep())))
				.booleanValue())
			{
				display.append(node.getLabel(pContext.getLocale()) + ", ");
			}
		}

		if (display.length() != 0)
		{
			display.delete(display.length() - 2, display.length());
		}
		pContext.add(display.toString());
	}

	@Override
	public void addForEdit(final UIResponseContext pContext)
	{
		SchemaNode[] nodes = pContext.getNode().getNodeChildren();
		if (nodes.length == 0)
		{
			throw new IllegalStateException(ILLEGAL_STATE_MESSAGE);
		}

		pContext.add("<ul style='padding:0'>");

		for (SchemaNode node : nodes)
		{
			if (!node.isTerminalValueDescendant()
				|| !SchemaTypeName.XS_BOOLEAN.equals(node.getXsTypeName()))
			{
				throw new IllegalStateException(ILLEGAL_STATE_MESSAGE);
			}

			String name = node.getPathInSchema().getLastStep().format();
			pContext.add("<li style='list-style-type:none;'>");
			pContext.add(
				"<input onchange='refreshForm();' type='checkbox' name='" + name + "' id='" + name
					+ "'");
			if (((Boolean) pContext.getValue(Path.SELF.add(node.getPathInSchema().getLastStep())))
				.booleanValue())
			{
				pContext.add(" checked");
			}
			pContext.add(">");
			pContext.add(
				"<label for='" + name + "'>" + node.getLabel(pContext.getLocale()) + "</label>");
			pContext.add("</li>");
		}

		pContext.add("</ul>");
	}

	@Override
	public void validateInput(final UIRequestContext pContext)
	{
		super.validateInput(pContext);

		SchemaNode[] nodes = pContext.getNode().getNodeChildren();
		if (nodes.length == 0)
		{
			throw new IllegalStateException(ILLEGAL_STATE_MESSAGE);
		}

		for (SchemaNode node : nodes)
		{
			if (!node.isTerminalValueDescendant()
				|| !node.getXsTypeName().equals(SchemaTypeName.XS_BOOLEAN))
			{
				throw new IllegalStateException(ILLEGAL_STATE_MESSAGE);
			}

			String value = pContext.getOptionalRequestParameterValue(
				node.getPathInAdaptation().getLastStep().format());
			if (value == null || "null".equals(value))
			{
				pContext.getValueContext(Path.SELF.add(node.getPathInAdaptation().getLastStep()))
					.setNewValue(Boolean.FALSE);
			}
			else
			{
				pContext.getValueContext(Path.SELF.add(node.getPathInAdaptation().getLastStep()))
					.setNewValue(Boolean.TRUE);
			}

		}
	}
}

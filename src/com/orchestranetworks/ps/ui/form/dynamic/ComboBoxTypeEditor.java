package com.orchestranetworks.ps.ui.form.dynamic;

import com.orchestranetworks.schema.*;
import com.orchestranetworks.ui.*;
import com.orchestranetworks.ui.base.*;
import com.orchestranetworks.ui.form.widget.*;

@Deprecated
public class ComboBoxTypeEditor extends UIBeanEditor
{

	@Override
	public void addForDisplay(final UIResponseContext pContext)
	{
		pContext.addWidget(Path.SELF);
	}

	@Override
	public void addForEdit(final UIResponseContext uiResCtx)
	{
		UIComboBox widget = uiResCtx.newComboBox(Path.SELF);
		widget.setActionOnAfterValueChanged(JsFunctionCall.on("refreshForm"));
		uiResCtx.addWidget(widget);
	}
}

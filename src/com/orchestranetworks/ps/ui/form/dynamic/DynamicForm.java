package com.orchestranetworks.ps.ui.form.dynamic;

import java.util.*;

import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.*;
import com.orchestranetworks.ui.UIFormLabelSpec.*;
import com.orchestranetworks.ui.form.*;
import com.orchestranetworks.ui.form.UIFormPaneWithTabs.*;

/**
 *
 * @see ComboBoxTypeEditor
 * @see DynamicAccessRuleBasedOnTypes
 *
 * @author MCH
 */
public class DynamicForm extends UIForm
{
	private class UIFormPaneForNode implements UIFormPane
	{
		private final SchemaNode node;

		public UIFormPaneForNode(final SchemaNode pNode)
		{
			this.node = pNode;
		}

		@Override
		public void writePane(final UIFormPaneWriter pWriter, final UIFormContext pContext)
		{
			DynamicForm.this.writePane(this.node, UIUtils.getCurrentAdaptation(pContext), pWriter);
		}
	}

	private class UIFormPaneGeneral implements UIFormPane
	{
		@Override
		public void writePane(final UIFormPaneWriter pWriter, final UIFormContext pContext)
		{
			DynamicForm.this.addJavascript(pWriter, pContext);
			SchemaNode rootNode = pContext.getCurrentTable().getTableOccurrenceRootNode();
			DynamicForm.this.writePane(rootNode, UIUtils.getCurrentAdaptation(pContext), pWriter);
		}
	}

	private static final String ILLEGAL_STATE_MESSAGE = "Type should be either a string or a complex type composed by boolean fields";

	private String pathToTypes;

	private List<SchemaNode> tabs = new ArrayList<>();

	private final List<SchemaNode> typesAsListsOfStrings = new ArrayList<>();
	private final List<SchemaNode> typesAsBoolean = new ArrayList<>();

	private void addFormRowOrGroup(final SchemaNode pNode, final UIFormPaneWriter pWriter, final Locale locale)
	{
		if (pNode.isTerminalValue())
		{
			UIFormRow formRow = pWriter.newFormRow(Path.SELF.add(pNode.getPathInAdaptation()));
			formRow.setRowId(UIUtils.getUniqueWebIdentifierForNode(pNode));
			DocumentationPane docPane = new DocumentationPane(pNode.getPathInSchema(), true);
			UIFormLabelSpec labelSpec = new UIFormLabelSpec(docPane, pNode.getLabel(locale));
			if (SchemaUtils.isNodeMandatory(pNode, new ArrayList<String>()))
			{
				labelSpec.setMandatoryIndicator();
			}
			formRow.setLabel(labelSpec);
			pWriter.addFormRow(formRow);
		}
		else
		{
			UIFormGroup formGroup = pWriter.newFormGroup(Path.SELF.add(pNode.getPathInAdaptation()));
			formGroup.setId(UIUtils.getUniqueWebIdentifierForNode(pNode));
			DocumentationPane docPane = new DocumentationPane(pNode.getPathInSchema(), true);
			UIFormLabelSpec labelSpec = new UIFormLabelSpec(docPane, pNode.getLabel(locale));
			formGroup.setLabel(labelSpec);
			pWriter.addFormGroup(formGroup);
		}
	}

	private void addJavascript(final UIFormPaneWriter pWriter, final UIFormContext pContext)
	{
		this.resolveTypesNodes(pContext);

		this.addJsFnToGetTypes(pWriter, pContext);

		this.addJsNodesToTypesMap(pWriter, pContext);

		this.addJsTabsRelatedMaps(pWriter, pContext);

		this.addJsFnToRefreshForm(pWriter);

		this.addJsFnContains(pWriter);

		pWriter.addJS_cr("refreshForm();");
	}

	private void addJsFnContains(final UIFormPaneWriter pWriter)
	{
		pWriter.addJS_cr("function contains(array, element){");
		pWriter.addJS_cr("for(var i=0; i< array.length; i++){");
		pWriter.addJS_cr("if(array[i] === element){");
		pWriter.addJS_cr("return true;");
		pWriter.addJS_cr("}");
		pWriter.addJS_cr("}");
		pWriter.addJS_cr("return false;");
		pWriter.addJS_cr("}");
	}

	private void addJsFnToGetTypes(final UIFormPaneWriter pWriter, final UIFormContext pContext)
	{
		pWriter.addJS_cr("function getTypes(){");
		pWriter.addJS_cr("var types = [];");
		pWriter.addJS_cr("var count = 0;");

		Adaptation currentAdaptation = UIUtils.getCurrentAdaptation(pContext);
		for (SchemaNode node : this.typesAsListsOfStrings)
		{
			if (pContext.getSession().getPermissions().getNodeAccessPermission(node, currentAdaptation).isHidden())
			{
				continue;
			}
			pWriter.addJS("var type = ");
			pWriter.addJS_getNodeValue(Path.SELF.add(node.getPathInAdaptation()));
			pWriter.addJS_cr(";");
			pWriter.addJS_cr("if(type!=null){");
			pWriter.addJS_cr("if(type.key !=null){");
			pWriter.addJS_cr("types[count] = type.key;");
			pWriter.addJS_cr("}else{");
			pWriter.addJS_cr("types[count] = type;");
			pWriter.addJS_cr("}");
			pWriter.addJS_cr("count++;");
			pWriter.addJS_cr("}");
		}

		for (SchemaNode node : this.typesAsBoolean)
		{
			if (pContext.getSession().getPermissions().getNodeAccessPermission(node, currentAdaptation).isHidden())
			{
				continue;
			}
			String name = node.getPathInSchema().getLastStep().format();
			if (UIUtils.isNodeReadOnlyOrHidden(node, pContext))
			{
				if (pContext.getCurrentRecord() != null && pContext.getCurrentRecord().get_boolean(node.getPathInAdaptation()))
				{
					pWriter.addJS_cr("types[count] = '" + node.getPathInSchema().getLastStep().format() + "';");
					pWriter.addJS_cr("count++;");
				}
			}
			else
			{
				pWriter.addJS("if(document.getElementById('" + name + "').checked){");
				pWriter.addJS_cr("types[count] = '" + node.getPathInSchema().getLastStep().format() + "';");
				pWriter.addJS_cr("count++;");
				pWriter.addJS_cr("}");
			}
		}
		pWriter.addJS("return types;");
		pWriter.addJS_cr("}");
	}

	// TODO replace private code
	private void addJsFnToRefreshForm(final UIFormPaneWriter pWriter)
	{
		pWriter.addJS_cr("function refreshForm(){");
		pWriter.addJS_cr("var types = getTypes();");
		pWriter.addJS_cr("for(var node in nodesToTypes){");
		pWriter.addJS_cr("for(var i=0; i< nodesToTypes[node].length; i++){");
		pWriter.addJS_cr("var div = document.getElementById(node);");
		pWriter.addJS_cr("if(nodesToTypes[node] != null && !contains(types,nodesToTypes[node][i])){");
		pWriter.addJS_cr("if(div.style.display == ''){");
		pWriter.addJS_cr("div.style.display='none';");
		pWriter.addJS_cr("tab = nodesToTabs[node]");
		pWriter.addJS_cr("if(tab != null && tabsDisplayIndicators[tab] != null){");
		pWriter.addJS_cr("tabsDisplayIndicators[tab]--;");
		pWriter.addJS_cr("if(tabsDisplayIndicators[tab] == 0){");
		pWriter.addJS_cr("EBX_Form.hideTab(tab);");
		pWriter.addJS_cr("}}}}else{");
		pWriter.addJS_cr("if(div.style.display=='none'){");
		pWriter.addJS_cr("div.style.display='';");
		pWriter.addJS_cr("tab = nodesToTabs[node]");
		pWriter.addJS_cr("if(tab != null && tabsDisplayIndicators[tab] != null){");
		pWriter.addJS_cr("tabsDisplayIndicators[tab]++;");
		pWriter.addJS_cr("if(tabsDisplayIndicators[tab] == 1){");
		pWriter.addJS_cr("EBX_Form.showTab(tab);");
		pWriter.addJS_cr("}}}");
		pWriter.addJS_cr("break;");
		pWriter.addJS_cr("}}}");
		pWriter.addJS_cr("for(var tab in tabsToTypes){");
		pWriter.addJS_cr("for(var i=0; i< tabsToTypes[tab].length; i++){");
		pWriter.addJS_cr("if(tabsToTypes[tab] != null && !contains(types,tabsToTypes[tab][i])){");
		pWriter.addJS_cr("EBX_Form.hideTab(tab);");
		pWriter.addJS_cr("}else{");
		pWriter.addJS_cr("EBX_Form.showTab(tab);");
		pWriter.addJS_cr("break;");
		pWriter.addJS_cr("}}}}");
	}

	private void addJsNodesToTypesMap(final UIFormPaneWriter pWriter, final UIFormContext pContext)
	{
		List<SchemaNode> nodes = SchemaUtils.getVisibleTerminalNodes(
			UIUtils.getCurrentAdaptation(pContext),
			pContext.getCurrentTable().getTableOccurrenceRootNode(),
			null,
			pContext.getSession());

		pWriter.addJS_cr("var nodesToTypes = {};");
		for (SchemaNode aNode : nodes)
		{
			List<String> nodeTypes = SchemaUtils.getTypesForNode(aNode);
			if (!nodeTypes.isEmpty())
			{
				pWriter.addJS("nodesToTypes['" + UIUtils.getUniqueWebIdentifierForNode(aNode) + "'] = [");
				for (String type : nodeTypes)
				{
					pWriter.addJS("'" + type + "',");
				}
				pWriter.addJS_cr("];");
			}
		}
	}

	private void addJsTabsRelatedMaps(final UIFormPaneWriter pWriter, final UIFormContext pContext)
	{
		pWriter.addJS_cr("var tabsDisplayIndicators = {};");
		pWriter.addJS_cr("var nodesToTabs = {};");
		pWriter.addJS_cr("var tabsToTypes = {};");
		for (SchemaNode tab : this.tabs)
		{
			List<SchemaNode> nodesInTab = SchemaUtils.getVisibleTerminalNodes(tab, pContext.getSession());
			for (SchemaNode node : nodesInTab)
			{
				pWriter.addJS(
					"nodesToTabs['" + UIUtils.getUniqueWebIdentifierForNode(node) + "'] = '" + UIUtils.getUniqueWebIdentifierForNode(tab)
						+ "';");
			}

			if (!this.HasAlwaysVisibleNodes(tab, pContext.getSession()))
			{
				pWriter.addJS("tabsDisplayIndicators['" + UIUtils.getUniqueWebIdentifierForNode(tab) + "'] = " + nodesInTab.size() + ";");
			}

			List<String> nodeTypes = SchemaUtils.getTypesForNode(tab);
			if (!nodeTypes.isEmpty())
			{
				pWriter.addJS("tabsToTypes['" + UIUtils.getUniqueWebIdentifierForNode(tab) + "'] = [");
				for (String type : nodeTypes)
				{
					pWriter.addJS("'" + type + "',");
				}
				pWriter.addJS_cr("];");
			}
		}
	}

	private void addTab(final SchemaNode tabNode, final UIFormPaneWithTabs uiFormPaneWithTabs, final UIFormContext pContext)
	{
		UIFormLabelSpec label = new UIFormLabelSpec(tabNode.getLabel(pContext.getLocale()));
		UIFormPane pane = new UIFormPaneForNode(tabNode);
		Tab tab = new Tab(label, pane);
		tab.setId(UIUtils.getUniqueWebIdentifierForNode(tabNode));
		uiFormPaneWithTabs.addTab(tab);
	}

	@Override
	public void defineBody(final UIFormBody pBody, final UIFormContext pContext)
	{
		this.tabs = SchemaUtils.getTabs(pContext.getCurrentTable().getTableOccurrenceRootNode());

		if (this.tabs.isEmpty())
		{
			pBody.setContent(new UIFormPaneGeneral());
			return;
		}

		UIFormPaneWithTabs uiFormPaneWithTabs = new UIFormPaneWithTabs();

		uiFormPaneWithTabs.addTab(UserMessage.createInfo(""), new UIFormPaneGeneral());

		for (SchemaNode tabNode : this.tabs)
		{
			this.addTab(tabNode, uiFormPaneWithTabs, pContext);
		}

		uiFormPaneWithTabs.setHomeIconDisplayed(true);
		pBody.setContent(uiFormPaneWithTabs);
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * com.orchestranetworks.ui.form.UIForm#defineBottomBar(com.orchestranetworks
	 * .ui.form.UIFormBottomBar, com.orchestranetworks.ui.form.UIFormContext)
	 */
	@Override
	public void defineBottomBar(final UIFormBottomBar uiFormBottomBar, final UIFormContext uifFormContext)
	{
		uiFormBottomBar.setSubmitAndCloseButtonDisplayable(true);
	}

	private List<String> getTypes(final ValueContextForInputValidation pContext)
	{
		List<String> types = new ArrayList<>();
		for (SchemaNode node : this.typesAsListsOfStrings)
		{
			types.add((String) pContext.getValue(Path.SELF.add(node.getPathInAdaptation())));
		}
		for (SchemaNode node : this.typesAsBoolean)
		{
			if ((Boolean) pContext.getValue(Path.SELF.add(node.getPathInAdaptation())))
			{
				types.add(node.getPathInAdaptation().getLastStep().format());
			}
		}
		return types;
	}

	private boolean HasAlwaysVisibleNodes(final SchemaNode pRootNode, final Session pSession)
	{
		List<SchemaNode> nodes = SchemaUtils.getVisibleTerminalNodes(pRootNode, pSession);
		for (SchemaNode node : nodes)
		{
			if (SchemaUtils.getTypesForNode(node).isEmpty())
			{
				return true;
			}
		}
		return false;
	}

	private void resolveTypesNodes(final UIFormContext pContext)
	{
		String[] pathsToTypes = this.pathToTypes.split(";");
		for (String pathToType : pathsToTypes)
		{
			SchemaNode typeNode = pContext.getCurrentTable().getTableOccurrenceRootNode().getNode(Path.SELF.add(Path.parse(pathToType)));

			if (SchemaTypeName.XS_STRING.equals(typeNode.getXsTypeName()) && typeNode.getMaxOccurs() == 1)
			{
				this.typesAsListsOfStrings.add(typeNode);
				continue;
			}

			if (SchemaTypeName.XS_BOOLEAN.equals(typeNode.getXsTypeName()) && typeNode.getMaxOccurs() == 1)
			{
				this.typesAsBoolean.add(typeNode);
				continue;
			}

			SchemaNode[] nodes = typeNode.getNodeChildren();
			if (nodes.length == 0)
			{
				throw new IllegalStateException(typeNode.getPathInAdaptation().format() + " : " + ILLEGAL_STATE_MESSAGE);
			}

			for (SchemaNode node : nodes)
			{
				if (!node.isTerminalValueDescendant() || !SchemaTypeName.XS_BOOLEAN.equals(node.getXsTypeName()))
				{
					throw new IllegalStateException(typeNode.getPathInAdaptation().format() + " : " + ILLEGAL_STATE_MESSAGE);
				}

				this.typesAsBoolean.add(node);
			}
		}
	}

	public void setPathToTypes(final String pathToTypes)
	{
		this.pathToTypes = pathToTypes;
	}

	@Override
	public void validateForm(final UIFormRequestContext pContext)
	{
		List<String> types = this.getTypes(pContext.getValueContext(Path.SELF));
		List<SchemaNode> nodes = SchemaUtils
			.getVisibleTerminalNodes(UIUtils.getCurrentAdaptation(pContext), pContext.getTableNode(), null, pContext.getSession());
		for (SchemaNode node : nodes)
		{
			List<String> nodeTypes = SchemaUtils.getTypesForNode(node);
			int size = nodeTypes.size();
			nodeTypes.removeAll(types);
			if (size == 0 || nodeTypes.size() < size)
			{
				continue;
			}
			pContext.getValueContext(Path.SELF.add(node.getPathInAdaptation())).setNewValue(null);
		}
		super.validateForm(pContext);
	}

	private void writePane(final SchemaNode pRootNode, final Adaptation pAdaptation, final UIFormPaneWriter pWriter)
	{
		if (pRootNode.isAssociationNode())
		{
			pWriter.addWidget(Path.SELF.add(pRootNode.getPathInAdaptation()));
			return;
		}

		List<SchemaNode> nodes = SchemaUtils.getVisibleFirstLevelNodes(pAdaptation, pRootNode, this.tabs, pWriter.getSession());

		pWriter.startTableFormRow();
		for (SchemaNode aNode : nodes)
		{
			this.addFormRowOrGroup(aNode, pWriter, pWriter.getLocale());
		}
		pWriter.endTableFormRow();
	}
}

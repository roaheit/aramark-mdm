/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.widget;

import com.orchestranetworks.ui.form.widget.*;

/**
 * This is an editor so that list-fields can be displayed in a table.
 */
public class ListUIWidgetFactory implements UIWidgetFactory<ListUICustomWidget>
{
	private boolean readOnly;
	private String separator = ", ";

	public boolean isReadOnly()
	{
		return readOnly;
	}

	public void setReadOnly(boolean readOnly)
	{
		this.readOnly = readOnly;
	}

	public String getSeparator()
	{
		return separator;
	}

	public void setSeparator(String separator)
	{
		this.separator = separator;
	}

	@Override
	public ListUICustomWidget newInstance(WidgetFactoryContext aContext)
	{
		return new ListUICustomWidget(aContext, this);
	}

	@Override
	public void setup(WidgetFactorySetupContext aContext)
	{
	}

}

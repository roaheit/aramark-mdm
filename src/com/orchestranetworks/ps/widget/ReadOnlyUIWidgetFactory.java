package com.orchestranetworks.ps.widget;

import com.orchestranetworks.ui.form.widget.*;

public class ReadOnlyUIWidgetFactory<T extends ReadOnlyUICustomWidget> implements UIWidgetFactory<T>
{
	private String editorRoles;
	private boolean neverEditable;
	private boolean neverValidate;

	/**
	 * Get a comma-separated list of roles that can edit
	 * 
	 * @return the roles
	 */
	public String getEditorRoles()
	{
		return this.editorRoles;
	}

	/**
	 * Set a comma-separated list of roles that can edit
	 * 
	 * @param editorRoles
	 *            the roles
	 */
	public void setEditorRoles(String editorRoles)
	{
		this.editorRoles = editorRoles;
	}

	/**
	 * Get whether this is never editable. If never editable, no one can edit it
	 * even if their roles are specified as editable.
	 * 
	 * @return whether this is never editable
	 */
	public boolean isNeverEditable()
	{
		return this.neverEditable;
	}

	/**
	 * Set whether this is never editable. If never editable, no one can edit it
	 * even if their roles are specified as editable.
	 * 
	 * @param neverEditable
	 *            whether this is never editable
	 */
	public void setNeverEditable(boolean neverEditable)
	{
		this.neverEditable = neverEditable;
	}

	/**
	 * Get whether this never validates. By default, the editor doesn't validate
	 * when it's read only and otherwise it validates. This will allow you to
	 * specify that it should never validate regardless of if it's editable.
	 * 
	 * @return whether this never validates
	 */
	public boolean isNeverValidate()
	{
		return this.neverValidate;
	}

	/**
	 * Set whether this never validates. By default, the editor doesn't validate
	 * when it's read only and otherwise it validates. This will allow you to
	 * specify that it should never validate regardless of if it's editable.
	 * 
	 * @param neverValidate
	 *            whether this never validates
	 */
	public void setNeverValidate(boolean neverValidate)
	{
		this.neverValidate = neverValidate;
	}

	@SuppressWarnings("unchecked")
	@Override
	public T newInstance(WidgetFactoryContext aContext)
	{
		return (T) new ReadOnlyUICustomWidget(aContext, this);
	}

	@Override
	public void setup(WidgetFactorySetupContext aContext)
	{
		// do nothing
	}
}

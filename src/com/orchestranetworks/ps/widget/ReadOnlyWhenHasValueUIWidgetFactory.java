package com.orchestranetworks.ps.widget;

import com.orchestranetworks.ui.form.widget.*;

/**
 * Creates a {@link ReadOnlyWhenHasValueUICustomWidget}.
 */
public class ReadOnlyWhenHasValueUIWidgetFactory<T extends ReadOnlyWhenHasValueUICustomWidget>
	extends
	ReadOnlyUIWidgetFactory<T>
{
	private boolean readWriteWhenDuplicating;

	@SuppressWarnings("unchecked")
	@Override
	public T newInstance(WidgetFactoryContext aContext)
	{
		return (T) new ReadOnlyWhenHasValueUICustomWidget(aContext, this);
	}

	@Override
	public void setup(WidgetFactorySetupContext aContext)
	{
		super.setup(aContext);
		// These parameters exist in the parent class so we can't prevent someone from specifying values for them,
		// but they don't make sense for this widget factory. (If it's never editable then you should just be using
		// ReadOnlyUIWidgetFactory, and you should not be specifying to never validate when this is read/write sometimes.)
		if (isNeverEditable())
		{
			aContext.addError("neverEditable parameter is not supported.");
		}
		if (isNeverValidate())
		{
			aContext.addError("neverValidate parameter is not supported.");
		}
	}

	public boolean isReadWriteWhenDuplicating()
	{
		return readWriteWhenDuplicating;
	}

	public void setReadWriteWhenDuplicating(boolean readWriteWhenDuplicating)
	{
		this.readWriteWhenDuplicating = readWriteWhenDuplicating;
	}
}

package com.orchestranetworks.ps.widget;

import com.orchestranetworks.ps.constants.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.service.directory.*;
import com.orchestranetworks.ui.form.widget.*;

@SuppressWarnings("rawtypes")
public class ReadOnlyUICustomWidget extends UISimpleCustomWidget
{
	protected static final String SEPARATOR = ",";
	protected final ReadOnlyUIWidgetFactory factory;

	public ReadOnlyUICustomWidget(WidgetFactoryContext aContext, ReadOnlyUIWidgetFactory factory)
	{
		super(aContext);
		this.factory = factory;
	}

	@Override
	public void validate(WidgetValidationContext validationContext)
	{
		if (!factory.isNeverValidate() && !factory.isNeverEditable())
			super.validate(validationContext);
	}

	/**
	 * Determine if the editor should be read-only
	 * 
	 * @param aWriter
	 *           the writer
	 * @param aContext
	 *            the context
	 * @return whether the editor should be read-only
	 */
	protected boolean isReadOnly(WidgetWriter aWriter, WidgetDisplayContext aContext)
	{
		return factory.isNeverEditable() || !aContext.getPermission().isReadWrite()
			|| !isUserAlwaysReadWrite(aWriter.getSession());
	}

	/**
	 * Checks if a user is always read/write, based on the specified
	 * <code>editorRoles</code>
	 *
	 * @param session
	 *            the session
	 * @return whether the user is always read/write
	 */
	protected boolean isUserAlwaysReadWrite(Session session)
	{
		String editorRolesToUse = (factory.getEditorRoles() == null) ? getDefaultEditorRoles()
			: factory.getEditorRoles();
		if (editorRolesToUse != null)
		{
			DirectoryHandler dirHandler = session.getDirectory();
			UserReference user = session.getUserReference();
			String[] editorRolesToUseArr = editorRolesToUse.split(SEPARATOR);
			for (String editorRole : editorRolesToUseArr)
			{
				String roleName = editorRole.trim();
				if (!"".equals(roleName))
				{
					if (dirHandler.isUserInRole(user, Role.forSpecificRole(roleName)))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Define the default roles to use if none are configured on the bean. By
	 * default, it is Tech Admin but this can be overwritten by subclasses.
	 */
	protected String getDefaultEditorRoles()
	{
		return CommonConstants.ROLE_TECH_ADMIN;
	}

	/**
	 * Adds the editor when it's read-only. Adds the default editor unless
	 * overridden.
	 */
	protected void doAddForDisplay(WidgetWriter aWriter)
	{
		UIWidget widget = aWriter.newBestMatching(Path.SELF);
		widget.setEditorDisabled(true);
		aWriter.addWidget(widget);
	}

	protected void doAddForEdit(WidgetWriter aWriter, WidgetDisplayContext aContext)
	{
		aWriter.addWidget(Path.SELF);
	}

	@Override
	public void write(WidgetWriter aWriter, WidgetDisplayContext aContext)
	{
		if (aContext.isDisplayedInTable() || isReadOnly(aWriter, aContext))
		{
			doAddForDisplay(aWriter);
		}
		else
		{
			doAddForEdit(aWriter, aContext);
		}
	}
}

/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.orchestranetworks.ps.widget;

import java.util.*;

import com.orchestranetworks.instance.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.ui.form.widget.*;

/**
 * This is an editor so that list-fields can be displayed in a table.
 */
public class ListUICustomWidget extends UIListCustomWidget
{
	private final ListUIWidgetFactory factory;

	public ListUICustomWidget(WidgetFactoryContext aContext, ListUIWidgetFactory factory)
	{
		super(aContext);
		this.factory = factory;
	}

	@SuppressWarnings("rawtypes")
	private static void doAddForDisplay(
		WidgetWriterForList aWriter,
		WidgetDisplayContext aContext,
		String separator)
	{
		SchemaNode node = aContext.getNode();
		Path path = node.getPathInAdaptation();
		if (path.isIndexed())
		{
			UIWidget widget = aWriter.newBestMatching(Path.SELF);
			widget.setEditorDisabled(true);
			aWriter.addWidget(widget);
			return;
		}
		ValueContext vc = aContext.getValueContext();
		List values = (List) vc.getValue();
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (Object value : values)
		{
			value = node.displayOccurrence(value, true, vc, Locale.getDefault());
			if (!first)
				sb.append(separator);
			else
				first = false;
			sb.append(value);
		}
		aWriter.add(sb.toString());
	}

	@Override
	public void write(WidgetWriterForList aWriter, WidgetDisplayContext aContext)
	{
		if (aContext.isDisplayedInTable())
		{
			doAddForDisplay(aWriter, aContext, factory.getSeparator());
		}
		else
		{
			if (factory.isReadOnly())
			{
				UIWidget widget = aWriter.newBestMatching(Path.SELF);
				widget.setEditorDisabled(true);
				aWriter.addWidget(widget);
			}
			else
			{
				aWriter.addWidget(Path.SELF);
			}
		}
	}

}

package com.orchestranetworks.ps.widget;

import com.orchestranetworks.ui.form.widget.*;

/**
 * A widget that is read only when the field has a value. It extends from {@link ReadOnlyUICustomWidget} so that
 * it can get the base behavior of making it read only, and take advantage of the <code>editorRoles</code> parameter.
 * However, it will only consider it read only if it is normally read only by the parent class and it contains a value.
 * 
 * This can be useful when you have a field that is sometimes given a default upon initial creation and you don't want
 * the user to change it, but when there is no default, you want them to be able to enter a value.
 * 
 * You may also want them to be able to change the value when they're duplicating a record because in that case, it's not
 * really the default, it just has a value due to having duplicated a record. So there is a parameter on the factory called
 * <code>readWriteWhileDuplicating<code> that can be used for that.
 */
public class ReadOnlyWhenHasValueUICustomWidget extends ReadOnlyUICustomWidget
{
	@SuppressWarnings("rawtypes")
	public ReadOnlyWhenHasValueUICustomWidget(
		WidgetFactoryContext aContext,
		ReadOnlyWhenHasValueUIWidgetFactory factory)
	{
		super(aContext, factory);
	}

	@SuppressWarnings("rawtypes")
	protected boolean isReadOnly(WidgetWriter aWriter, WidgetDisplayContext aContext)
	{
		boolean readOnly = super.isReadOnly(aWriter, aContext);
		// If the parent implementation says it's read only
		if (readOnly)
		{
			// If it doesn't have a value, it's actually going to be read/write
			if (aContext.getValueContext().getValue() == null)
			{
				readOnly = false;
			}
			// Otherwise it has a value so it's only going to be read only if
			// we're not duplicating when we've said it shouldn't be read/write while duplicating.
			else
			{
				readOnly = !(aContext.isDuplicatingRecord()
					&& ((ReadOnlyWhenHasValueUIWidgetFactory) factory)
						.isReadWriteWhenDuplicating());
			}
		}
		return readOnly;
	}
}

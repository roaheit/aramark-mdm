package com.orchestranetworks.ps.service;

import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;
import com.orchestranetworks.userservice.declaration.*;

public class GenericServiceDeclaration<S extends EntitySelection, U extends ActivationContext<S>>
	implements UserServiceDeclaration<S, U>
{
	private final ServiceKey serviceKey;
	private final Class<? extends UserService<S>> implementation;
	private final String title;
	private final String description;
	private final String instruction;

	public GenericServiceDeclaration(
		ServiceKey aServiceKey,
		Class<? extends UserService<S>> implementation,
		String aTitle,
		String aDescription,
		String anInstruction)
	{
		this.serviceKey = aServiceKey;
		this.implementation = implementation;
		this.title = aTitle;
		this.description = aDescription;
		this.instruction = anInstruction;
	}

	@Override
	public final ServiceKey getServiceKey()
	{
		return this.serviceKey;
	}

	public final String getTitle()
	{
		return this.title;
	}

	public final String getDescription()
	{
		return this.description;
	}

	public final String getInstruction()
	{
		return this.instruction;
	}

	@Override
	public void defineProperties(UserServicePropertiesDefinitionContext aDefinition)
	{
		if (title != null)
		{
			aDefinition.setLabel(title);
		}
		if (description != null)
		{
			aDefinition.setDescription(description);
		}
		if (instruction != null)
		{
			aDefinition.setConfirmationMessageBeforeLaunch(instruction);
		}
	}

	@Override
	public void declareWebComponent(WebComponentDeclarationContext aDeclaration)
	{
		aDeclaration.setAvailableAsPerspectiveAction(true);
		aDeclaration.setAvailableAsWorkflowUserTask(true);
	}

	@Override
	public UserService<S> createUserService()
	{
		try
		{
			return implementation.newInstance();
		}
		catch (InstantiationException | IllegalAccessException e)
		{
			throw new RuntimeException(e);
		}
	}

	@Override
	public void defineActivation(U aContext)
	{
	}

}

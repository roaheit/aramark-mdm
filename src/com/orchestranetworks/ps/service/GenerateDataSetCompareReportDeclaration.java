package com.orchestranetworks.ps.service;

import com.orchestranetworks.ps.servicepermission.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;
import com.orchestranetworks.userservice.declaration.*;

public class GenerateDataSetCompareReportDeclaration
	extends
	GenericServiceDeclaration<DatasetEntitySelection, ActivationContextOnDataset>
	implements UserServiceDeclaration.OnDataset

{

	public GenerateDataSetCompareReportDeclaration(String moduleName)
	{
		super(
			moduleName == null ? ServiceKey.forName("GenerateDataSetCompareReport")
				: ServiceKey.forModuleServiceName(moduleName, "GenerateDataSetCompareReport"),
			null,
			"Generate Dataset Compare Report",
			"Download an excel workbook detailing the differences between this data set and the same from the data space's initial snapshot",
			null);
	}

	@Override
	public void defineActivation(ActivationContextOnDataset definition)
	{
		definition.setPermissionRule(new ChildDataSpaceOnlyServicePermissionRule<>());
	}

	@Override
	public UserService<DatasetEntitySelection> createUserService()
	{
		return new GenerateCompareReport<>();
	}

}

package com.orchestranetworks.ps.service;

import java.io.*;
import java.util.*;

import org.apache.commons.collections4.*;
import org.apache.poi.ss.usermodel.*;

import com.onwbp.org.apache.commons.io.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.*;

/**
 * Export data set to an excel workbook. Give details of tables and fields and
 * recurse into reachable data sets.
 */
public abstract class AbstractFileDownloadUserService<S extends DataspaceEntitySelection>
	extends
	AbstractUserService<S>
{
	private boolean complete;
	public abstract List<File> getFiles();

	public static File saveWorkbook(Workbook workbook, File file)
	{
		OutputStream os = null;
		try
		{
			os = new FileOutputStream(file);
			workbook.write(os);
			os.flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			IOUtils.closeQuietly(os);
		}
		return file;
	}

	@Override
	public void setupDisplay(
		UserServiceSetupDisplayContext<S> aContext,
		UserServiceDisplayConfigurator aConfigurator)
	{
		if (!complete && submitted)
			aConfigurator.setLeftButtons(aConfigurator.newCloseButton());
		super.setupDisplay(aContext, aConfigurator);
	}

	@Override
	public void landService()
	{
		List<File> files = getFiles();
		if (!CollectionUtils.isEmpty(files) && !complete)
		{
			for (File file : files)
			{
				writeDownloadLink(file);
			}
			complete = true;
		}
		else
		{
			super.landService();
		}
	}

	@Override
	protected void writeResultPane(
		UserServicePaneContext aPaneContext,
		UserServicePaneWriter aWriter)
	{
		if (!complete)
			super.writeResultPane(aPaneContext, aWriter);
		else
			landService();
	}
}

package com.orchestranetworks.ps.servicepermission;

import com.orchestranetworks.ps.workflow.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.permission.*;

/**
 * This is a service that is only allowed from within the Create/Update Price Tier Workflow
 */
public class WorkflowPermissionProfileOnlyServicePermissionRule<S extends DataspaceEntitySelection>
	extends
	MasterOrChildDataSpaceOnlyServicePermissionRule<S>
{

	//TODO:  convert to a collection of profiles... use static initializer to create the collection from the string
	protected String workflowPermissionProfile = null;

	public WorkflowPermissionProfileOnlyServicePermissionRule()
	{
		super();
		allowInsideWorkflow = true;
		allowOutsideWorkflow = false;
	}

	@Override
	public UserServicePermission getPermission(ServicePermissionRuleContext<S> aContext)
	{
		if (workflowPermissionProfile != null
			&& workflowPermissionProfile.equals(WorkflowUtilities.getTrackingInfoPermissionsUser(aContext.getSession())))
		{
			return super.getPermission(aContext);
		}
		return UserServicePermission.getDisabled();
	}

	public String getWorkflowPermissionProfile()
	{
		return workflowPermissionProfile;
	}

	public void setWorkflowPermissionProfile(String workflowPermissionProfile)
	{
		this.workflowPermissionProfile = workflowPermissionProfile;
	}

}

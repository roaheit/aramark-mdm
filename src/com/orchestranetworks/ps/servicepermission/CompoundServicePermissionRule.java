package com.orchestranetworks.ps.servicepermission;

import java.util.*;

import com.onwbp.base.text.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.permission.*;

public class CompoundServicePermissionRule<S extends DataspaceEntitySelection>
	implements ServicePermissionRule<S>
{
	private List<ServicePermissionRule<S>> componentRules = new ArrayList<>();

	@Override
	public UserServicePermission getPermission(ServicePermissionRuleContext<S> arg0)
	{
		UserServicePermission result = UserServicePermission.getEnabled();
		for (ServicePermissionRule<S> servicePermissionRule : componentRules)
		{
			UserServicePermission subResult = servicePermissionRule.getPermission(arg0);
			result = combine(result, subResult);
		}
		return result;
	}

	private static UserServicePermission combine(
		UserServicePermission result,
		UserServicePermission newSub)
	{
		if (newSub.isDisabled())
		{
			if (result.isDisabled())
				return UserServicePermission.getDisabled(
					appendMessage(result.getDisabledReason(), newSub.getDisabledReason()));
			else
				return newSub;
		}
		return result;
	}

	private static UserMessage appendMessage(UserMessage left, UserMessage right)
	{
		if (left == null)
			return right;
		if (right == null)
			return left;
		StringBuilder msg = new StringBuilder();
		msg.append(left.formatMessage(Locale.getDefault())).append(" and ").append(
			right.formatMessage(Locale.getDefault()));
		return UserMessage.createError(msg.toString());
	}

	public CompoundServicePermissionRule<S> appendRule(ServicePermissionRule<S> rule)
	{
		componentRules.add(rule);
		return this;
	}
}

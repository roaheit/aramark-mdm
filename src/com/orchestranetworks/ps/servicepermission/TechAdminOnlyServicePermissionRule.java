package com.orchestranetworks.ps.servicepermission;

import com.orchestranetworks.ps.constants.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.service.directory.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.userservice.permission.*;

public class TechAdminOnlyServicePermissionRule<S extends DataspaceEntitySelection>
	implements ServicePermissionRule<S>
{

	@Override
	public UserServicePermission getPermission(ServicePermissionRuleContext<S> context)
	{
		Session session = context.getSession();
		//the first time you set up the repository, need to bring in roles
		DirectoryHandler directory = DirectoryHandler
			.getInstance(context.getEntitySelection().getDataspace().getRepository());
		if (directory.isSpecificRoleDefined(CommonConstants.TECH_ADMIN))
		{
			if (session.isUserInRole(CommonConstants.TECH_ADMIN))
				return UserServicePermission.getEnabled();
		}
		else if (session.isUserInRole(Profile.ADMINISTRATOR))
		{
			return UserServicePermission.getEnabled();
		}
		return UserServicePermission.getDisabled();
	}
}
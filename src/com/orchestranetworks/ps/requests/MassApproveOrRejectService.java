package com.orchestranetworks.ps.requests;

import java.util.*;

import com.onwbp.adaptation.*;
import com.orchestranetworks.interactions.*;
import com.orchestranetworks.ps.service.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.selection.*;
import com.orchestranetworks.workflow.*;
import com.orchestranetworks.workflow.UserTask.*;

public class MassApproveOrRejectService extends AbstractUserService<TableViewEntitySelection>
{
	private final boolean approve;
	private final RequestPathConfig config;

	public MassApproveOrRejectService(RequestPathConfig config, boolean approve)
	{
		super();
		this.config = config;
		this.approve = approve;
	}

	public void execute(Session session) throws OperationException
	{
		// this service is available on Request records only (as specified in the request path
		// config).
		TableViewEntitySelection selection = context.getEntitySelection();
		List<Adaptation> rows = AdaptationUtil.getRecords(selection.getSelectedRecords().execute());
		WorkflowEngine we = WorkflowEngine
			.getFromRepository(selection.getDataspace().getRepository(), session);
		UserReference user = session.getUserReference();
		List<WorkItem> workItemsOffered = we.getWorkItemsOfferedToUser(user);
		Map<ProcessInstanceKey, WorkItem> offered = createWorkItemMap(workItemsOffered);
		List<WorkItem> workItemsTaken = we.getWorkItemsAllocatedToUser(user);
		Map<ProcessInstanceKey, WorkItem> taken = createWorkItemMap(workItemsTaken);
		for (Adaptation row : rows)
		{
			ProcessInstanceKey pik = ProcessInstanceKey
				.parse(row.getString(config.getRequestProcessInstanceKeyPath()));
			WorkItem workItem = offered.get(pik);
			if (workItem == null)
				workItem = taken.get(pik);
			if (workItem != null)
			{
				WorkItemKey wik = workItem.getWorkItemKey();
				if (!user.equals(workItem.getUserReference()))
				{
					we.allocateWorkItemToMyself(wik);
				}
				SessionInteraction si = we.createOrOpenInteraction(wik);
				if (approve)
				{
					si.setComment("Batch-advanced by " + user.getLabel());
					si.accept();
				}
				else if (workItem.isRejectEnabled())
				{
					si.setComment("Batch-rejected by " + user.getLabel());
					si.reject();
				}
			}
		}
	}

	public static Map<ProcessInstanceKey, WorkItem> createWorkItemMap(List<WorkItem> workItems)
	{
		Map<ProcessInstanceKey, WorkItem> result = new HashMap<>();
		for (WorkItem workItem : workItems)
		{
			ProcessInstanceKey pik = workItem.getProcessInstanceKey();
			if (!result.containsKey(pik))
				result.put(pik, workItem);
		}
		return result;
	}

	public boolean isApprove()
	{
		return approve;
	}

}

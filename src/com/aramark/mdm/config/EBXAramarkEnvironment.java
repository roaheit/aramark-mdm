package com.aramark.mdm.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.aramark.mdm.job.ImportParameter;

@Component
@Qualifier("EBXAramarkEnvironment")
public final class EBXAramarkEnvironment {

 
	public static String exbServiceUser;
	@Value("${ebx.service.user}")
	private String _exbServiceUser;

	public static String exbServicePassword;
	@Value("${ebx.service.password}")
	private String _exbServicePassword;

	public static String ebxDBUrl;
	@Value("${ebx.db.url}")
	private String _ebxDBUrl;

	public static String exbDBUser;
	@Value("${ebx.db.user}")
	private String _exbDBUser;

	public static String exbDBPassword;
	@Value("${ebx.db.password}")
	private String _exbDBPassword;
	
	
	/*public static String googleAPIKey;
	@Value("${ebx.google.api.key}")
	private String _googleAPIKey;*/
	
	public static String googleClientId;
	@Value("${ebx.google.client.Id}")
	private String _googleClientId;
	
	public static String googleChannelId;
	@Value("${ebx.google.channel.Id}")
	private String _googleChannelId;
	
	@Autowired
	public Environment environment;

	@Value("#{'${workflow.inbound}'.split(',')}")
	private List<String> jobRecords;
	
	public static Map<String, List<ImportParameter>> importJobs;
	private Map<String, List<ImportParameter>> _importJobs = new HashMap<String, List<ImportParameter>>();
	
	
	@PostConstruct
	public void init() {
		EBXAramarkEnvironment.exbServiceUser = _exbServiceUser;
		EBXAramarkEnvironment.exbServicePassword = _exbServicePassword;
		EBXAramarkEnvironment.ebxDBUrl = _ebxDBUrl;
		EBXAramarkEnvironment.exbDBUser = _exbDBUser;
		EBXAramarkEnvironment.exbDBPassword = _exbDBPassword;
		EBXAramarkEnvironment.googleChannelId = _googleChannelId;
		EBXAramarkEnvironment.googleClientId = _googleClientId;
		//EBXAramarkEnvironment.googleAPIKey = _googleAPIKey;
		
	
		 for (String jobRecord: jobRecords)
		 {
			 String[] jobFields = jobRecord.split(";");
			 String workFlowKey = jobFields[0];
			 String importFileName = jobFields[1];
			 String importPath = jobFields[2];
			 ImportParameter ip = new ImportParameter(importFileName, importPath); 
			 if (_importJobs.containsKey(workFlowKey))
			 {
				 
				 _importJobs.get(workFlowKey).add(ip);
			 }
			 else {
			 
				 List <ImportParameter> jpl = new ArrayList<ImportParameter>();
				 jpl.add(ip);
				 _importJobs.put(workFlowKey, jpl);
		
			 }
		};	
		
		EBXAramarkEnvironment.importJobs = _importJobs;
	}

		private EBXAramarkEnvironment() {
			
	}
	
	
	
	
	
}
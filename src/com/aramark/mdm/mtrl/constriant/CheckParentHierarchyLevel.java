package com.aramark.mdm.mtrl.constriant;

import java.util.*;

import com.aramark.mdm.mtrl.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.ValueContextForUpdate;

public class CheckParentHierarchyLevel implements ConstraintOnNull,  Constraint
{
	
	private static String MESSAGE = "Parent material specified must be one level above the child material level in the hierarchy";
	
	@Override
	public void checkNull(ValueContextForValidation context) throws InvalidSchemaException
	{
		// nothing to do
	}

	@Override
	public void checkOccurrence(Object value, ValueContextForValidation valueContext)
		throws InvalidSchemaException
	{
	    //  the parent in the hierarchy is passed in via "value", if no parent specified then return.  
		if (value != null)
		{
			Integer intParentHierLevel = null; 
			Integer intChildHierLevel = null; 
			String parentHierLevel = null;
			
			// given we have the compound key of the parent in the variable value.  What method of AdaptationUtil method do we use 
			// to retrieve that data? 
			String parent = valueContext.getValue().toString(); 			
						
			String childHierLevelstr = valueContext.getValue(Path.PARENT.add(MaterialPaths._Root_MATERIAL_DETAILS_MATERIAL_HIERARCHY._MATERIAL_HIERARCHY_LEVEL)).toString();			
			Adaptation parentRecord = valueContext.getAdaptationTable().lookupAdaptationByPrimaryKey(PrimaryKey.parseString(parent));
			try
			{
				if (parentRecord != null)
				{
					parentHierLevel = parentRecord.getString(
						MaterialPaths._Root_MATERIAL_DETAILS_MATERIAL_HIERARCHY._MATERIAL_HIERARCHY_LEVEL);
				}
				else if (parentRecord == null)
				{
					throw new NullPointerException();
				}

				intParentHierLevel = Integer.parseInt(parentHierLevel); 
				intChildHierLevel  = Integer.parseInt(childHierLevelstr); 
				Integer difference = intChildHierLevel - intParentHierLevel;
				if (difference.equals(1))
				{
					return;
				}
				else
				{
					valueContext.addError(MESSAGE);
				}
			}
			catch (NumberFormatException ex)
			{
				valueContext.addError("The hierarchy levels specified must contain only numbers");
			}
			catch (NullPointerException ex)
			{
				valueContext.addError("Parent deleted without deleting child");
			}

			
		}
	}
	
	@Override
	public void setup(ConstraintContext aContext)
	{
		// nothing to do

	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}


}

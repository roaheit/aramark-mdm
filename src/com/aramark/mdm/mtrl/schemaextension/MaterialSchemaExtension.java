package com.aramark.mdm.mtrl.schemaextension;

import com.aramark.mdm.mtrl.accessrule.MaterialWorkflowAccessRule;
import com.aramark.mdm.mtrl.path.MaterialPaths;
import com.orchestranetworks.ps.accessrule.AccessRulesManager;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;

public class MaterialSchemaExtension implements SchemaExtensions
{

	@Override
	public void defineExtensions(SchemaExtensionsContext context)
	{

		AccessRulesManager manager = new AccessRulesManager(context);
		manager.setAccessRuleOnNodeAndAllDescendants(
			MaterialPaths._Root,
			false,
			new MaterialWorkflowAccessRule());

	}

}

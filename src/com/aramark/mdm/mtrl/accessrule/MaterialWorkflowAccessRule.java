package com.aramark.mdm.mtrl.accessrule;

import com.aramark.mdm.common.constants.AramarkMaterialConstants;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ps.accessrule.WorkflowAccessRule;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;

public class MaterialWorkflowAccessRule extends WorkflowAccessRule
{

	@Override
	public AccessPermission getPermission(
		Adaptation aAdaptation,
		Session aSession,
		SchemaNode aNode)
	{
	
		if (aSession
			.isUserInRole(Role.forSpecificRole(AramarkMaterialConstants.RECIPE_CREATE_ROLE)))
		{
			return AccessPermission.getReadWrite();
		}
		return super.getPermission(aAdaptation, aSession, aNode);
	}

}

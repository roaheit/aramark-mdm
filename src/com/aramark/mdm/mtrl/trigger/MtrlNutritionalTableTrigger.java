package com.aramark.mdm.mtrl.trigger;

import java.util.HashMap;

import com.aramark.mdm.common.constants.AramarkOrganizationConstants;
import com.aramark.mdm.mtrl.path.MaterialPaths;
import com.aramark.mdm.mtrl.path.MaterialReferencePaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.procedure.CreateRecordProcedure;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;

public class MtrlNutritionalTableTrigger extends BaseTableTrigger
{



	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context) throws OperationException
	{
		ProcedureContext pContext = context.getProcedureContext();
		Adaptation nutrFactAdaptation = context.getAdaptationOccurrence();
		AdaptationTable nutritionalFactDataTable = nutrFactAdaptation.getContainer()
			.getTable(MaterialPaths._Root_AramarkNutrition_NutrFactData.getPathInSchema());
		Adaptation materialReferenceDataSet = getMaterialReferenceDataSet(
			context.getAdaptationHome().getRepository());
		AdaptationTable nutrientDefinitionTable = materialReferenceDataSet
			.getTable(MaterialReferencePaths._Root_Reference_NutrDef.getPathInSchema());
		RequestResult reqResult = nutrientDefinitionTable.createRequestResult(null);
		if (reqResult != null)
		{
			Adaptation nutrientDefinition = null;
			try
			{
				while ((nutrientDefinition = reqResult.nextAdaptation()) != null)
				{
					HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();

					pathValueMap.put(
						MaterialPaths._Root_AramarkNutrition_NutrFactData._NutrFactId,
						nutrFactAdaptation.getOccurrencePrimaryKey().format());
					pathValueMap.put(
						MaterialPaths._Root_AramarkNutrition_NutrFactData._NutrName,
						nutrientDefinition.getOccurrencePrimaryKey().format());
					CreateRecordProcedure
						.execute(pContext, nutritionalFactDataTable, pathValueMap, true, true);
				}
			}
			finally
			{
				reqResult.close();
			}
		}


	}

	protected Adaptation getMaterialReferenceDataSet(Repository repo) throws OperationException
	{
		AdaptationHome materialReferenceDataSpace = AdaptationUtil
			.getDataSpaceOrThrowOperationException(
				repo,
				AramarkOrganizationConstants.MTRL_MASTER_DATA_SP);
		Adaptation materialReferenceDataSet = AdaptationUtil.getDataSetOrThrowOperationException(
			materialReferenceDataSpace,
			AramarkOrganizationConstants.MTRL_MASTER_REF_DATA);

		return materialReferenceDataSet;
	}



}

package com.aramark.mdm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.aramark.mdm.common.type.AramarkMDMException;
import com.aramark.mdm.config.EBXAramarkEnvironment;

@Component
public class SequenceNumberDAO {

	final static Logger logger = Logger.getLogger(SequenceNumberDAO.class);

	public Connection openConnection() {

		Connection connection = null;

		try {
			String userName = EBXAramarkEnvironment.exbDBUser;
			String password = EBXAramarkEnvironment.exbDBPassword;
			String url = EBXAramarkEnvironment.ebxDBUrl;

			Properties connectionProps = new Properties();
			connectionProps.put("user", userName);
			connectionProps.put("password", password);
			return DriverManager.getConnection(url, connectionProps);

		} catch (SQLException e) {
			logger.error(e.getMessage());
		}

		return connection;

	}

	public Long getNextVal(String sequenceName) {

		Long seqVal = new Long(0);
		Connection conn = null;
		ResultSet rs = null;
		PreparedStatement pst = null;

		try {

			conn = openConnection();

			String sql = "SELECT " + sequenceName + ".NEXTVAL FROM DUAL";

			pst = conn.prepareStatement(sql);
			synchronized (this) {
				rs = pst.executeQuery();
				if (rs.next()) {
					seqVal = rs.getLong(1);
				} else {
					throw new AramarkMDMException("No sequence value returned from " + sequenceName);
				}
			}

			// logger.debug("Got next sequence value:" + seqVal + " from " + sequenceName);
		} catch (SQLException e) {
			logger.error(e.getMessage());
			throw new AramarkMDMException(e.getMessage());

		} finally {
			try {
				rs.close();
				pst.close();
				conn.close();
			} catch (SQLException e) {

				e.printStackTrace();
			}

		}
		return seqVal;

	}
}
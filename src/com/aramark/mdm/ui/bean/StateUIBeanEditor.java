package com.aramark.mdm.ui.bean;

import org.apache.log4j.Logger;

import com.aramark.mdm.geography.GeographyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;
import com.orchestranetworks.ui.form.widget.UIWidget;

public class StateUIBeanEditor extends UIBeanEditor {
	
	final static Logger logger = Logger.getLogger(StateUIBeanEditor.class);
		
	private void translateState(UIResponseContext context) {

		if (context.getValue() != null) {
 			Repository repository = Repository.getDefault();
			// Get state table
			AdaptationHome dataSpace = repository.lookupHome(HomeKey.forBranchName("GeographyDataSpace"));
			Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName("GeographyDataSet"));
			// Get state table
			AdaptationTable stateTable = dataSet.getTable(GeographyPaths._Root_STATE_PROVINCE.getPathInSchema());
			String stateCode = (String)  context.getValue();
		
			String predicate = GeographyPaths._Root_STATE_PROVINCE._STATE_PROVINCE_CD.format() + "= '" + stateCode + "'";
			
			RequestResult rr = stateTable.createRequestResult(predicate);

			if (rr != null) {
				try {
					Adaptation record = null;
					if ((record = rr.nextAdaptation()) != null) {						
						context.add(record.getOccurrencePrimaryKey().format()); 
					}

				} finally {
					rr.close();
				}

			} else {
				UIWidget widget = context.newBestMatching(Path.SELF);
				context.addWidget(widget);
			}
		} else {

			UIWidget widget = context.newBestMatching(Path.SELF);
			context.addWidget(widget);
		}
	}
	
	@Override
	public void addForDisplay(UIResponseContext context) {
		translateState(context);
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		translateState(context);
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {
		translateState(context);
	}



}

package com.aramark.mdm.ui.bean;

import org.apache.log4j.Logger;

import com.aramark.mdm.geography.GeographyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;
import com.orchestranetworks.ui.form.widget.UIWidget;

public class CountryUIBeanEditor extends UIBeanEditor {
	
	final static Logger logger = Logger.getLogger(CountryUIBeanEditor.class);
		
	private void translateISOCountryCode(UIResponseContext context) {

		if (context.getValue() != null) {
			Repository repository = Repository.getDefault();
			AdaptationHome dataSpace = repository.lookupHome(HomeKey.forBranchName("GeographyDataSpace"));
			Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName("GeographyDataSet"));
			AdaptationTable table = dataSet.getTable(GeographyPaths._Root_COUNTRY.getPathInSchema());
			String countryCode = (String)  context.getValue();
			context.add(countryCode); // displays 3 char ISO code

		} else {

			UIWidget widget = context.newBestMatching(Path.SELF);
			context.addWidget(widget);
		}
	}
	
	@Override
	public void addForDisplay(UIResponseContext context) {
		translateISOCountryCode(context);
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		translateISOCountryCode(context);
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {
		translateISOCountryCode(context);
	}



}

package com.aramark.mdm.ui.bean;

import org.apache.log4j.Logger;

import com.aramark.mdm.geography.GeographyPaths;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.ui.UIBeanEditor;
import com.orchestranetworks.ui.UIResponseContext;
import com.orchestranetworks.ui.form.widget.UIWidget;

public class AddressLineOneUIBeanEditor extends UIBeanEditor {
	
	final static Logger logger = Logger.getLogger(AddressLineOneUIBeanEditor.class);
		
	private void prependStreeNumber(UIResponseContext context) {

		if (context.getValue() != null) {
			String streetNumber = (String) context.getValue(Path.PARENT.add(GeographyPaths._Root_ADDRESS._STREET_NUMBER));
			if (streetNumber != null && streetNumber.length() > 0)
				context.add(streetNumber + " ");
		}
		UIWidget widget = context.newBestMatching(Path.SELF);
		context.addWidget(widget);

	}
	
	@Override
	public void addForDisplay(UIResponseContext context) {
		prependStreeNumber(context);
	}

	@Override
	public void addForEdit(UIResponseContext context) {
		prependStreeNumber(context);
	}

	@Override
	public void addForDisplayInTable(UIResponseContext context) {
		prependStreeNumber(context);
	}



}

package com.aramark.mdm.ui.form.address;

import java.util.*;

import org.apache.log4j.*;

import com.aramark.mdm.geography.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.googlemapsinterface.googlemaps.*;
import com.orchestranetworks.ps.util.googlemapsinterface.googlemaps.GoogleMaps.AddressEntryMode;
import com.orchestranetworks.ps.util.googlemapsinterface.message.*;
import com.orchestranetworks.ps.util.googlemapsinterface.ui.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.ui.*;
import com.orchestranetworks.ui.form.*;

public class AddressMainPane implements UIFormPane
{

	final static Logger logger = Logger.getLogger(AddressMainPane.class);

	
	@Override
	public void writePane(final UIFormPaneWriter pWriter, final UIFormContext pContext)
	{
		Locale locale = pContext.getLocale();

		boolean withinMaps = false;
		String trackingInfo = pContext.getSession().getTrackingInfo();
		if (trackingInfo != null
			&& trackingInfo.equals(AddressFormMapping.TrackingInfo.WITHIN_GOOGLE_MAPS))
		{
			withinMaps = true;
		}

		Adaptation record = pContext.getCurrentRecord();

		boolean readWrite = false;
		if (record != null)
		{
			readWrite = pContext.getSession()
				.getPermissions()
				.getAdaptationAccessPermission(record)
				.isReadWrite();
		}
		else
		{
			AdaptationTable table = pContext.getCurrentTable();
			readWrite = pContext.getSession()
				.getPermissions()
				.getTableActionPermissionToCreateRootOccurrence(table)
				.isEnabled();
		}

		// Instantiate a new GoogleMapsAddressMapping object
		GoogleMapsAddressFieldMapping mapping = AddressFormMapping.getAddressGoogleMapsMapping();

		// Instantiate a new GoogleMaps object
		GoogleMaps googleMaps = new GoogleMaps(mapping);

		String mapId = "LocationMap";
		String autocompleteFieldId = "LocationAutocompleteField";

		// Left side
		pWriter.add("<div style=\"width:40%; float:left; min-width:460px;\">");

		String leftDivStyle = "";
		leftDivStyle += "width:90%;";
		if (!withinMaps)
		{
			leftDivStyle += "padding-top:30px;";
		}
		leftDivStyle += "-webkit-box-sizing:border-box; -moz-box-sizing:border-box; box-sizing:border-box;";

		pWriter.add("<div style=\"" + leftDivStyle + "\">");

		pWriter.startTableFormRow();

		SchemaNode rootNode = pContext.getCurrentTable().getTableOccurrenceRootNode();

		SchemaNode addressNode = rootNode.getNode(GeographyPaths._Root_ADDRESS.getPathInSchema());
		UIUtils.addTitleFormRow(pWriter, addressNode.getLabel(locale));

		if (readWrite && !withinMaps)
		{
			pWriter.startFormRow(new UIFormLabelSpec(""));
			String placeholder = Messages.get(this.getClass(), locale, "addressFieldPlaceholder");

			// Insert the Google Maps Autocomplete field
			googleMaps.insertAutocompleteAddressField(
				pWriter,
				placeholder,
				"width:250px;",
				mapId,
				autocompleteFieldId);

			pWriter.endFormRow();
		}

		UIUtils.addSubNodesFormRow(pWriter, addressNode, null);
		
		pWriter.addJS_cr("var comm = 'alert(\"hi\");'");		
		pWriter.addJS_cr("var comm1 = '$(\"_POSTAL_CODE\").hide();'");		

		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._STREET_NUMBER);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._ADDRESS_LINE_ONE);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._CITY_LOCALITY_NAME);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._STATE_PROVINCE);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._POSTAL_CODE);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._COUNTY_NAME);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._COUNTRY);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._LATITUDE);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._LONGITUDE);
		pWriter.addFormRow(GeographyPaths._Root_ADDRESS._IS_VALIDATED);
		pWriter.endTableFormRow();
		// Always hide the IS_VALIDATED field
		pWriter.addJS_cr("document.getElementById('_IS_VALIDATED').style.visibility = 'hidden';");
		pWriter.addJS_cr("document.querySelector('#_IS_VALIDATED~button').style.visibility = 'hidden';");	
		pWriter.addJS_cr("document.querySelector('#_IS_VALIDATED~button~button').style.visibility = 'hidden';");
		pWriter.addJS_cr("document.querySelector('label[for=_IS_VALIDATED]').parentNode.parentNode.style.visibility = 'hidden';");
		pWriter.addJS_cr("document.getElementsByName(\"_IS_VALIDATED\")[0].value =\"Y\";");		
		// set up geocoding mode as default
	 	googleMaps.setGeoCodingMode(pWriter);
		pWriter.add("</div>");
		pWriter.add("</div>");

		// Right side
		if (!withinMaps)
		{
			pWriter.add("<div style=\"width:60%; float:right; width:500px;  min-width:460px;\">");

			String rightDivStyle = "";
			rightDivStyle += "width:100%;";
			rightDivStyle += "padding:30px;";
			rightDivStyle += "-webkit-box-sizing:border-box; -moz-box-sizing:border-box; box-sizing:border-box;";

			pWriter.add("<div style=\"" + rightDivStyle + "\">");

			// Instantiate a new GoogleMapsAddressValue object
			GoogleMapsAddressValue addressValue = GoogleMapsAddressValue
				.createAddressValue(record, mapping, locale);

			// Insert the Map
			googleMaps.insertMap(pWriter, addressValue, "width:100%; height:400px;", mapId);

			pWriter.add("</div>");
			pWriter.add("</div>");
		}
	}
}

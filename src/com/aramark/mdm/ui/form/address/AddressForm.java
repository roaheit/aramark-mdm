package com.aramark.mdm.ui.form.address;


import org.apache.log4j.Logger;

import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ui.form.UIForm;
import com.orchestranetworks.ui.form.UIFormBody;
import com.orchestranetworks.ui.form.UIFormContext;
import com.orchestranetworks.ui.form.UIFormPaneWithTabs;

public class AddressForm extends UIForm
{
	final static Logger logger = Logger.getLogger(AddressForm.class);

	@Override
	public void defineBody(final UIFormBody pBody, final UIFormContext pContext)
	{
	

		if (pContext.isCreatingRecord())
		{
			pBody.setContent(new AddressMainPane());
		}
		else
		{
			UIFormPaneWithTabs tabs = new UIFormPaneWithTabs();

			tabs.setHomeIconDisplayed(true);
			tabs.addTab(UserMessage.createInfo(""), new AddressMainPane());

			pBody.setContent(tabs);
		}
	}
}

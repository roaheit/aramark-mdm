package com.aramark.mdm.ui.form.address;

import org.apache.log4j.Logger;

import com.aramark.mdm.geography.GeographyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.util.googlemapsinterface.googlemaps.GoogleMapsAddressFieldMapping;
import com.orchestranetworks.ps.util.googlemapsinterface.googlemaps.GoogleMapsAddressFieldMapping.ForeignKeyMapping;

public final class AddressFormMapping
{
	final static Logger logger = Logger.getLogger(AddressFormMapping.class);
	
	public class TrackingInfo
	{
		public static final String WITHIN_GOOGLE_MAPS = "withinGoogleMaps";
	}

	public static GoogleMapsAddressFieldMapping getAddressGoogleMapsMapping()
	{
		Repository repository = Repository.getDefault();
		GoogleMapsAddressFieldMapping mapping = new GoogleMapsAddressFieldMapping();
		// get the lookup tables, state and country
		AdaptationHome dataSpace = repository.lookupHome(HomeKey.forBranchName("GeographyDataSpace"));
		Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName("GeographyDataSet"));
		AdaptationTable countriesTable = dataSet.getTable(GeographyPaths._Root_COUNTRY.getPathInSchema());
		AdaptationTable statesTable = dataSet.getTable(GeographyPaths._Root_STATE_PROVINCE.getPathInSchema());

		
		mapping.setPlacePath(GeographyPaths._Root_ADDRESS._PLACE_ID);
		mapping.setNumberPath(GeographyPaths._Root_ADDRESS._STREET_NUMBER);
		mapping.setStreetPath(GeographyPaths._Root_ADDRESS._ADDRESS_LINE_ONE);
		mapping.setCityPath(GeographyPaths._Root_ADDRESS._CITY_LOCALITY_NAME);
		mapping.setZipcodePath(GeographyPaths._Root_ADDRESS._POSTAL_CODE);
		mapping.setCountyPath(GeographyPaths._Root_ADDRESS._COUNTY_NAME);
		mapping.setRegionPath(GeographyPaths._Root_ADDRESS._STATE_PROVINCE);
		mapping.setCountryPath(GeographyPaths._Root_ADDRESS._COUNTRY);
		mapping.setLatitudePath(GeographyPaths._Root_ADDRESS._LATITUDE);
		mapping.setLongitudePath(GeographyPaths._Root_ADDRESS._LONGITUDE);
	

		ForeignKeyMapping countryFKMapping = mapping.new ForeignKeyMapping(
			countriesTable, // table
			GeographyPaths._Root_COUNTRY._COUNTRY_CD,	          // primary key
			GeographyPaths._Root_COUNTRY._COUNTRY_NAME, 		 // label
			GeographyPaths._Root_COUNTRY._COUNTRY_CD_ALPHA_2); 	 // Google Map Reference
		mapping.setCountryFKMapping(countryFKMapping);

		ForeignKeyMapping stateFKMapping = mapping.new ForeignKeyMapping(
				statesTable, // table
				GeographyPaths._Root_STATE_PROVINCE._STATE_PROVINCE_CD,	    // primary key
				GeographyPaths._Root_STATE_PROVINCE._STATE_PROVINCE_NAME,   // label
				GeographyPaths._Root_STATE_PROVINCE._STATE_PROVINCE_CD); 	// Google Map Reference
		mapping.setRegionFKMapping(stateFKMapping);

		return mapping;
	}

	private AddressFormMapping() {}
}

package com.aramark.mdm.vndr.constraint;

import java.util.*;

import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;

public class VendorLocationOperatingCompanyConstraintOnNull implements ConstraintOnNull, Constraint
{

	private static String MESSAGE = "Operating Company is required if Vendor has Operating Companies";

	@Override
	public void checkNull(ValueContextForValidation context) throws InvalidSchemaException
	{
		// Operating Company is required if Vendor has Operating Companies
		Adaptation vendor = AdaptationUtil.followFK(
			context,
			Path.PARENT.add(VendorPaths._Root_VendorDetails_VendorLocation._Vendor));
		if (vendor == null)
		{
			return;
		}

		if (!AdaptationUtil.isLinkedRecordListEmpty(
			vendor,
			VendorPaths._Root_Vendor._VendorOperatingCompanies))
		{
			context.addError(MESSAGE);
		}
	}

	@Override
	public void checkOccurrence(Object arg0, ValueContextForValidation arg1)
		throws InvalidSchemaException
	{
		// nothing to do
	}

	@Override
	public void setup(ConstraintContext aContext)
	{
		// nothing to do

	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}

}

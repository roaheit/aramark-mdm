package com.aramark.mdm.vndr.valuefunction;

import java.util.*;

import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;

public class VendorItemMissingFromLatestUploadValueFunction implements ValueFunction
{

	@Override
	public Object getValue(Adaptation adaptation)
	{
		// return true if the vendor table upload timestamp is different that the item's upload timestamp.
		Date vendorItemUploadTimestamp = adaptation.getDate(VendorPaths._Root_VendorDetails_VendorItem._UploadTimestamp);
		Adaptation vendorAdaptation = AdaptationUtil.followFK(
			adaptation,
			VendorPaths._Root_VendorDetails_VendorItem._Vendor);
		Date vendorUploadTimestamp = vendorAdaptation.getDate(VendorPaths._Root_Vendor._UploadTimestamp);
		if (vendorItemUploadTimestamp != null && vendorUploadTimestamp != null)
		{
			return !vendorItemUploadTimestamp.equals(vendorUploadTimestamp);
		}
		return true;
	}
	@Override
	public void setup(ValueFunctionContext arg0)
	{
		// TODO Auto-generated method stub

	}

}

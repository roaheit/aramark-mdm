package com.aramark.mdm.vndr.valuefunction;

import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;

public class VendorItemHasServiceTypesValueFunction implements ValueFunction
{

	@Override
	public Object getValue(Adaptation adaptation)
	{

		return !AdaptationUtil.getLinkedRecordList(
			adaptation,
			VendorPaths._Root_VendorDetails_VendorItem._VendorItemCatalogGroup).isEmpty();

	}
	@Override
	public void setup(ValueFunctionContext arg0)
	{
		// TODO Auto-generated method stub

	}

}

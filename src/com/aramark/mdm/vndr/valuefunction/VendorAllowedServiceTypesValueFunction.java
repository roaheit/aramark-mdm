package com.aramark.mdm.vndr.valuefunction;

import java.util.*;

import com.aramark.mdm.common.path.*;
import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;

public class VendorAllowedServiceTypesValueFunction implements ValueFunction
{

	@Override
	public Object getValue(Adaptation adaptation)
	{
		@SuppressWarnings("unchecked")
		List<List<String>> allowedServiceTypeLists = (List<List<String>>) AdaptationUtil.getLinkedRecordList(
			adaptation,
			VendorPaths._Root_Vendor._VendorConcepts,
			CommonReferencePaths._Root_Concept.__allowedServiceTypeList);

		Set<String> allowedServiceTypeSet = new TreeSet<String>();
		for (List<String> allowedServiceTypeList : allowedServiceTypeLists)
		{
			allowedServiceTypeSet.addAll(allowedServiceTypeList);
		}
		return new ArrayList<String>(allowedServiceTypeSet);

	}
	@Override
	public void setup(ValueFunctionContext arg0)
	{
		// TODO Auto-generated method stub

	}

}

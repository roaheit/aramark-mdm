package com.aramark.mdm.vndr.trigger;

import java.math.*;
import java.util.*;

import com.aramark.mdm.vndr.path.*;
import com.aramark.mdm.vndr.utils.matching.*;
import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.addon.daqa.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.procedure.*;
import com.orchestranetworks.ps.trigger.*;
import com.orchestranetworks.ps.trigger.TriggerActionValidator.TriggerAction;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.ps.util.matching.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.schema.trigger.*;
import com.orchestranetworks.service.*;

public class VendorItemTableTrigger extends BaseTableTrigger
{

	@Override
	public void setup(TriggerSetupContext context)
	{
		// Enable Cascade Delete from within Workflows by default for all tables
		setEnableCascadeDelete(true);
		super.setup(context);
	}

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException
	{
		super.handleBeforeCreate(context);
		setRecordAsGolden(context.getOccurrenceContextForUpdate());
	}

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context) throws OperationException
	{
		ProcedureContext pContext = context.getProcedureContext();
		Adaptation adaptation = context.getAdaptationOccurrence();
		Date uploadTimestamp = adaptation.getDate(VendorPaths._Root_VendorDetails_VendorItem._UploadTimestamp);
		// As part of handling the SKU Mapping use Case - Mapped Items removed from CIF File, 
		// Update the Vendor table with the UploadTimestamp if it is not null
		if (uploadTimestamp != null)
		{
			updateVendorUploadTimestamp(pContext, adaptation, uploadTimestamp);
		}
		// Execute the match for the Vendor Item record and update the Match Candidates.
		// Match a new record with all of the other records in the match table.
		// Once complete the record will itself be flagged as a "golden" record.
		executeMatch(pContext, adaptation);
		super.handleAfterCreate(context);
	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context) throws OperationException
	{
		ProcedureContext pContext = context.getProcedureContext();
		Adaptation adaptation = context.getAdaptationOccurrence();
		Date uploadTimestamp = adaptation.getDate(VendorPaths._Root_VendorDetails_VendorItem._UploadTimestamp);
		// As part of handling the SKU Mapping use Case - Mapped Items removed from CIF File, 
		// Update the Vendor table with the UploadTimestamp if it is not null
		if (uploadTimestamp != null)
		{
			updateVendorUploadTimestamp(pContext, adaptation, uploadTimestamp);
		}
		super.handleAfterModify(context);

	}

	private void updateVendorUploadTimestamp(
		ProcedureContext pContext,
		Adaptation vendorItemAdaptation,
		Date uploadTimestamp) throws OperationException
	{
		Adaptation vendorAdaptation = AdaptationUtil.followFK(
			vendorItemAdaptation,
			VendorPaths._Root_VendorDetails_VendorItem._Vendor);
		HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
		pathValueMap.put(VendorPaths._Root_Vendor._UploadTimestamp, uploadTimestamp);
		ModifyValuesProcedure.execute(pContext, vendorAdaptation, pathValueMap, true, true);
	}

	private void executeMatch(ProcedureContext pContext, Adaptation adaptation)
		throws OperationException
	{
		ExecuteMatchResultForVendorItem matchVendorItem = new ExecuteMatchResultForVendorItem();
		matchVendorItem.runMatch(adaptation, pContext);

	}

	@Override
	protected TriggerActionValidator createTriggerActionValidator(TriggerAction triggerAction)
	{
		return new VendorItemTriggerActionValidator();
	}

	private class VendorItemTriggerActionValidator implements TriggerActionValidator
	{
		@Override
		public UserMessage validateTriggerAction(
			Session session,
			ValueContext valueContext,
			ValueChanges valueChanges,
			TriggerAction triggerAction) throws OperationException
		{
			if (triggerAction == TriggerAction.DELETE)
			{
				if (!((boolean) valueContext.getValue(VendorPaths._Root_VendorDetails_VendorItem._MissingFromLatestUpload) || (boolean) valueContext.getValue(VendorPaths._Root_VendorDetails_VendorItem._HasPotentialDuplicates)))
				{
					return UserMessage.createError("Vendor Items included in the latest upload file cannot be deleted unless they have potential duplicates.");
				}
			}
			return null;
		}

	}

	protected void setRecordAsGolden(ValueContextForUpdate context) throws OperationException
	{
		// Set Record to Golden to allow for future matching
		context.setValue(MatchingState.GOLDEN.getValue(), SimMatch.MATCHING_STATE);
		context.setValue(1, SimMatch.MATCHING_CLUSTERID);
		Date now = new Date();
		context.setValue(now, SimMatch.MATCHING_TIMESTAMP);
		context.setValue(BigDecimal.valueOf(100), SimMatch.MATCHING_SCORE);
	}

}

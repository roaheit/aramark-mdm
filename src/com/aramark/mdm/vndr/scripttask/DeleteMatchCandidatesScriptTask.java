package com.aramark.mdm.vndr.scripttask;

import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.procedure.*;
import com.orchestranetworks.ps.workflow.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.*;

/**
 * Delete all Match candidates where "missingFromLatestUpload=false" from the Match Candidate Table 
 * to keep things clean since these records are no longer needed once an import has been completed 
 * and all "missingFromLatestUpload=true" records have been identified.
 * 
 */
public class DeleteMatchCandidatesScriptTask extends ScriptTask
{

	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException
	{
		Repository repository = context.getRepository();
		Adaptation dataSet = WorkflowUtilities.getDataSet(
			context,
			repository,
			WorkflowConstants.PARAM_WORKING_DATA_SPACE);

		AdaptationTable matchCandidateTable = dataSet.getTable(VendorPaths._Root_VendorDetails_MatchCandidate.getPathInSchema());

		// delete all Match candidates where "missingFromLatestUpload=false" from the Match Candidate Table 
		deleteMatchCandidatesMissingFromLatestUpload(context.getSession(), matchCandidateTable);

	}

	private void deleteMatchCandidatesMissingFromLatestUpload(
		Session session,
		AdaptationTable matchCandidateTable) throws OperationException
	{
		String predicate = VendorPaths._Root_VendorDetails_MatchCandidate.__missingFromLatestUpload.format()
			+ "='false'";
		RequestResult requestResult = matchCandidateTable.createRequestResult(predicate);
		Adaptation matchCandidate = null;
		while ((matchCandidate = requestResult.nextAdaptation()) != null)
		{
			DeleteRecordProcedure.execute(matchCandidate, session, true, false, false);
		}
	}

}

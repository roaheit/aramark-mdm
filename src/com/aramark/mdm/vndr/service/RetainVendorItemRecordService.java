package com.aramark.mdm.vndr.service;

import java.io.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.aramark.mdm.vndr.path.*;
import com.aramark.mdm.vndr.utils.matching.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.procedure.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.*;

/**
 * Service to delete all the Current Record's Match Candidates, including any that had missingFromLatestUpload=false.
 *
 */

public class RetainVendorItemRecordService extends HttpServlet
{
	private ServiceContext sContext;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		sContext = ServiceContext.getServiceContext(request);
		Adaptation targetVendorItemRecord = sContext.getCurrentAdaptation();
		try
		{
			// Delete all the Current Record's Match Candidates, including any that had missingFromLatestUpload=false
			DeleteMatchCandidateRecordsProcedure deleteMatchCandidateRecordsProcedure = new DeleteMatchCandidateRecordsProcedure(
				targetVendorItemRecord,
				VendorPaths._Root_VendorDetails_VendorItem._AllMatchCandidates);

			ProcedureExecutor.executeProcedure(
				deleteMatchCandidateRecordsProcedure,
				sContext.getSession(),
				targetVendorItemRecord.getHome());
		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}

		UIServiceComponentWriter writer = sContext.getUIComponentWriter();
		String messageContent = "Service Complete.";
		String message = "alert('" + messageContent + "');";
		writer.addJS(message);
		writer.addJS_cr("window.location.href='" + sContext.getURLForEndingService() + "';");

	}

}

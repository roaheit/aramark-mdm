package com.aramark.mdm.vndr.service;

import java.io.*;
import java.util.*;

import javax.servlet.*;
import javax.servlet.http.*;

import com.aramark.mdm.vndr.path.*;
import com.aramark.mdm.vndr.utils.matching.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.procedure.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.ui.*;

/**
 * Service to copy all the Item Services from the selected match candidate record and associate them to the Current record 
 * and delete the selected match candidate's vendor item record and its Item Services. 
 * Also delete all the Current Record's Match Candidates, including any that had missingFromLatestUpload=false.
 *
 */

public class ReplacePotentialDuplicateRecordService extends HttpServlet
{
	private ServiceContext sContext;
	private String redirection;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
		throws ServletException, IOException
	{
		sContext = ServiceContext.getServiceContext(request);
		Adaptation potentialDuplicateCandidateRecord = sContext.getCurrentAdaptation();

		Adaptation candidateVendorItemRecord = AdaptationUtil.followFK(
			potentialDuplicateCandidateRecord,
			VendorPaths._Root_VendorDetails_MatchCandidate._CandidateVendorItem);

		Adaptation targetVendorItemRecord = AdaptationUtil.followFK(
			potentialDuplicateCandidateRecord,
			VendorPaths._Root_VendorDetails_MatchCandidate._VendorItem);

		redirection = sContext.getURLForSelection(targetVendorItemRecord);

		try
		{
			// Copy all the Item Services from the selected match candidate and associate them to the Current record
			ReplacePotentialDuplicateRecordProcedure potentialDuplicateRecordProcedure = new ReplacePotentialDuplicateRecordProcedure(
				candidateVendorItemRecord,
				targetVendorItemRecord);
			ProcedureExecutor.executeProcedure(
				potentialDuplicateRecordProcedure,
				sContext.getSession(),
				targetVendorItemRecord.getHome());

			// Delete the selected match candidate's vendor item record and its Item Services
			DeleteCandidateVendorItemRecordProcedure deleteCandidateVendorItemRecordProcedure = new DeleteCandidateVendorItemRecordProcedure(
				candidateVendorItemRecord);
			ProcedureExecutor.executeProcedure(
				deleteCandidateVendorItemRecordProcedure,
				sContext.getSession(),
				targetVendorItemRecord.getHome());

			// Delete all the Current Record's Match Candidates, including any that had missingFromLatestUpload=false
			DeleteMatchCandidateRecordsProcedure deleteMatchCandidateRecordsProcedure = new DeleteMatchCandidateRecordsProcedure(
				targetVendorItemRecord,
				VendorPaths._Root_VendorDetails_VendorItem._AllMatchCandidates);
			ProcedureExecutor.executeProcedure(
				deleteMatchCandidateRecordsProcedure,
				sContext.getSession(),
				targetVendorItemRecord.getHome());

		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}

		UIServiceComponentWriter writer = sContext.getUIComponentWriter();
		String messageContent = "Service Complete.";
		String message = "alert('" + messageContent + "');";
		writer.addJS(message);
		writer.addJS_cr("parent.document.location.href='" + redirection + "';");

	}

	public String getRedirection()
	{
		return redirection;
	}

	public class ReplacePotentialDuplicateRecordProcedure implements Procedure
	{

		private Adaptation candidateVendorItemRecord;
		private Adaptation targetVendorItemRecord;

		public ReplacePotentialDuplicateRecordProcedure(
			Adaptation candidateVendorItemRecord,
			Adaptation targetVendorItemRecord)
		{
			this.candidateVendorItemRecord = candidateVendorItemRecord;
			this.targetVendorItemRecord = targetVendorItemRecord;
		}

		@Override
		public void execute(ProcedureContext pctx) throws Exception
		{
			List<Adaptation> vendorItemServiceTypes = AdaptationUtil.getLinkedRecordList(
				candidateVendorItemRecord,
				VendorPaths._Root_VendorDetails_VendorItem._VendorItemCatalogGroup);

			if (!vendorItemServiceTypes.isEmpty())
			{
				for (Adaptation vendorItemServiceType : vendorItemServiceTypes)
				{
					HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
					pathValueMap.put(
						VendorPaths._Root_VendorDetails_VendorItemCatalogGroup._VendorItem,
						targetVendorItemRecord.getOccurrencePrimaryKey().format());
					pathValueMap.put(
						VendorPaths._Root_VendorDetails_VendorItemCatalogGroup._CatalogGroup,
						vendorItemServiceType.getString(VendorPaths._Root_VendorDetails_VendorItemCatalogGroup._CatalogGroup));
					CreateRecordProcedure.execute(
						pctx,
						vendorItemServiceType.getContainerTable(),
						pathValueMap,
						true,
						true);
				}
			}

		}
	}

	public class DeleteCandidateVendorItemRecordProcedure implements Procedure
	{

		private Adaptation vendorItem;

		public DeleteCandidateVendorItemRecordProcedure(Adaptation vendorItem)
		{

			this.vendorItem = vendorItem;
		}

		@Override
		public void execute(ProcedureContext pctx) throws Exception
		{
			DeleteRecordProcedure.execute(pctx, vendorItem, true, false, false);
		}
	}

}

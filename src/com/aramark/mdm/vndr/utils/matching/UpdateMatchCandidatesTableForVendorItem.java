package com.aramark.mdm.vndr.utils.matching;

import java.util.*;

import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.procedure.*;
import com.orchestranetworks.ps.util.matching.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;

public class UpdateMatchCandidatesTableForVendorItem implements Procedure
{

	private List<SimMatchCandidate> candidates;
	private AdaptationTable matchCandidatesTable;
	private String pivotRecordPk;

	@Override
	public void execute(ProcedureContext pctx) throws Exception
	{

		for (SimMatchCandidate candidate : candidates)
		{
			HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
			pathValueMap.put(
				VendorPaths._Root_VendorDetails_MatchCandidate._VendorItem,
				pivotRecordPk);
			pathValueMap.put(
				VendorPaths._Root_VendorDetails_MatchCandidate._CandidateVendorItem,
				candidate.getPrimaryKey().format());
			CreateRecordProcedure.execute(pctx, matchCandidatesTable, pathValueMap, true, true);
		}
	}

	public void setMatchCandidatesTable(AdaptationTable matchCandidatesTable)
	{
		this.matchCandidatesTable = matchCandidatesTable;
	}

	public void setPivotRecordPk(String pivotRecordPk)
	{
		this.pivotRecordPk = pivotRecordPk;
	}

	public void setCandidates(List<SimMatchCandidate> candidates)
	{
		this.candidates = candidates;
	}

}

package com.aramark.mdm.vndr.utils.matching;

import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.ps.util.matching.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;

public class ExecuteMatchResultForVendorItem
{

	public void runMatch(Adaptation vendorItem, ProcedureContext pContext)
		throws OperationException
	{
		try
		{
			// Execute Simulate Match
			SimMatch match = new SimMatch();
			match.addPivotRecord(vendorItem);
			match.execute(pContext);

			// Clear all records for this Vendor Item from the match results table
			DeleteMatchCandidateRecords deleteMatchCandidateRecords = new DeleteMatchCandidateRecords(
				vendorItem,
				VendorPaths._Root_VendorDetails_VendorItem._AllMatchCandidates);
			deleteMatchCandidateRecords.execute(pContext);
			// Update the matching results table with the candidate matches
			UpdateMatchCandidatesTableForVendorItem updateMatchCandidatesTableForVendorItem = new UpdateMatchCandidatesTableForVendorItem();
			updateMatchCandidatesTableForVendorItem.setCandidates(match.getCandidates());
			updateMatchCandidatesTableForVendorItem.setMatchCandidatesTable(vendorItem.getContainer()
				.getTable(VendorPaths._Root_VendorDetails_MatchCandidate.getPathInSchema()));
			updateMatchCandidatesTableForVendorItem.setPivotRecordPk(vendorItem.getOccurrencePrimaryKey()
				.format());
			updateMatchCandidatesTableForVendorItem.execute(pContext);

			// Clear the pivot list.
			match.clear();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public class DeleteMatchCandidateRecords implements Procedure
	{

		private Adaptation vendorItem;
		private Path associationField;

		public DeleteMatchCandidateRecords(Adaptation vendorItem, Path association)
		{

			this.vendorItem = vendorItem;
			this.associationField = association;
		}

		@Override
		public void execute(ProcedureContext pctx) throws Exception
		{
			AdaptationUtil.deleteLinkedRecordList(vendorItem, associationField, pctx);
		}
	}

}

package com.aramark.mdm.vndr.utils.matching;

import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;

public class DeleteMatchCandidateRecordsProcedure implements Procedure
{

	private Adaptation vendorItem;
	private Path associationField;

	public DeleteMatchCandidateRecordsProcedure(Adaptation vendorItem, Path association)
	{

		this.vendorItem = vendorItem;
		this.associationField = association;
	}

	@Override
	public void execute(ProcedureContext pctx) throws Exception
	{
		AdaptationUtil.deleteLinkedRecordList(vendorItem, associationField, pctx);
	}
}

package com.aramark.mdm.vndr.tablereffilter;

import java.util.*;

import com.aramark.mdm.locn.path.*;
import com.aramark.mdm.vndr.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;

public class VendorLocationLocationTableRefFilter implements TableRefFilter
{

	private static final String MESSAGE = "At least 1 of the Location's concepts must exist in the Vendor's list of concepts.";

	@Override
	public boolean accept(Adaptation adaptation, ValueContext context)
	{
		// There must be at least 1 common concept defined for the Vendor and the Location
		Adaptation vendor = AdaptationUtil.followFK(
			context,
			Path.PARENT.add(VendorPaths._Root_VendorDetails_VendorLocation._Vendor));
		if (vendor == null)
		{
			return false;
		}
		List<String> vendorConceptKeyList = AdaptationUtil.getLinkedRecordKeyList(
			vendor,
			VendorPaths._Root_Vendor._VendorConcepts);
		@SuppressWarnings("unchecked")
		List<String> locationConceptKeyList = (List<String>) AdaptationUtil.getLinkedRecordList(
			adaptation,
			LocationPaths._Root_Location._LocationConcepts,
			LocationPaths._Root_LocationDetails_LocationConcept._Concept);

		return !CollectionUtils.intersection(vendorConceptKeyList, locationConceptKeyList)
			.isEmpty();
	}
	@Override
	public void setup(TableRefFilterContext arg0)
	{

	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1) throws InvalidSchemaException
	{
		return MESSAGE;
	}

}

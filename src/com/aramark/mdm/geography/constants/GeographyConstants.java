package com.aramark.mdm.geography.constants;

import com.aramark.mdm.geography.GeographyPaths._Root_ADDRESS;
import com.orchestranetworks.schema.Path;

public class GeographyConstants {

	public static final Path[]  GEOGRAPHY_ADDRESS_READONLY_FIELDS = 
		{
			_Root_ADDRESS._STREET_NUMBER,
			_Root_ADDRESS._PLACE_ID,
			_Root_ADDRESS._ADDRESS_LINE_ONE,
			_Root_ADDRESS._CITY_LOCALITY_NAME,
			_Root_ADDRESS._STATE_PROVINCE,
			_Root_ADDRESS._POSTAL_CODE,
			_Root_ADDRESS._COUNTY_NAME,
			_Root_ADDRESS._COUNTRY,
			_Root_ADDRESS._LATITUDE,
			_Root_ADDRESS._LONGITUDE,
			_Root_ADDRESS._IS_VALIDATED
				
		};
	

}

package com.aramark.mdm.geography.trigger;

import org.apache.log4j.Logger;

import com.aramark.mdm.geography.GeographyPaths;
import com.aramark.mdm.ui.form.address.AddressMainPane;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;
public class AddressTableTrigger extends BaseTableTrigger {

	final static Logger logger = Logger.getLogger(AddressTableTrigger.class);


	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {
		 ValueContextForUpdate vcu = context.getOccurrenceContextForUpdate();
		 // set validated to true
		 
		 logger.debug(("_IS_VALIDATED " + vcu.getValue(GeographyPaths._Root_ADDRESS._IS_VALIDATED)));
//		 if ((vcu.getValue(GeographyPaths._Root_ADDRESS._LATITUDE) != null) && (vcu.getValue(GeographyPaths._Root_ADDRESS._LONGITUDE) != null))
//		 {
//			 vcu.setValue("Y", GeographyPaths._Root_ADDRESS._IS_VALIDATED);
//		 }
				
	
		super.handleBeforeCreate(context);
	}
	
	
	@Override
	public void setup(TriggerSetupContext context) {
		super.setup(context);		
	}


	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException {
		
		 ValueContextForUpdate vcu = context.getOccurrenceContextForUpdate();
		 if ((vcu.getValue(GeographyPaths._Root_ADDRESS._LATITUDE) != null) && (vcu.getValue(GeographyPaths._Root_ADDRESS._LONGITUDE) != null))
		 {
			 vcu.setValue("Y", GeographyPaths._Root_ADDRESS._IS_VALIDATED);
		 }

		super.handleBeforeModify(context);
	}
	
}

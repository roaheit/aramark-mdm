package com.aramark.mdm.geography.schemaextension;

import com.aramark.mdm.geography.GeographyPaths;
import com.aramark.mdm.geography.accessrule.AddressFieldsReadOnlyAccessRule;
import com.aramark.mdm.geography.constants.GeographyConstants;
import com.orchestranetworks.ps.accessrule.AccessRulesManager;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.SchemaExtensions;
import com.orchestranetworks.schema.SchemaExtensionsContext;

public class GeographySchemaExtension implements SchemaExtensions
{

	@Override
	public void defineExtensions(SchemaExtensionsContext context)
	{
		AccessRulesManager manager = new AccessRulesManager(context);
		manager.setAccessRuleOnNodeAndAllDescendants(
			GeographyPaths._Root_ADDRESS.getPathInSchema(),
			false,
			new AddressFieldsReadOnlyAccessRule());

   
		for ( Path path : GeographyConstants.GEOGRAPHY_ADDRESS_READONLY_FIELDS)
			manager.setAccessRuleOnNode(GeographyPaths._Root_ADDRESS.getPathInSchema().add(path), new AddressFieldsReadOnlyAccessRule());

		
		
		
	}
}

/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.aramark.mdm.geography.accessrule;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Session;

/**
 */
public class AddressFieldsReadOnlyAccessRule implements AccessRule
{

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node)
	{

		if (adaptation.isSchemaInstance() || adaptation.isHistory())
		{
			return AccessPermission.getReadWrite();
		}

		return AccessPermission.getReadOnly();
	}
}

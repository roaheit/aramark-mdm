package com.aramark.mdm.common.enums;

public class SubLocationTypes
{
	public static final String CONSUMER_OUTLET = "1";
	public static final String PRODUCTION_AREAS = "2";
	public static final String STORAGE_AREAS = "3";
}

package com.aramark.mdm.common.enums;

public class OrgTrackingInfo
{
	public static final String ORGN_DISTRICT_DATAENTRY = "ORGN-District-DataEntry";
	public static final String ORGN_PROFIT_CENTER_DATAENTRY = "ORGN-Profit Center-DataEntry";

}

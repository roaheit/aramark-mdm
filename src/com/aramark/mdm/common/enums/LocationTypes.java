package com.aramark.mdm.common.enums;

public class LocationTypes {
	public static final String ARAMARK_OPERATED_SERVICES = "ARMK";
	public static final String NON_ARAMARK_OPERATED = "NAMRK";
}

package com.aramark.mdm.common.enums;

public class OrgHierarchyTypeCodes
{
	public static final String SITE = "STE";
	public static final String STANDARD_FINANCIAL = "FIN";
	public static final String GPO = "GPO";
	public static final String CLIENT_COMBINED = "CCB";
	public static final String CLIENT_FAMILY = "CFY";
	public static final String REPORTING_COMBINES = "RCB";
}

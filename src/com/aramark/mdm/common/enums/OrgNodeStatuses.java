package com.aramark.mdm.common.enums;

public class OrgNodeStatuses {

	public static final String SITE_ACTIVE = "19";
	public static final String LOCATION_ACTIVE = "21";
	public static final String PROFIT_CENTER_ACTIVE = "1";
	public static final String SUB_LOCATION_ACTIVE = "27";
	public static final String DISTRICT_ACTIVE = "6";

}

package com.aramark.mdm.common.enums;

public class OrgRoles
{
	// roles for managing access to the tabs for Districts
	public static final String ORGN_DST_NODE_C = "_p_orgn_dst_node_c";
	public static final String ORGN_DST_NODE_U = "_p_orgn_dst_node_u";
	public static final String ORGN_DST_NODE_R = "_p_orgn_dst_node_r";
	public static final String ORGN_DST_HIER_U = "_p_orgn_dst_hier_u";
	public static final String ORGN_DST_HIER_R = "_p_orgn_dst_hier_r";
	public static final String ORGN_DST_ADDR_U = "_p_orgn_dst_addr_u";
	public static final String ORGN_DST_ADDR_R = "_p_orgn_dst_addr_r";
	public static final String ORGN_DST_RESP_U = "_p_orgn_dst_resp_u";
	public static final String ORGN_DST_RESP_R = "_p_orgn_dst_resp_r";

	// roles for managing access to the tabs for Profit Centers

	public static final String ORGN_PC_NODE_U = "_p_orgn_pc_node_u";
	public static final String ORGN_PC_NODE_R = "_p_orgn_pc_node_r";
	public static final String ORGN_PC_HIER_U = "_p_orgn_pc_hier_u";
	public static final String ORGN_PC_HIER_R = "_p_orgn_pc_hier_r";
	public static final String ORGN_PC_ATTR_U = "_p_orgn_pc_atr_u";
	public static final String ORGN_PC_ATTR_R = "_p_orgn_pc_atr_r";
	public static final String ORGN_PC_ATTR_ACCT_U = "_p_orgn_pc_atr_acct_u";
	public static final String ORGN_PC_ATTR_ACCT_R = "_p_orgn_pc_atr_acct_r";
	public static final String ORGN_PC_ATTR_PAY_U = "_p_orgn_pc_atr_pay_u";
	public static final String ORGN_PC_ATTR_PAY_R = "_p_orgn_pc_atr_pay_r";
	public static final String ORGN_PC_ATTR_FIN_U = "_p_orgn_pc_atr_fin_u";
	public static final String ORGN_PC_ATTR_FIN_R = "_p_orgn_pc_atr_fin_r";
	public static final String ORGN_PC_ATTR_RATE_U = "_p_orgn_pc_atr_rate_u";
	public static final String ORGN_PC_ATTR_RATE_R = "_p_orgn_pc_atr_rate_r";
	public static final String ORGN_PC_ATTR_MKT_U = "_p_orgn_pc_atr_mkt_u";
	public static final String ORGN_PC_ATTR_MKT_R = "_p_orgn_pc_atr_mkt_r";
	public static final String ORGN_PC_ATTR_OTHR_U = "_p_orgn_pc_atr_othr_u";
	public static final String ORGN_PC_ATTR_OTHR_R = "_p_orgn_pc_atr_othr_r";
	public static final String ORGN_PC_ADDR_U = "_p_orgn_pc_addr_u";
	public static final String ORGN_PC_ADDR_R = "_p_orgn_pc_addr_r";
	public static final String ORGN_PC_RESP_U = "_p_orgn_pc_resp_u";
	public static final String ORGN_PC_RESP_R = "_p_orgn_pc_resp_r";
	public static final String ORGN_PC_COA_U = "_p_orgn_pc_coa_u";
	public static final String ORGN_PC_COA_R = "_p_orgn_pc_coa_r";
}

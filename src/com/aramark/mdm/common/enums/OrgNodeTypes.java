package com.aramark.mdm.common.enums;

import java.util.Arrays;
import java.util.List;

public class OrgNodeTypes {
	public static final String PROFIT_CENTER = "1";
	public static final String SITE = "8";
	public static final String LOCATION = "9";
	public static final String GPO_CUSTOMER = "32";
	public static final String SUB_LOCATION = "10";
	public static final String DISTRICT = "2";

	public static final List<String> GLOBAL_HIERARCHY_ORG_NODE_TYPE_LIST = Arrays
		.asList(new String[] { PROFIT_CENTER, DISTRICT, LOCATION, SUB_LOCATION });

	public static boolean isGlobalHierarchyOrgNodeType(String orgNodeType)
	{
		return GLOBAL_HIERARCHY_ORG_NODE_TYPE_LIST.contains(orgNodeType);
	}
}

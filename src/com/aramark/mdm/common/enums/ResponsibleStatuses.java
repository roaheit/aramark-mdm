package com.aramark.mdm.common.enums;

public class ResponsibleStatuses
{

	public static final String ACTIVE = "1";
	public static final String INACTIVE = "2";

}

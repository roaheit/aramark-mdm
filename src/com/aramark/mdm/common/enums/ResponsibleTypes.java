package com.aramark.mdm.common.enums;

public class ResponsibleTypes
{
	public static final String MANAGER = "1";
	public static final String ASSISTANT_MANAGER = "2";
}

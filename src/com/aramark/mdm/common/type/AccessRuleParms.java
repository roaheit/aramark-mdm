package com.aramark.mdm.common.type;

import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.Role;

public class AccessRuleParms
{

	private Role roleRequiredForUpdate = null;
	private Role roleRequiredForRead = null;
	private String forOrgNodeType = null;
	private Path nodeTypePath = null;
	private String trackingInfo = null;
	private String stringOfParms = null;

	public AccessRuleParms()
	{
    }
	
	public AccessRuleParms(
		String stringOfParms)
	{
		this.stringOfParms = stringOfParms;
		String[] rolesNodeType = new String[5];
		rolesNodeType = this.stringOfParms.split(",");
		this.setForOrgNodeType(rolesNodeType[0]);
		this.setNodeTypePath(Path.parse(rolesNodeType[1]));
		this.setRoleRequiredForUpdate(Role.forSpecificRole(rolesNodeType[2]));
		this.setRoleRequiredForRead(Role.forSpecificRole(rolesNodeType[3]));
		this.setTrackingInfo(rolesNodeType[4]);
	}

	public void setRoleRequiredForUpdate(Role roleRequiredForUpdate)
	{
		this.roleRequiredForUpdate = roleRequiredForUpdate;
	}

	public Role getRoleRequiredForUpdate()
	{
		return roleRequiredForUpdate;
	}

	public void setRoleRequiredForRead(Role roleRequiredForRead)
	{
		this.roleRequiredForRead = roleRequiredForRead;
	}

	public Role getRoleRequiredForRead()
	{
		return roleRequiredForRead;
	}

	public void setForOrgNodeType(String forOrgNodeType)
	{
		this.forOrgNodeType = forOrgNodeType;
	}

	public String getForOrgNodeType()
	{
		return forOrgNodeType;
	}

	public void setNodeTypePath(Path nodeTypePath)
	{
		this.nodeTypePath = nodeTypePath;
	}

	public Path getNodeTypePath()
	{
		return nodeTypePath;
	}


	public String getTrackingInfo()
	{
		return trackingInfo;
	}

	public void setTrackingInfo(String trackingInfo)
	{
		this.trackingInfo = trackingInfo;
	}

}

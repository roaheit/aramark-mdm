package com.aramark.mdm.common.type;

public class AramarkMDMException extends RuntimeException{

	private static final long serialVersionUID = -3493359832783334897L;

    public AramarkMDMException(String message) {
        super(message);
    }
	
}

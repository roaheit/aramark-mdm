package com.aramark.mdm.common.scheduledtask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/** import com.orchestranetworks.service.OperationException;**/
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.workflow.WorkflowConstants;
import com.orchestranetworks.scheduler.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.*; 


public class InitiateImportWorkflow extends ScheduledTask {
	
	private String workflow;
		
	@Override
	public void execute(ScheduledExecutionContext context) throws OperationException, ScheduledTaskInterruption {
		
		Repository repository = Repository.getDefault();
		try
		{
			WorkflowEngine wfEngine = WorkflowEngine.getFromRepository(
				repository,
				context.getSession());
			ProcessLauncher launcher = wfEngine.getProcessLauncher(PublishedProcessKey.forName(workflow));
			launcher.setLabel(UserMessage.createInfo("Import Archive"));
			launcher.launchProcess();
		}
		catch (OperationException e)
		{
			e.printStackTrace();
		}

	}	

	public final String getWorkflow()
	{
		return this.workflow;
	}

	public final void setWorkflow(String workflow)
	{
		this.workflow = workflow;
	}

}

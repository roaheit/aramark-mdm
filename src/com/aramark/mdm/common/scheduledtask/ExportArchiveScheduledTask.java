package com.aramark.mdm.common.scheduledtask;

/** import com.orchestranetworks.service.OperationException;**/
import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.scheduler.*;
import com.orchestranetworks.service.*;

public class ExportArchiveScheduledTask extends ScheduledTask {

	private String dataSpaceName; /* = "LocationMasterDataSpace"; */
	private String dataSetName;   /*  = "LocationDataSet";        */ 
	private String envirName;   /*  = "LocationDataSet";        */ 		
	
	@Override
	public void execute(ScheduledExecutionContext context) throws OperationException, ScheduledTaskInterruption {
		
			
		Repository repo = Repository.getDefault();
		AdaptationHome dataSpace = repo.lookupHome(HomeKey.forBranchName(dataSpaceName));
		if (dataSpace == null)
		{
			throw OperationException.createError("Data space " + dataSpace + " does not exist.");
		}
		Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName(dataSetName));
		if (dataSet == null)
		{
			throw OperationException.createError("Data set " + dataSetName
				+ " does not exist in the dataspace ." + dataSpaceName);
		}

		final ArchiveExportSpec archiveExportSpec = new ArchiveExportSpec();
		/* archiveExportSpec.setArchive(Archive.forFileInDefaultDirectory("LocationDataArchive.ebx")); */
		/*  export file name will be the dataspace name.  It is intended to be a statically named file.  */
		
		String fileName = dataSpaceName + envirName + ".ebx"; 
		
		/* archiveExportSpec.setArchive(Archive.forFileInDefaultDirectory(dataSpaceName)); */
		archiveExportSpec.setArchive(Archive.forFileInDefaultDirectory(fileName));
		archiveExportSpec.addInstance(AdaptationName.forName(dataSetName), true);
		Session session = repo.createSessionFromLoginPassword("Svc_MDG", "KaleAndCollards"); 	
		
		
		ProgrammaticService.createForSession(session, dataSpace).execute(
			new Procedure()
			{
				@Override
				public void execute(final ProcedureContext procedureContext) throws Exception
				{
					procedureContext.doExportArchive(archiveExportSpec);
				}
			});

	}
	
	public String getDataSpaceName()
	{
		return this.dataSpaceName;
	}

	public void setDataSpaceName(String dataSpaceName)
	{
		this.dataSpaceName = dataSpaceName;
	}

	public String getDataSetName()
	{
		return this.dataSetName;
	}

	public void setDataSetName(String dataSetName)
	{
		this.dataSetName = dataSetName;
	}

	public String getEnvirName()
	{
		return this.envirName;
	}

	public void setEnvirName(String envirName)
	{
		this.envirName = envirName;
	}
}

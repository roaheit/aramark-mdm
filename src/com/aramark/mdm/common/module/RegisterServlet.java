package com.aramark.mdm.common.module;

import com.orchestranetworks.ps.module.*;

/**
 */
@SuppressWarnings("serial")
public class RegisterServlet extends PSRegisterServlet
{

	public RegisterServlet()
	{
		super("aramark-mdm");
	}

}
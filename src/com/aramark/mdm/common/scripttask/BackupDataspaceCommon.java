package com.aramark.mdm.common.scripttask;
import com.aramark.mdm.config.EBXAramarkEnvironment;
import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.*;

public class BackupDataspaceCommon extends ScriptTask
{

		@Override
		public void executeScript(ScriptTaskContext context) throws OperationException
		{
			final String dataSpaceName = "CommonReferenceMasterDataSpace"; 
			final String dataSetName = "CommonReferenceDataSet";        
			
			String serviceUser = EBXAramarkEnvironment.exbServiceUser;
			String servicePassword = EBXAramarkEnvironment.exbServicePassword;
		
			
			Repository repository = context.getRepository();
	
			AdaptationHome dataSpace = repository.lookupHome(HomeKey.forBranchName(dataSpaceName));
			if (dataSpace == null)
			{
				throw OperationException.createError("Data space " + dataSpace + " does not exist.");
			}
			Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName(dataSetName));
			if (dataSet == null)
			{
				throw OperationException.createError("Data set " + dataSetName
					+ " does not exist in the dataspace ." + dataSpaceName);
			}  
			
			final ArchiveExportSpec archiveExportSpec = new ArchiveExportSpec();
			/*  export file name will be the dataspace name.  It is intended to be a statically named file.  */
			
			String fileName = dataSpaceName + "_Backup.ebx"; 
			archiveExportSpec.setArchive(Archive.forFileInDefaultDirectory(fileName));
			archiveExportSpec.addInstance(AdaptationName.forName(dataSetName), true);
			Session session = repository.createSessionFromLoginPassword(serviceUser,servicePassword); 	
			
			ProgrammaticService.createForSession(session, dataSpace).execute(
				new Procedure()
				{
					@Override
					public void execute(final ProcedureContext procedureContext) throws Exception
					{
						procedureContext.doExportArchive(archiveExportSpec);
					}
				});

		}	

	}
	
	


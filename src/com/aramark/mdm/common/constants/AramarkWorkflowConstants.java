package com.aramark.mdm.common.constants;

import com.orchestranetworks.ps.workflow.*;

public class AramarkWorkflowConstants 
{
	public static final String WF_KEY_USER_MAINTAIN_MASTER_DATA = "USER_MaintainMasterData";
	public static final String WF_KEY_ORGN_MAINTAIN_MASTER_DATA = "ORGN_MaintainMasterData";
	public static final String PARAM_ORG_NODE_TYPE = "orgNodeType";
	public static final String PARAM_ORG_NODE_TYPE_CODE = "orgNodeTypeCode";
	public static final String WF_KEY_CREATE_OR_UPDATE_ORG_NODE = "ORGN_MaintainMasterData";
	public static final String WF_KEY_CREATE_OR_UPDATE = "ORGN-MaintainMasterData";
}

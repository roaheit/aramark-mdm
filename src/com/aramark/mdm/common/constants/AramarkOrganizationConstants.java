package com.aramark.mdm.common.constants;

import com.aramark.mdm.common.enums.OrgNodeTypes;
import com.aramark.mdm.common.enums.OrgRoles;
import com.aramark.mdm.common.enums.OrgTrackingInfo;
import com.aramark.mdm.orgn.path.OrganizationPaths;

public class AramarkOrganizationConstants {
	public static final String WF_KEY_USER_MAINTAIN_MASTER_DATA = "USER_MaintainMasterData";
	public static final String WF_KEY_ORGN_MAINTAIN_MASTER_DATA = "ORGN_MaintainMasterData";
	public static final String PARAM_ORG_NODE_TYPE = "orgNodeType";
	public static final String WF_KEY_CREATE_OR_UPDATE_ORG_NODE = "ORGN_MaintainMasterData";
	public static final Object PROFIT_CENTER_IN_SITE_HIER = "STE|1";
	public static final Object SITE_IN_SITE_HIER = "STE|8";
	public static final Object LOCATION_IN_SITE_HIER = "STE|9";
	public static final Object SUB_LOCATION_IN_SITE_HIER = "STE|10";
	public static final Object DISTRICT_IN_FINANCIAL_HIER = "FIN|2";
	public static final String MTRL_MASTER_DATA_SP = "MaterialMasterDataSpace";
	public static final String MTRL_MASTER_REF_DATA = "MaterialReferenceDataSet";
	public static final String DELIMITER = ",";
	//   org node type id, path of the org node type, role name for update, role name for read only, wf tracking information 

	public static final String DISTRICT_NODE_ROLES = OrgNodeTypes.DISTRICT + DELIMITER
		+ OrganizationPaths._Root_Organization_OrgNode._OrgNodeType.format()
		+ DELIMITER + OrgRoles.ORGN_DST_NODE_U + DELIMITER + OrgRoles.ORGN_DST_NODE_R + DELIMITER
		+ OrgTrackingInfo.ORGN_DISTRICT_DATAENTRY;

	public static final String DISTRICT_HIER_ROLES = OrgNodeTypes.DISTRICT + DELIMITER
		+ OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType.format()
		+ DELIMITER + OrgRoles.ORGN_DST_HIER_U + DELIMITER + OrgRoles.ORGN_DST_HIER_R + DELIMITER
		+ OrgTrackingInfo.ORGN_DISTRICT_DATAENTRY;
	
	public static final String DISTRICT_ADDR_ROLES = OrgNodeTypes.DISTRICT + DELIMITER
		+ OrganizationPaths._Root_Organization_Node_NodeAddress.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_DST_ADDR_U + DELIMITER + OrgRoles.ORGN_DST_ADDR_R + DELIMITER
		+ OrgTrackingInfo.ORGN_DISTRICT_DATAENTRY;
	
	public static final String DISTRICT_RESP_ROLES = OrgNodeTypes.DISTRICT + DELIMITER
		+ OrganizationPaths._Root_Organization_Node_NodeResponsible.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_DST_RESP_U + DELIMITER + OrgRoles.ORGN_DST_RESP_R + DELIMITER
		+ OrgTrackingInfo.ORGN_DISTRICT_DATAENTRY;

	// org node type id, path of the org node type, role name for update, role name for read only, wf tracking information 

	// public static final String PC_NODE_ROLES = "1,./ORG_NODE_TYPE_ID,_p_orgn_pc_node_u,_p_orgn_pc_node_r,ORGN-Profit Center-DataEntry";
	public static final String PC_NODE_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_OrgNode._OrgNodeType.format()
		+ DELIMITER + OrgRoles.ORGN_PC_NODE_U + DELIMITER + OrgRoles.ORGN_PC_NODE_R + DELIMITER
		+ OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_HIER_ROLES = "1,./ORG_NODE_TYPE_ID,_p_orgn_pc_hier_u,_p_orgn_pc_hier_r,ORGN-Profit Center-DataEntry";
	public static final String PC_HIER_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType.format()
		+ DELIMITER + OrgRoles.ORGN_PC_HIER_U + DELIMITER + OrgRoles.ORGN_PC_HIER_R + DELIMITER
		+ OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_ATTR_ROLES = "1,./_orgNodeTypeId,_p_orgn_pc_atr_u,_p_orgn_pc_atr_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ATTR_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ATTR_U + DELIMITER + OrgRoles.ORGN_PC_ATTR_R + DELIMITER
		+ OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_ATTR_ACCT_ROLES = "1,./_orgNodeTypeId,_p_orgn_pc_atr_acct_u,_p_orgn_pc_atr_acct_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ATTR_ACCT_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ATTR_ACCT_U + DELIMITER + OrgRoles.ORGN_PC_ATTR_ACCT_R
		+ DELIMITER + OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_ATTR_PAY_ROLES = "1,./_orgNodeTypeId,_p_orgn_pc_atr_pay_u,_p_orgn_pc_atr_pay_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ATTR_PAY_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ATTR_PAY_U + DELIMITER + OrgRoles.ORGN_PC_ATTR_PAY_R
		+ DELIMITER + OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_ATTR_FIN_ROLES = "1,./_orgNodeTypeId,_p_orgn_pc_atr_fin_u,_p_orgn_pc_atr_fin_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ATTR_FIN_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ATTR_FIN_U + DELIMITER + OrgRoles.ORGN_PC_ATTR_FIN_R
		+ DELIMITER + OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	//public static final String PC_ATTR_RATE_ROLES = "1,./_orgNodeTypeId,_p_orgn_pc_atr_rate_u,_p_orgn_pc_atr_rate_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ATTR_RATE_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ATTR_RATE_U + DELIMITER + OrgRoles.ORGN_PC_ATTR_RATE_R
		+ DELIMITER + OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_ATTR_MKT_ROLES = "1,./_orgNodeTypeId,_p_orgn_pc_atr_mkt_u,_p_orgn_pc_atr_mkt_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ATTR_MKT_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ATTR_MKT_U + DELIMITER + OrgRoles.ORGN_PC_ATTR_MKT_R
		+ DELIMITER + OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_ATTR_OTHR_ROLES = "1,./_orgNodeTypeId,_p_orgn_pc_atr_othr_u,_p_orgn_pc_atr_othr_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ATTR_OTHR_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ATTR_OTHR_U + DELIMITER + OrgRoles.ORGN_PC_ATTR_OTHR_R
		+ DELIMITER + OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_ADDR_ROLES = "1,./_ORG_NODE_TYPE_ID,_p_orgn_pc_addr_u,_p_orgn_pc_addr_r,ORGN-Profit Center-DataEntry";
	public static final String PC_ADDR_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_Node_NodeAddress.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_ADDR_U + DELIMITER + OrgRoles.ORGN_PC_ADDR_R + DELIMITER
		+ OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	// public static final String PC_RESP_ROLES = "1,./_ORG_NODE_TYPE_ID,_p_orgn_pc_resp_u,_p_orgn_pc_resp_r,ORGN-Profit Center-DataEntry";
	public static final String PC_RESP_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_Node_NodeResponsible.__orgNodeTypeID.format()
		+ DELIMITER + OrgRoles.ORGN_PC_RESP_U + DELIMITER + OrgRoles.ORGN_PC_RESP_R + DELIMITER
		+ OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;
	//public static final String PC_COA_ROLES = "1,./_ORG_NODE_TYPE_ID,_p_orgn_pc_coa_u,_p_orgn_pc_coa_r,ORGN-Profit Center-DataEntry";
	public static final String PC_COA_ROLES = OrgNodeTypes.PROFIT_CENTER + DELIMITER
		+ OrganizationPaths._Root_Organization_OrgChart.__orgNodeTypeID.format() + DELIMITER
		+ OrgRoles.ORGN_PC_COA_U + DELIMITER + OrgRoles.ORGN_PC_COA_R + DELIMITER
		+ OrgTrackingInfo.ORGN_PROFIT_CENTER_DATAENTRY;


}

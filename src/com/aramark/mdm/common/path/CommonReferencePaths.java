package com.aramark.mdm.common.path;

import com.orchestranetworks.schema.*;
/**
 * Generated by EBX5 5.8.1 fix D [1067:0009], at  2018/02/15 10:07:23 [EST].
 * WARNING: Any manual changes to this class may be overwritten by generation process.
 * DO NOT MODIFY THIS CLASS.
 * 
 * This interface defines constants related to schema [Module: aramark-mdm, path: /WEB-INF/ebx/schemas/CommonReferenceDataModel.xsd].
 * 
 * Root paths in this interface: 
 * 	'/'   relativeToRoot: false
 * 
 */
public interface CommonReferencePaths
{
	// ===============================================
	// Constants for nodes under '/'.
	// Prefix:  ''.
	// Statistics:
	//		197 path constants.
	//		50 leaf nodes.
	public static final Path _Root = Path.parse("/root");

	// Table type path
	public final class _Root_Application
	{
		private static final Path _Root_Application = _Root.add("Application");
		public static Path getPathInSchema()
		{
			return _Root_Application;
		}
		public static final Path _Base = Path.parse("./base");
		public static final Path _Base_Id = Path.parse("./base/id");
		public static final Path _Base_Name = Path.parse("./base/name");
		public static final Path _Base_Description = Path.parse("./base/description");
		public static final Path _ApplicationRoles = Path.parse("./applicationRoles");
		public static final Path __applicationRoleList = Path.parse("./_applicationRoleList");
	} 

	// Table type path
	public final class _Root_ApplicationRole
	{
		private static final Path _Root_ApplicationRole = _Root.add("ApplicationRole");
		public static Path getPathInSchema()
		{
			return _Root_ApplicationRole;
		}
		public static final Path _Application = Path.parse("./application");
		public static final Path _Base = Path.parse("./base");
		public static final Path _Base_Id = Path.parse("./base/id");
		public static final Path _Base_Name = Path.parse("./base/name");
		public static final Path _Base_Description = Path.parse("./base/description");
	} 

	// Table type path
	public final class _Root_CommodityType
	{
		private static final Path _Root_CommodityType = _Root.add("CommodityType");
		public static Path getPathInSchema()
		{
			return _Root_CommodityType;
		}
		public static final Path _Base = Path.parse("./base");
		public static final Path _Base_Id = Path.parse("./base/id");
		public static final Path _Base_Name = Path.parse("./base/name");
		public static final Path _Base_Description = Path.parse("./base/description");
		public static final Path _ParentCommodityType = Path.parse("./parentCommodityType");
		public static final Path _ChildCommodityTypes = Path.parse("./childCommodityTypes");
	} 

	// Table type path
	public final class _Root_Concept
	{
		private static final Path _Root_Concept = _Root.add("Concept");
		public static Path getPathInSchema()
		{
			return _Root_Concept;
		}
		public static final Path _Base = Path.parse("./base");
		public static final Path _Base_Id = Path.parse("./base/id");
		public static final Path _Base_Name = Path.parse("./base/name");
		public static final Path _Base_Description = Path.parse("./base/description");
		public static final Path _AllowedServiceTypes = Path.parse("./allowedServiceTypes");
		public static final Path __allowedServiceTypeList = Path.parse("./_allowedServiceTypeList");
	} 

	// Table type path
	public final class _Root_CatalogGroup
	{
		private static final Path _Root_CatalogGroup = _Root.add("CatalogGroup");
		public static Path getPathInSchema()
		{
			return _Root_CatalogGroup;
		}
		public static final Path _Id = Path.parse("./id");
		public static final Path _Name = Path.parse("./name");
		public static final Path _Description = Path.parse("./description");
	} 

	// Table type path
	public final class _Root_CATALOG_GROUP_MAPPING
	{
		private static final Path _Root_CATALOG_GROUP_MAPPING = _Root.add("CATALOG_GROUP_MAPPING");
		public static Path getPathInSchema()
		{
			return _Root_CATALOG_GROUP_MAPPING;
		}
		public static final Path _SERVICE_TYPE_ID = Path.parse("./SERVICE_TYPE_ID");
		public static final Path _MENU_STRATEGY_ID = Path.parse("./MENU_STRATEGY_ID");
		public static final Path _CATALOG_GROUP_ID = Path.parse("./CATALOG_GROUP_ID");
	} 

	// Table type path
	public final class _Root_CatalogGroupServiceType
	{
		private static final Path _Root_CatalogGroupServiceType = _Root.add("CatalogGroupServiceType");
		public static Path getPathInSchema()
		{
			return _Root_CatalogGroupServiceType;
		}
		public static final Path _CatalogGroup = Path.parse("./catalogGroup");
		public static final Path _ServiceType = Path.parse("./serviceType");
	} 

	// Table type path
	public final class _Root_Menu_Strategy
	{
		private static final Path _Root_Menu_Strategy = _Root.add("Menu_Strategy");
		public static Path getPathInSchema()
		{
			return _Root_Menu_Strategy;
		}
		public static final Path _MENU_STRATEGY_ID = Path.parse("./MENU_STRATEGY_ID");
		public static final Path _MENU_STRATEGY_NAME = Path.parse("./MENU_STRATEGY_NAME");
		public static final Path _MENU_STRATEGY_DESC = Path.parse("./MENU_STRATEGY_DESC");
	} 

	// Table type path
	public final class _Root_ServiceType
	{
		private static final Path _Root_ServiceType = _Root.add("ServiceType");
		public static Path getPathInSchema()
		{
			return _Root_ServiceType;
		}
		public static final Path _Base = Path.parse("./base");
		public static final Path _Base_Id = Path.parse("./base/id");
		public static final Path _Base_Name = Path.parse("./base/name");
		public static final Path _Base_Description = Path.parse("./base/description");
		public static final Path _AllowableConcepts = Path.parse("./allowableConcepts");
		public static final Path __allowableConceptList = Path.parse("./_allowableConceptList");
	} 

	// Table type path
	public final class _Root_ServiceTypeConcept
	{
		private static final Path _Root_ServiceTypeConcept = _Root.add("ServiceTypeConcept");
		public static Path getPathInSchema()
		{
			return _Root_ServiceTypeConcept;
		}
		public static final Path _ServiceType = Path.parse("./serviceType");
		public static final Path _Concept = Path.parse("./concept");
	} 
	public static final Path _Root_Contact_Types = _Root.add("Contact_Types");

	// Table type path
	public final class _Root_Contact_Types_ADDRESS_TYPE
	{
		private static final Path _Root_Contact_Types_ADDRESS_TYPE = _Root_Contact_Types.add("ADDRESS_TYPE");
		public static Path getPathInSchema()
		{
			return _Root_Contact_Types_ADDRESS_TYPE;
		}
		public static final Path _ADDRESS_TYPE_ID = Path.parse("./ADDRESS_TYPE_ID");
		public static final Path _ADDRESS_TYPE_DESC = Path.parse("./ADDRESS_TYPE_DESC");
		public static final Path _IS_ACTIVE = Path.parse("./IS_ACTIVE");
		public static final Path _AllowMultiple = Path.parse("./allowMultiple");
	} 

	// Table type path
	public final class _Root_Contact_Types_ADDRESS_STATUS
	{
		private static final Path _Root_Contact_Types_ADDRESS_STATUS = _Root_Contact_Types.add("ADDRESS_STATUS");
		public static Path getPathInSchema()
		{
			return _Root_Contact_Types_ADDRESS_STATUS;
		}
		public static final Path _ADDRESS_STATUS_ID = Path.parse("./ADDRESS_STATUS_ID");
		public static final Path _ADDRESS_STATUS_DESC = Path.parse("./ADDRESS_STATUS_DESC");
		public static final Path _IS_ENABLED = Path.parse("./IS_ENABLED");
	} 

	// Table type path
	public final class _Root_Contact_Types_EMAIL_TYPE
	{
		private static final Path _Root_Contact_Types_EMAIL_TYPE = _Root_Contact_Types.add("EMAIL_TYPE");
		public static Path getPathInSchema()
		{
			return _Root_Contact_Types_EMAIL_TYPE;
		}
		public static final Path _EMAIL_TYPE_ID = Path.parse("./EMAIL_TYPE_ID");
		public static final Path _EMAIL_TYPE_DESC = Path.parse("./EMAIL_TYPE_DESC");
		public static final Path _IS_VALID = Path.parse("./IS_VALID");
	} 

	// Table type path
	public final class _Root_Contact_Types_PHONE_TYPE
	{
		private static final Path _Root_Contact_Types_PHONE_TYPE = _Root_Contact_Types.add("PHONE_TYPE");
		public static Path getPathInSchema()
		{
			return _Root_Contact_Types_PHONE_TYPE;
		}
		public static final Path _PHONE_TYPE_ID = Path.parse("./PHONE_TYPE_ID");
		public static final Path _PHONE_TYPE_DESC = Path.parse("./PHONE_TYPE_DESC");
		public static final Path _IS_ACTIVE = Path.parse("./IS_ACTIVE");
	} 
	// ===============================================

}

package com.aramark.mdm.orgn.usertask;

import com.aramark.mdm.orgn.path.*;
import com.onwbp.adaptation.*;
import com.onwbp.base.text.*;
import com.orchestranetworks.ps.usertask.*;
import com.orchestranetworks.ps.workflow.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.*;

public class ServiceRequestNumberUserTask extends GeneralMaintenanceUserTask
{
	@Override
	public void checkBeforeWorkItemCompletion(UserTaskBeforeWorkItemCompletionContext context)
	{
		// Exit criteria - Service request number must be ???
		if (context.isAcceptAction())
		{
			Adaptation dataSet = null;
			try
			{
				dataSet = WorkflowUtilities.getDataSet(
					context,
					context.getRepository(),
					WorkflowConstants.PARAM_WORKING_DATA_SPACE);
			}
			catch (OperationException ex)
			{
				context.reportMessage(UserMessage.createError("Error looking up dataset.", ex));
				return;
			}
			checkFieldRequired(context, dataSet, OrganizationPaths._Root_ServiceRequest);
		}

		super.checkBeforeWorkItemCompletion(context);
	}
	
	@Override
	public void handleWorkItemCompletion(UserTaskWorkItemCompletionContext context)
		throws OperationException
	{
		Adaptation dataSet = WorkflowUtilities.getDataSet(
			context,
			context.getRepository(),
			WorkflowConstants.PARAM_WORKING_DATA_SPACE);
		String serviceReqNum = dataSet.getString(OrganizationPaths._Root_ServiceRequest);
		context.setVariableString("serviceRequestNumber", serviceReqNum);
		super.handleWorkItemCompletion(context);
	}

	
	
	
}

package com.aramark.mdm.orgn.usertask;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.aramark.mdm.common.constants.AramarkWorkflowConstants;
import com.aramark.mdm.common.enums.Countries;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.base.text.UserMessage;
import com.orchestranetworks.ps.usertask.GeneralMaintenanceUserTask;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.ps.workflow.WorkflowConstants;
import com.orchestranetworks.ps.workflow.WorkflowUtilities;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.comparison.DifferenceBetweenHomes;
import com.orchestranetworks.service.comparison.DifferenceBetweenInstances;
import com.orchestranetworks.service.comparison.DifferenceBetweenOccurrences;
import com.orchestranetworks.service.comparison.DifferenceBetweenTables;
import com.orchestranetworks.service.comparison.DifferenceHelper;
import com.orchestranetworks.service.comparison.ExtraOccurrenceOnRight;
import com.orchestranetworks.workflow.UserTaskBeforeWorkItemCompletionContext;

public class OrganizationDataEntryUserTask extends GeneralMaintenanceUserTask
{
	@Override
	public void checkBeforeWorkItemCompletion(UserTaskBeforeWorkItemCompletionContext context)
	{

		if (context.isAcceptAction())
		{
			Adaptation dataSet = null;
			try
			{
				dataSet = WorkflowUtilities.getDataSet(
					context,
					context.getRepository(),
					WorkflowConstants.PARAM_WORKING_DATA_SPACE);
			}
			catch (OperationException ex)
			{
				context.reportMessage(UserMessage.createError("Error looking up dataset.", ex));
				return;
			}
			String orgNodeType = context
				.getVariableString(AramarkWorkflowConstants.PARAM_ORG_NODE_TYPE);
			AdaptationHome workingDataSpace = null;
			if (orgNodeType != null && orgNodeType.equals("Location"))
			{
				try
				{
					workingDataSpace = WorkflowUtilities.getDataSpace(
						context,
						context.getRepository(),
						WorkflowConstants.PARAM_WORKING_DATA_SPACE);
				}
				catch (OperationException ex)
				{
					context
						.reportMessage(UserMessage.createError("Error looking up dataspace.", ex));
					return;
				}

				try
				{
					List<Adaptation> locationAttributesAdaptations = computeDelta(
						workingDataSpace.getParent(),
						workingDataSpace,
						context);
					if (locationAttributesAdaptations != null
						&& !locationAttributesAdaptations.isEmpty())
					{
						for (Adaptation locationAttributeAdaptation : locationAttributesAdaptations)
						{
							Adaptation orgNode = AdaptationUtil.followFK(
								locationAttributeAdaptation,
								OrganizationPaths._Root_Organization_Location_LocationAttributes._OrgNode);
							String countryCd = orgNode.getString(
								OrganizationPaths._Root_Organization_OrgNode._Country);
							if (countryCd.equals(Countries.USA))
							{
								checkFieldRequired(
									context,
									locationAttributeAdaptation,
									OrganizationPaths._Root_Organization_Location_LocationAttributes._Financials_PostalCD,
									true,
									true);
							}
						}
					}
				}
				catch (OperationException e)
				{
					e.printStackTrace();
				}
				// old location for checkBeforeWorkItemCompletion
			}
			super.checkBeforeWorkItemCompletion(context);
		}
	}
	

	/**
	* 
	* @param initialSnapshot
	* @param workingDataSpace
	* @param context
	* @throws OperationException 
	*/
	private List<Adaptation> computeDelta(
		final AdaptationHome initialSnapshot,
		final AdaptationHome workingDataSpace,
		UserTaskBeforeWorkItemCompletionContext context) throws OperationException
	{
		final Path locationAttributeTablePath = OrganizationPaths._Root_Organization_Location_LocationAttributes
			.getPathInSchema();
		List<Adaptation> locationAttributesAdaptations = new ArrayList<Adaptation>();
		final DifferenceBetweenHomes differenceBetweenDataSpaces = DifferenceHelper
			.compareHomes(initialSnapshot, workingDataSpace, false);
		if (!differenceBetweenDataSpaces.isEmpty())
		{
			@SuppressWarnings("unchecked")
			final List<DifferenceBetweenInstances> differenceBetweenInstances = differenceBetweenDataSpaces
				.getDeltaInstances();
			for (final Iterator<DifferenceBetweenInstances> iterator = differenceBetweenInstances
				.iterator(); iterator.hasNext();)
			{
				final DifferenceBetweenInstances differenceBetweenInstance = iterator.next();
				if (!differenceBetweenInstances.isEmpty())
				{
					@SuppressWarnings("unchecked")
					final List<DifferenceBetweenTables> deltaTables = differenceBetweenInstance
						.getDeltaTables();
					for (final Iterator<DifferenceBetweenTables> iteratorOnTables = deltaTables
						.iterator(); iteratorOnTables.hasNext();)
					{
						final DifferenceBetweenTables differenceBetweenTable = iteratorOnTables
							.next();
						if (locationAttributeTablePath
							.equals(differenceBetweenTable.getPathOnLeft()))
						{
							// Modified records
							@SuppressWarnings("unchecked")
							final List<DifferenceBetweenOccurrences> deltaOccurrences = differenceBetweenTable
								.getDeltaOccurrences();
							for (final Iterator<DifferenceBetweenOccurrences> iteratorOnOccurences = deltaOccurrences
								.iterator(); iteratorOnOccurences.hasNext();)
							{
								final DifferenceBetweenOccurrences differenceBetweenOccurrence = iteratorOnOccurences
									.next();
								locationAttributesAdaptations
									.add(differenceBetweenOccurrence.getOccurrenceOnRight());
							}
							// Created records
							@SuppressWarnings("unchecked")
							final List<ExtraOccurrenceOnRight> extraOccurrencesOnRight = differenceBetweenTable
								.getExtraOccurrencesOnRight();
							for (final Iterator<ExtraOccurrenceOnRight> iteratorOnOccurences = extraOccurrencesOnRight
								.iterator(); iteratorOnOccurences.hasNext();)
							{
								final ExtraOccurrenceOnRight extraOccurrenceOnRight = iteratorOnOccurences
									.next();
								locationAttributesAdaptations
									.add(extraOccurrenceOnRight.getExtraAdaptationOnRight());

							}
						}
					}
				}
			}
		}
		return locationAttributesAdaptations;
	}


	
	
	
}

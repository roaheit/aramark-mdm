package com.aramark.mdm.orgn.tablereffilter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.aramark.mdm.common.enums.YesNoFlags;
import com.aramark.mdm.orgn.path.OrganizationExternalRefPaths;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;

public class OrgMapSourceSysValFilterTableRefFilter implements TableRefFilter {

	private static final String MESSAGE = "";
	

	@Override
	public boolean accept(Adaptation adaptation, ValueContext valueContext) {

		// Filter applied (./IS_MAPPED='N' or
		// ./SOURCE_SYSTEM_VALUE_ID=${../SOURCE_SYSTEM_VALUE/SOURCE_SYSTEM_VALUE_ID})
		// and ./SOURCE_SYSTEM=${../SOURCE_SYSTEM} and ./IS_IGNORED='N' and
		// ./ORG_NODE_TYPE=${../ORG_NODE_TYPE_ID}
		//TFS 1641 

		if (adaptation.getOccurrencePrimaryKey().format().equals(valueContext.getValue(Path.SELF))) {
			return true;
		}

		Adaptation orgNodeAdapt = AdaptationUtil.followFK(valueContext,
			Path.PARENT.add(OrganizationPaths._Root_Organization_OrgMapping._OrgNode));

		Adaptation sourceSysAdapt = AdaptationUtil.followFK(valueContext,
			Path.PARENT.add(OrganizationPaths._Root_Organization_OrgMapping._SourceSystem));

		if (orgNodeAdapt == null && sourceSysAdapt == null) {
			return false;
		}

		List<Adaptation> parentOrgNodeAdapts = AdaptationUtil.getLinkedRecordList(orgNodeAdapt,
			OrganizationPaths._Root_Organization_OrgNode._OrgHierarchy);

		List<String> parentOrgVaLNnodeList = new ArrayList<String>();
		if (parentOrgNodeAdapts == null) {
			return false;
		} else {
			for (Adaptation parents : parentOrgNodeAdapts) {

				Adaptation parentOrg = AdaptationUtil.followFK(parents,
					OrganizationPaths._Root_Organization_OrgHierarchy._ParentOrgHierarchy);

				Adaptation orgIdAdapt = AdaptationUtil.followFK(parentOrg,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode);

				String parentOrgValNnodeType = orgIdAdapt
					.getString(OrganizationPaths._Root_Organization_OrgNode._OrgNodeOrgValue) + "|"
					+ orgIdAdapt
						.getString(OrganizationPaths._Root_Organization_OrgNode._OrgNodeType);

				parentOrgVaLNnodeList.add(parentOrgValNnodeType);

			}
		}

		String sourceSysVal = (String) valueContext.getValue(Path.SELF);

		String orgNodeTypePredicate = orgNodeAdapt
			.getString(OrganizationPaths._Root_Organization_OrgNode._OrgNodeType);

		String sourceSystemPredicate = sourceSysAdapt
			.getString(OrganizationExternalRefPaths._Root_SourceSystem._SourceSystemID);

		String unfilteredorgnodeType = adaptation
			.getString(OrganizationExternalRefPaths._Root_SourceSystemValue._OrgNodeType);

		String unfilteredIsMapped = adaptation
			.getString(OrganizationExternalRefPaths._Root_SourceSystemValue._IsMapped);

		String unfilteredSrcSysVal = String.valueOf(
			adaptation.get_int(
				OrganizationExternalRefPaths._Root_SourceSystemValue._SourceSystemValueID));

		String unfilteredSourceSystem = adaptation
			.getString(OrganizationExternalRefPaths._Root_SourceSystemValue._SourceSystem);

		String unfilteredIsIgnored = adaptation
			.getString(OrganizationExternalRefPaths._Root_SourceSystemValue._IsIgnored);

		String unfilteredParSysValId = adaptation
			.getString(
				OrganizationExternalRefPaths._Root_SourceSystemValue._ParentSourceSystemValue);

		String unfilteredParOrgNodeType = adaptation
			.getString(OrganizationExternalRefPaths._Root_SourceSystemValue._ParentOrgNodeType);

		if (orgNodeTypePredicate != null && unfilteredorgnodeType != null && unfilteredIsMapped != null
				&& unfilteredIsIgnored != null && unfilteredSourceSystem != null && unfilteredParSysValId != null
				&& unfilteredParOrgNodeType != null && unfilteredSrcSysVal != null) {

			if (parentOrgVaLNnodeList != null) {
				String unfilteredOrgValParNnodeType = unfilteredParSysValId + "|" + unfilteredParOrgNodeType;
				for (int i = 0; i < parentOrgVaLNnodeList.size(); i++) {
					boolean isParent = (parentOrgVaLNnodeList.get(i)).equals(unfilteredOrgValParNnodeType);
					if (isParent) {
						return ((orgNodeTypePredicate.equals(unfilteredorgnodeType)
								&& unfilteredIsIgnored.equals(YesNoFlags.NO)
								&& sourceSystemPredicate.equals(unfilteredSourceSystem))
								&& (unfilteredIsMapped.equals(YesNoFlags.NO)
										|| (sourceSysVal != null && unfilteredSrcSysVal.equals(sourceSysVal)))
								&& isParent);

					}

				}

			}
			return ((orgNodeTypePredicate.equals(unfilteredorgnodeType) && unfilteredIsIgnored.equals(YesNoFlags.NO)
					&& sourceSystemPredicate.equals(unfilteredSourceSystem))
					&& (unfilteredIsMapped.equals(YesNoFlags.NO)
							|| (sourceSysVal != null && unfilteredSrcSysVal.equals(sourceSysVal))));

		}

		return false;
	}

	@Override
	public void setup(TableRefFilterContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return MESSAGE;
	}

}

package com.aramark.mdm.orgn.tablereffilter;

import java.util.Locale;

import com.aramark.mdm.common.enums.SubLocationTypes;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;

public class SubLocationAttrTableRefFilter implements TableRefFilter
{
	private static final String MESSAGE = "Brand pertains only to Sub Locations of type Consumer Outlets";

	@Override
	public boolean accept(Adaptation adaptation, ValueContext context)
	{
		// return true if the brand should be seen, false if it should be hidden. 
		String subLocationType = (String) context.getValue(
			Path.PARENT.add(
				OrganizationPaths._Root_Organization_SubLocation_SubLocationAttributes._SubLocationType));

		if (subLocationType == null)
		{
			return true;
		}

		if (!subLocationType.equals(SubLocationTypes.CONSUMER_OUTLET))
		{
			return false; // filter all brands when sublocation type is something other than consumer outlet. 
		}
		return true; // no filtering for SubLocationTypes.CONSUMER_OUTLET
	}

	@Override
	public void setup(TableRefFilterContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext) throws InvalidSchemaException {
		// TODO Auto-generated method stub
		return MESSAGE;
	}

}

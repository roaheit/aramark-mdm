package com.aramark.mdm.orgn.tablereffilter;

import java.util.Locale;

import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.TableRefFilter;
import com.orchestranetworks.schema.TableRefFilterContext;

public class LocAttrSpecificToServiceTypeTableRefFilter implements TableRefFilter
{
	private static final String MESSAGE = "";
	// constants that make this code generic across the FK's in location attributes ex. Bill code, concept, service type etc.
	Path CHART_ID = Path.parse("./CHART_ID");
	Path COUNTRY_CD = Path.parse("./COUNTRY_CD");

	@Override
	public boolean accept(Adaptation adaptation, ValueContext context)
	{
		// get locations country code and coa chart from the corresponding org node
		
		Adaptation orgNode = AdaptationUtil.followFK(
			context,
			Path.PARENT
				.add(OrganizationPaths._Root_Organization_Location_LocationAttributes._OrgNode));
			
		if (orgNode == null)
		{
			return false;
		}
		
		String desiredChartId = orgNode
			.getString(OrganizationPaths._Root_Organization_OrgNode.__coaChart);
		String desiredCountryCd = orgNode
			.getString(OrganizationPaths._Root_Organization_OrgNode._Country);
		
		//
		// adaption contains the Rows to be filtered.  Use the constants above to allow this class to work against 
		//  several different FK tables Intercompany code, tax code, bill code, future code, service type etc. 
		//  
		
		String unfilteredChartId = adaptation.getString(CHART_ID);		
		String unfilteredCountryCd = adaptation.getString(COUNTRY_CD);		
		if (desiredChartId != null && desiredCountryCd != null  && unfilteredChartId != null && unfilteredCountryCd != null 
				&& desiredChartId.equals(unfilteredChartId) && desiredCountryCd.equals(unfilteredCountryCd)  ) 
		{
			return true; 
		}
		return false; 
	}
	@Override
	public void setup(TableRefFilterContext arg0)
	{

	}

	@Override
	public String toUserDocumentation(Locale arg0, ValueContext arg1) throws InvalidSchemaException
	{
		return MESSAGE;
	}

}

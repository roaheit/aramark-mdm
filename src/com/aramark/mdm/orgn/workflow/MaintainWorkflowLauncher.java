/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.aramark.mdm.orgn.workflow;

import javax.servlet.http.HttpServletRequest;

import com.aramark.mdm.common.constants.AramarkWorkflowConstants;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.orchestranetworks.ps.workflow.WorkflowLauncher;
import com.orchestranetworks.service.OperationException;


/**
 */
@SuppressWarnings("serial")
public class MaintainWorkflowLauncher extends WorkflowLauncher
{
	
	
	
	public MaintainWorkflowLauncher()
	{
		super();
	}

	public void execute(HttpServletRequest request) throws OperationException
	{
		super.execute(
			request,
			AramarkWorkflowConstants.WF_KEY_CREATE_OR_UPDATE,
			OrganizationPaths._Root_Organization_OrgNode.getPathInSchema().format(),
			null);
	}

	
	/**
	 * Get the workflow model label as it should appear in the workflow instance name.
	 * By default, this simply returns the input but it can be subclassed to massage the label.
	 * 
	 * @param workflowModelLabel the label of the workflow model
	 * @return the label as it should appear in the workflow instance name
	 */
	protected String getWorkflowModelLabelForInstanceName(String workflowModelLabel)
	{
		return workflowModelLabel;
	}




}

/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.aramark.mdm.orgn.workflow;

import javax.servlet.http.HttpServletRequest;

import com.aramark.mdm.common.constants.AramarkWorkflowConstants;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.orchestranetworks.ps.workflow.WorkflowLauncher;
import com.orchestranetworks.service.OperationException;


/**
 */
@SuppressWarnings("serial")
public class MaintainOrgNodeWorkflowLauncher extends WorkflowLauncher
{
	private String orgNodeType = null;

	private String orgNodeTypeCode = null;

	public String getOrgNodeTypeCode() {
		return orgNodeTypeCode;
	}

	public void setOrgNodeTypeCode(String orgNodeTypeCode) {
		this.orgNodeTypeCode = orgNodeTypeCode;
	}

	public String getOrgNodeType() {
		return orgNodeType;
	}

	public void setOrgNodeType(String orgNodeType) {
		this.orgNodeType = orgNodeType;
	}

	public MaintainOrgNodeWorkflowLauncher(String orgNodeType, String orgNodeTypeCode)
	{
		super();
		this.orgNodeType = orgNodeType;
		this.orgNodeTypeCode = orgNodeTypeCode;
	}
	
	public MaintainOrgNodeWorkflowLauncher(String orgNodeType)
	{
		super();
		this.orgNodeType = orgNodeType;
		this.orgNodeTypeCode = orgNodeType;
	}

	public MaintainOrgNodeWorkflowLauncher()
	{
		super();
	}

	public void execute(HttpServletRequest request) throws OperationException
	{
		super.execute(
			request,
			AramarkWorkflowConstants.WF_KEY_CREATE_OR_UPDATE_ORG_NODE,
			OrganizationPaths._Root_Organization_OrgNode.getPathInSchema().format(),
			null);
	}

	
	/**
	 * Get the workflow model label as it should appear in the workflow instance name.
	 * By default, this simply returns the input but it can be subclassed to massage the label.
	 * 
	 * @param workflowModelLabel the label of the workflow model
	 * @return the label as it should appear in the workflow instance name
	 */
	protected String getWorkflowModelLabelForInstanceName(String workflowModelLabel)
	{
		return workflowModelLabel;
	}



	@Override
	protected void setAdditionalContextVariables() throws OperationException
	{
		launcher.setInputParameter(AramarkWorkflowConstants.PARAM_ORG_NODE_TYPE, orgNodeType);
		launcher.setInputParameter(AramarkWorkflowConstants.PARAM_ORG_NODE_TYPE_CODE, orgNodeTypeCode);
		
		super.setAdditionalContextVariables();

	}

}

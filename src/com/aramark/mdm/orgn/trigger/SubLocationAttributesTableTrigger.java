package com.aramark.mdm.orgn.trigger;

import com.aramark.mdm.common.enums.SubLocationTypes;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ValueContextForUpdate;

public class SubLocationAttributesTableTrigger extends BaseTableTrigger
{
	@Override
	public void setup(TriggerSetupContext context)
	{
		super.setup(context);
	}

	@Override
	public void handleNewContext(NewTransientOccurrenceContext context)
	{

		ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		try
		{
			setDefaultType(vc);
			setDefaultBrand(vc, "1");

		}
		catch (Exception e)
		{
			LoggingCategory.getKernel().info(e.getMessage());
		}

	}
	
	private void setDefaultType(ValueContextForUpdate vc)
		throws OperationException
	{

		vc.setValue(
			SubLocationTypes.CONSUMER_OUTLET,
			OrganizationPaths._Root_Organization_SubLocation_SubLocationAttributes._SubLocationType);
	}

	private void setDefaultBrand(ValueContextForUpdate vc, String brand) throws OperationException
	{

		vc.setValue(
			brand,
			OrganizationPaths._Root_Organization_SubLocation_SubLocationAttributes._Brand);

	}

}

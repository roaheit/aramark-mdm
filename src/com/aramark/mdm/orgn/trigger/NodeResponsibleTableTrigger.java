package com.aramark.mdm.orgn.trigger;

import java.util.HashMap;

import com.aramark.mdm.common.enums.ResponsibleStatuses;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.aramark.mdm.orgn.path.OrganizationRefPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.ps.procedure.ModifyValuesProcedure;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.schema.trigger.ValueChanges;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;

public class NodeResponsibleTableTrigger extends BaseTableTrigger
{


	@Override
	public void setup(TriggerSetupContext context)
	{
		super.setup(context);

	}

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context) throws OperationException
	{
		ProcedureContext nodeRespContext = context.getProcedureContext();
		Adaptation nodeRespAdaptation = context.getAdaptationOccurrence();
		setOldActiveRespToInactive(nodeRespAdaptation, nodeRespContext);

	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context) throws OperationException
	{
		ProcedureContext nodeRespContext = context.getProcedureContext();
		Adaptation nodeRespAdaptation = context.getAdaptationOccurrence();
		ValueChanges valueChanges = context.getChanges();
		ValueChange respStatusValueChange = valueChanges
			.getChange(
				OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus);
		ValueChange respTypeValueChange = valueChanges
			.getChange(
				OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType);
		String respStatus = nodeRespAdaptation
			.getString(
				OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus);
		if ((respStatusValueChange != null || respTypeValueChange != null)
			&& respStatus.equals(ResponsibleStatuses.ACTIVE))
		{
			setOldActiveRespToInactive(nodeRespAdaptation, nodeRespContext);
		}
	}

	private void setOldActiveRespToInactive(
		Adaptation nodeRespAdaptation,
		ProcedureContext nodeRespContext)
		throws OperationException
	{

		String respType = nodeRespAdaptation
			.getString(
				OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType);
		String respStatus = nodeRespAdaptation
			.getString(
				OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus);
		String orgNodeId = (String) nodeRespAdaptation
			.getString(OrganizationPaths._Root_Organization_Node_NodeResponsible._OrgNode);
		Adaptation respTypeAdaptation = AdaptationUtil.followFK(
			nodeRespAdaptation,
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType);
		String allowMultipleFlag = respTypeAdaptation.getString(
			OrganizationRefPaths._Root_Organization_Responsible_ResponsibleType._AllowMultiple);

		if (orgNodeId != null && !"Y".equals(allowMultipleFlag))
		{

			AdaptationTable nodeRespAdaptationTable = nodeRespAdaptation.getContainer()
				.getTable(
					OrganizationPaths._Root_Organization_Node_NodeResponsible.getPathInSchema());

			String predicate = OrganizationPaths._Root_Organization_Node_NodeResponsible._OrgNode
				.format() + " = '" + orgNodeId + "' and "
				+ OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType
					.format()
				+ " = '" + respType + "'" + " and "
				+ OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus
					.format()
				+ " = '" + respStatus + "'";

			RequestResult reqRes = nodeRespAdaptationTable.createRequestResult(predicate);
			if ((reqRes != null) && (reqRes.isSizeGreaterOrEqual(1)))
			{
				Adaptation newnodeRespAdaptation = null;

				try
				{
					while ((newnodeRespAdaptation = reqRes.nextAdaptation()) != null)
					{
						if (!nodeRespAdaptation.equalsToAdaptation(newnodeRespAdaptation))

							updateRespStatus(
								nodeRespContext,
								newnodeRespAdaptation,
								ResponsibleStatuses.INACTIVE);
					}
				}

				finally
				{
					reqRes.close();
				}
			}

		}
	}

	private void updateRespStatus(
		ProcedureContext nodeRespContext,
		Adaptation newnodeRespAdaptation,
		String respStatus)
		throws OperationException
	{
		HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus,
			respStatus);
		ModifyValuesProcedure
			.execute(nodeRespContext, newnodeRespAdaptation, pathValueMap, true, true);
	}

}

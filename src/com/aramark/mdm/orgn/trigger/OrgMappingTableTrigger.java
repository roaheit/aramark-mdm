package com.aramark.mdm.orgn.trigger;

import java.util.HashMap;

import com.aramark.mdm.common.enums.YesNoFlags;
import com.aramark.mdm.orgn.path.OrganizationExternalRefPaths;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.procedure.ModifyValuesProcedure;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterDeleteOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;

public class OrgMappingTableTrigger extends BaseTableTrigger {

	@Override
	public void setup(TriggerSetupContext context) {
		super.setup(context);

	}

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context) throws OperationException {

		ProcedureContext pContext = context.getProcedureContext();
		Adaptation orgMappingAdaptation = context.getAdaptationOccurrence();
		Session contexSession = context.getSession();

		Adaptation sourceSystemValueAdaptation = AdaptationUtil.followFK(orgMappingAdaptation,
			OrganizationPaths._Root_Organization_OrgMapping._SourceSystemValue);

		String isMapped = sourceSystemValueAdaptation
			.getString(OrganizationExternalRefPaths._Root_SourceSystemValue._IsMapped);
		if (isMapped != null) {
			updateISMapped(sourceSystemValueAdaptation, orgMappingAdaptation, isMapped, contexSession, pContext);
		}
	}

	@Override
	public void handleAfterDelete(AfterDeleteOccurrenceContext bContext) throws OperationException {

		ProcedureContext pContext = bContext.getProcedureContext();
		Adaptation orgMappingAdaptation = bContext.getTable().getContainerAdaptation();

		ValueContext currentRecord = bContext.getOccurrenceContext();

		Session bContextSession = bContext.getSession();

		Adaptation sourceSystemValueAdaptation = AdaptationUtil.followFK(currentRecord,
			OrganizationPaths._Root_Organization_OrgMapping._SourceSystemValue);

		String isMapped = sourceSystemValueAdaptation
			.getString(OrganizationExternalRefPaths._Root_SourceSystemValue._IsMapped);
		if (isMapped != null) {
			updateISMapped(sourceSystemValueAdaptation, orgMappingAdaptation, isMapped, bContextSession, pContext);

		}

	}

	private void updateISMapped(Adaptation sourceSystemValueAdaptation, Adaptation orgMappingAdaptation,
			String isMapped, Session session, ProcedureContext pContext) throws OperationException {
		HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
		String updateIsMapped = null;
		if (isMapped.equals(YesNoFlags.NO)) {
			updateIsMapped = YesNoFlags.YES;
			pathValueMap.put(
				OrganizationExternalRefPaths._Root_SourceSystemValue._IsMapped,
				updateIsMapped);

		} else {
			updateIsMapped = YesNoFlags.NO;
			pathValueMap.put(
				OrganizationExternalRefPaths._Root_SourceSystemValue._IsMapped,
				updateIsMapped);

		}

		if (orgMappingAdaptation.getHome().getKey().getName().equals("OrganizationDataSpace")) {
			ModifyValuesProcedure.execute(pContext, sourceSystemValueAdaptation, pathValueMap);
		} else {
			ModifyValuesProcedure.execute(sourceSystemValueAdaptation, pathValueMap, session);
		}

	}

}

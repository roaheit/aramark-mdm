package com.aramark.mdm.orgn.trigger;

import java.util.HashMap;
import java.util.List;

import com.aramark.mdm.common.constants.AramarkOrganizationConstants;
import com.aramark.mdm.common.enums.OrgNodeTypes;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.aramark.mdm.wipcoa.path.WipCoaPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.ps.procedure.ModifyValuesProcedure;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.BeforeModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.schema.trigger.ValueChanges;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

public class OrgHierarchyTableTrigger extends BaseTableTrigger {
	@Override
	public void setup(TriggerSetupContext context) {
		super.setup(context);
	}

	public void handleNewContext(NewTransientOccurrenceContext context) {
		ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		// how to get the session?
		Session session = context.getSession();
		if (session.getInteraction(true) != null) {
			if (session.getTrackingInfo() != null && session.getTrackingInfo().equals("ORGN-Location-DataEntry")) {
				// vc.setValue("STE|9",OrganizationPaths._Root_Organization_ORG_HIERARCHY._ORG_HIERARCHY_TYPE_CD);
				vc.setValue(AramarkOrganizationConstants.LOCATION_IN_SITE_HIER,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
				vc.setValue(OrgNodeTypes.LOCATION,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType);
			}

			if (session.getTrackingInfo() != null
				&& session.getTrackingInfo().equals("ORGN-Sub Location-DataEntry"))
			{
				vc.setValue(
					AramarkOrganizationConstants.SUB_LOCATION_IN_SITE_HIER,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
				vc.setValue(
					OrgNodeTypes.SUB_LOCATION,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType);
			}

			if (session.getTrackingInfo() != null
				&& session.getTrackingInfo().equals("ORGN-District-DataEntry"))
			{
				vc.setValue(
					AramarkOrganizationConstants.DISTRICT_IN_FINANCIAL_HIER,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
				vc.setValue(
					OrgNodeTypes.DISTRICT,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType);
			}
			if (session.getTrackingInfo() != null && session.getTrackingInfo().equals("ORGN-Site-DataEntry")) {
				// vc.setValue("STE|8",OrganizationPaths._Root_Organization_ORG_HIERARCHY._ORG_HIERARCHY_TYPE_CD);
				// vc.setValue("8",OrganizationPaths._Root_Organization_ORG_HIERARCHY._ORG_NODE_TYPE_ID);
				vc.setValue(AramarkOrganizationConstants.SITE_IN_SITE_HIER,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
				vc.setValue(
					OrgNodeTypes.SITE,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType);
			}

			if (session.getTrackingInfo() != null && session.getTrackingInfo().equals("ORGN-Profit Center-DataEntry")) {
				// vc.setValue("STE|1",OrganizationPaths._Root_Organization_ORG_HIERARCHY._ORG_HIERARCHY_TYPE_CD);
				// vc.setValue("1",OrganizationPaths._Root_Organization_ORG_HIERARCHY._ORG_NODE_TYPE_ID);
				vc.setValue(AramarkOrganizationConstants.PROFIT_CENTER_IN_SITE_HIER,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
				vc.setValue(OrgNodeTypes.PROFIT_CENTER,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType);
			}
		}
	}

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException {
		ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		setGlobalParent(vc);
		super.handleBeforeCreate(context);
	}

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context) throws OperationException {
		ProcedureContext pContext = context.getProcedureContext();
		Adaptation orgHierAdaptation = context.getAdaptationOccurrence();
		String orgHierType = orgHierAdaptation
			.getString(OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
		String orgParentHierId = orgHierAdaptation
			.getString(OrganizationPaths._Root_Organization_OrgHierarchy._ParentOrgHierarchy);
		if ((orgParentHierId != null) && (orgHierType != null)
			&& (AramarkOrganizationConstants.LOCATION_IN_SITE_HIER.equals(orgHierType)))
		{
			defaultLocAttributes(orgHierAdaptation, pContext);
		}
	}

	@Override
	public void handleBeforeModify(BeforeModifyOccurrenceContext context) throws OperationException
	{
		ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		setGlobalParent(vc);
		super.handleBeforeModify(context);
	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context) throws OperationException {
		ProcedureContext pContext = context.getProcedureContext();
		Adaptation orgHierAdaptation = context.getAdaptationOccurrence();
		String orgHierType = orgHierAdaptation
			.getString(OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
		ValueChanges valueChanges = context.getChanges();
		ValueChange parentValueChange = valueChanges
			.getChange(OrganizationPaths._Root_Organization_OrgHierarchy._ParentOrgHierarchy);

		// For locations in site hierarchy, if the parent profit center changed
		// this will require updates to
		// bill code, concept code, tax code, inter company code, future code,
		// event code, and service type
		// on the corresponding location attributes row
		//
		if ((parentValueChange != null) && (orgHierType != null)
				&& (AramarkOrganizationConstants.LOCATION_IN_SITE_HIER.equals(orgHierType))) {
			defaultLocAttributes(orgHierAdaptation, pContext);

		}
	}

	private void setGlobalParent(ValueContextForUpdate vc)
	{
		Adaptation parentOrgHierarchy = AdaptationUtil.followFK(
			vc,
			OrganizationPaths._Root_Organization_OrgHierarchy._ParentOrgHierarchy);
		if (parentOrgHierarchy != null)
		{
			String parentOrgNodeTypeId = parentOrgHierarchy
				.getString(OrganizationPaths._Root_Organization_OrgHierarchy._OrgNodeType);
			vc.setValue(
				parentOrgNodeTypeId,
				OrganizationPaths._Root_Organization_OrgHierarchy.__parentOrgNodeType);
			if (OrgNodeTypes.isGlobalHierarchyOrgNodeType(parentOrgNodeTypeId))
			{
				vc.setValue(
					parentOrgHierarchy.getOccurrencePrimaryKey().format(),
					OrganizationPaths._Root_Organization_OrgHierarchy.__globalParentOrgHierarchy);
			}
		}
	}

	private void defaultLocAttributes(Adaptation orgHierAdaptation, ProcedureContext pContext) {

		String orgNodeId = orgHierAdaptation
			.getString(OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode);
		if (orgNodeId != null) {
			Adaptation orgNodeAdaptation = AdaptationUtil.followFK(
				orgHierAdaptation,
				OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode);
			if (orgNodeAdaptation != null)
			{
				//  Is there a location for this org node?  If not, then there is nothing to default. 
				List<Adaptation> locAttrAdaptations = AdaptationUtil.getLinkedRecordList(
					orgNodeAdaptation,
					OrganizationPaths._Root_Organization_OrgNode._LocationAttributes);

				for (Adaptation locAttrAdaptation : locAttrAdaptations)
				{
					if (locAttrAdaptation != null)
					{
						// default values for Bill code, concept code, tax code,
						// inter-company code, future code, event type, service type
						Adaptation chartOfAcctAdaptation = AdaptationUtil.followFK(
							orgNodeAdaptation,
							OrganizationPaths._Root_Organization_OrgNode.__coaChart);

						if (chartOfAcctAdaptation != null)
						{
							try
							{
								String chartOfAccount = chartOfAcctAdaptation
									.getOccurrencePrimaryKey().format();
								String countryCode = orgNodeAdaptation.getString(
									OrganizationPaths._Root_Organization_OrgNode._Country);

								AdaptationHome wipMasterDataSpace;

								wipMasterDataSpace = AdaptationUtil
									.getDataSpaceOrThrowOperationException(
										pContext.getAdaptationHome().getRepository(),
										"WorkInProgressMasterDataSpace");

								Adaptation wipCOADataset = AdaptationUtil
									.getDataSetOrThrowOperationException(
										wipMasterDataSpace,
										"WIPCOADataSet");

								String billCode = DefaultCodes(
									WipCoaPaths._Root_BILL_CODE_CHART.getPathInSchema(),
									Path.parse("./BILL_CD"),
									"0",
									chartOfAccount,
									countryCode,
									wipCOADataset);

								String conceptCode = DefaultCodes(
									WipCoaPaths._Root_CONCEPT_CHART.getPathInSchema(),
									Path.parse("./CONCEPT_CD"),
									"0000",
									chartOfAccount,
									countryCode,
									wipCOADataset);

								String taxCode = DefaultCodes(
									WipCoaPaths._Root_TAX_CODE_CHART.getPathInSchema(),
									Path.parse("./TAX_CD"),
									"00",
									chartOfAccount,
									countryCode,
									wipCOADataset);

								String interCompCode = DefaultCodes(
									WipCoaPaths._Root_INTERCOMPANY_CODE_CHART.getPathInSchema(),
									Path.parse("./INTERCOMPANY_CD"),
									"0000",
									chartOfAccount,
									countryCode,
									wipCOADataset);

								String futureCode = DefaultCodes(
									WipCoaPaths._Root_FUTURE_CODE_CHART.getPathInSchema(),
									Path.parse("./FUTURE_CD"),
									"00000",
									chartOfAccount,
									countryCode,
									wipCOADataset);

								String eventCode = DefaultCodes(
									WipCoaPaths._Root_EVENT_CODE_CHART.getPathInSchema(),
									Path.parse("./EVENT_CD"),
									"000",
									chartOfAccount,
									countryCode,
									wipCOADataset);

								String serviceType = DefaultCodes(
									WipCoaPaths._Root_SERVICE_TYPE_CHART.getPathInSchema(),
									Path.parse("./SERVICE_TYPE_CD"),
									"000",
									chartOfAccount,
									countryCode,
									wipCOADataset);

								updateLocationRecord(
									pContext,
									locAttrAdaptation,
									billCode,
									conceptCode,
									taxCode,
									interCompCode,
									futureCode,
									eventCode,
									serviceType);

							}
							catch (OperationException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
	}

	private String DefaultCodes(
		Path wipCoaPath,
		Path colName,
		String defaultCode,
		String chartOfAccount,
			String countryCode, Adaptation wipCOADataset) {

		// procedure for Bill Code
		AdaptationTable codeChartTable = wipCOADataset.getTable(wipCoaPath);
		String predicate = Path.parse("./CHART_ID").format() + " = '" + chartOfAccount + "' and "
				+ Path.parse("./COUNTRY_CD").format() + " = '" + countryCode + "' and " + colName.format() + " = '"
				+ defaultCode + "'";

		RequestResult cdRequestResult = codeChartTable.createRequestResult(predicate);
		if (cdRequestResult != null) {
			try {
				Adaptation codeChart = null;
				if ((codeChart = cdRequestResult.nextAdaptation()) != null) {
					return codeChart.getOccurrencePrimaryKey().format();
				}

			} finally {
				cdRequestResult.close();
			}

		}
		// return null if the default value was not found in the table. 
		return null;
	}

	private void updateLocationRecord(
		ProcedureContext pContext,
		Adaptation locAttrAdaptation,
		String billCode,
		String conceptCode,
		String taxCode,
		String interCompCode,
		String futureCode,
		String eventCode,
		String serviceType) throws OperationException
	{
		HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._Financials_BillCDChart,
			billCode);
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._Financials_ConceptCDChart,
			conceptCode);
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._Financials_TaxCDChart,
			taxCode);
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._Financials_IntercompanyCDChart,
			interCompCode);
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._Financials_FutureCDChart,
			futureCode);
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._Financials_EventTypeChart,
			eventCode);
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._ServiceType,
			serviceType);
		ModifyValuesProcedure.execute(pContext, locAttrAdaptation, pathValueMap, true, true);
	}

}

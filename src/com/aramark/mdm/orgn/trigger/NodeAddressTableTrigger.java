package com.aramark.mdm.orgn.trigger;

import java.util.HashMap;

import com.aramark.mdm.common.enums.ResponsibleStatuses;
import com.aramark.mdm.common.path.CommonReferencePaths;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.ps.procedure.ModifyValuesProcedure;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.schema.trigger.ValueChanges;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;

public class NodeAddressTableTrigger extends BaseTableTrigger
{


	@Override
	public void setup(TriggerSetupContext context)
	{
		super.setup(context);

	}


	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context) throws OperationException
	{
		ProcedureContext nodeAddrContext = context.getProcedureContext();
		Adaptation nodeAddrAdaptation = context.getAdaptationOccurrence();
		setOldActiveAddrToInactive(nodeAddrAdaptation, nodeAddrContext);

	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context) throws OperationException
	{
		ProcedureContext nodeAddrContext = context.getProcedureContext();
		Adaptation nodeAddrAdaptation = context.getAdaptationOccurrence();
		ValueChanges valueChanges = context.getChanges();
		ValueChange addrStatusValueChange = valueChanges
			.getChange(OrganizationPaths._Root_Organization_Node_NodeAddress._AddressStatus);
		ValueChange addrTypeValueChange = valueChanges
			.getChange(OrganizationPaths._Root_Organization_Node_NodeAddress._AddressType);
		String addrStatus = nodeAddrAdaptation
			.getString(OrganizationPaths._Root_Organization_Node_NodeAddress._AddressStatus);
		if ((addrStatusValueChange != null || addrTypeValueChange != null)
			&& addrStatus.equals(ResponsibleStatuses.ACTIVE))
		{
			setOldActiveAddrToInactive(
				nodeAddrAdaptation,
				nodeAddrContext);
		}
	}

	private void setOldActiveAddrToInactive(
		Adaptation nodeAddrAdaptation,
		ProcedureContext nodeAddrContext)
		throws OperationException
	{

		String addrType = nodeAddrAdaptation
			.getString(OrganizationPaths._Root_Organization_Node_NodeAddress._AddressType);
		String addrStatus = nodeAddrAdaptation
			.getString(OrganizationPaths._Root_Organization_Node_NodeAddress._AddressStatus);
		String orgNodeId = (String) nodeAddrAdaptation
			.getString(OrganizationPaths._Root_Organization_Node_NodeAddress._OrgNode);
		Adaptation addrTypeAdaptation = AdaptationUtil.followFK(
			nodeAddrAdaptation,
			OrganizationPaths._Root_Organization_Node_NodeAddress._AddressType);
		String allowMultipleFlag = addrTypeAdaptation
			.getString(CommonReferencePaths._Root_Contact_Types_ADDRESS_TYPE._AllowMultiple);

		if (orgNodeId != null && !"Y".equals(allowMultipleFlag))
		{
		
			AdaptationTable nodeAddrAdaptationTable = nodeAddrAdaptation.getContainer()
				.getTable(OrganizationPaths._Root_Organization_Node_NodeAddress.getPathInSchema());

			String predicate = OrganizationPaths._Root_Organization_Node_NodeAddress._OrgNode
				.format()
				+ " = '" + orgNodeId + "' and "
				+ OrganizationPaths._Root_Organization_Node_NodeAddress._AddressType
					.format()
				+ " = '" + addrType + "'" + " and "
				+ OrganizationPaths._Root_Organization_Node_NodeAddress._AddressStatus
					.format()
				+ " = '" + addrStatus + "'";

			RequestResult reqRes = nodeAddrAdaptationTable.createRequestResult(predicate);
			if ((reqRes != null) && (reqRes.isSizeGreaterOrEqual(1)))
			{
				Adaptation newnodeAddrAdaptation = null;

				try
				{
					while ((newnodeAddrAdaptation = reqRes.nextAdaptation()) != null)
					{
						if (!nodeAddrAdaptation.equalsToAdaptation(newnodeAddrAdaptation))

							updateAddrStatus(
								nodeAddrContext,
								newnodeAddrAdaptation,
								ResponsibleStatuses.INACTIVE);
					}
				}

				finally
				{
					reqRes.close();
				}
			}

		}
	}


	private void updateAddrStatus(
		ProcedureContext nodeAddrContext,
		Adaptation newnodeAddrAdaptation,
		String addrStatus)
		throws OperationException
	{
		HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Node_NodeAddress._AddressStatus,
			addrStatus);
		ModifyValuesProcedure
			.execute(nodeAddrContext, newnodeAddrAdaptation, pathValueMap, true, true);
	}


	}


package com.aramark.mdm.orgn.trigger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.aramark.mdm.common.constants.AramarkOrganizationConstants;
import com.aramark.mdm.common.enums.YesNoFlags;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.PrimaryKey;
import com.orchestranetworks.ps.procedure.ModifyValuesProcedure;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.trigger.AfterCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.AfterModifyOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.schema.trigger.ValueChange;
import com.orchestranetworks.schema.trigger.ValueChanges;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.ProcedureContext;

public class LocationAttributesTableTrigger extends BaseTableTrigger {

	@Override
	public void setup(TriggerSetupContext context) {
		super.setup(context);
	}

	public void handleNewContext(NewTransientOccurrenceContext context) {
		/*ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		String orgNodeId = (String) vc
				.getValue(OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._ORG_NODE_ID);
		if (orgNodeId != null) {
			Adaptation orgNode = AdaptationUtil.followFK(vc,
					OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._ORG_NODE_ID);
			if (orgNode != null) {
		
				// default values for Bill code, concept code, tax code,
				// inter-company code, future code, event type, service type
				Adaptation chartOfAcctAdaptation = AdaptationUtil.followFK(orgNode,
						OrganizationPaths._Root_Organization_ORG_NODE.__chartOfAccount);
		
				if (chartOfAcctAdaptation != null) {
					try {
						String chartOfAccount = chartOfAcctAdaptation.getOccurrencePrimaryKey().format();
						String countryCode = orgNode
								.getString(OrganizationPaths._Root_Organization_ORG_NODE._COUNTRY_CD);
		
						AdaptationHome wipMasterDataSpace;
		
						wipMasterDataSpace = AdaptationUtil.getDataSpaceOrThrowOperationException(
								context.getAdaptationHome().getRepository(), "WorkInProgressMasterDataSpace");
		
						Adaptation wipCOADataset = AdaptationUtil
								.getDataSetOrThrowOperationException(wipMasterDataSpace, "WIPCOADataSet");
		
						DefaultCodes(WipCoaPaths._Root_BILL_CODE_CHART.getPathInSchema(), Path.parse("./BILL_CD"), "0",
								OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._BILL_CD_ID,
								chartOfAccount, countryCode, wipCOADataset, vc);
		
						DefaultCodes(WipCoaPaths._Root_CONCEPT_CHART.getPathInSchema(), Path.parse("./CONCEPT_CD"),
								"0000",
								OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._CONCEPT_CD_ID,
								chartOfAccount, countryCode, wipCOADataset, vc);
		
						DefaultCodes(WipCoaPaths._Root_TAX_CODE_CHART.getPathInSchema(), Path.parse("./TAX_CD"), "00",
								OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._TAX_CD_ID,
								chartOfAccount, countryCode, wipCOADataset, vc);
		
						DefaultCodes(WipCoaPaths._Root_INTERCOMPANY_CODE_CHART.getPathInSchema(),
								Path.parse("./INTERCOMPANY_CD"), "0000",
								OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._INTER_COMPANY_CD_ID,
								chartOfAccount, countryCode, wipCOADataset, vc);
		
						DefaultCodes(WipCoaPaths._Root_FUTURE_CODE_CHART.getPathInSchema(), Path.parse("./FUTURE_CD"),
								"00000",
								OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._FUTURE_CD_ID,
								chartOfAccount, countryCode, wipCOADataset, vc);
		
						DefaultCodes(WipCoaPaths._Root_EVENT_CODE_CHART.getPathInSchema(), Path.parse("./EVENT_CD"),
								"000", OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._EVENT_TYPE_ID,
								chartOfAccount, countryCode, wipCOADataset, vc);
		
						DefaultCodes(WipCoaPaths._Root_SERVICE_TYPE_CHART.getPathInSchema(),
								Path.parse("./SERVICE_TYPE_CD"), "000",
								OrganizationPaths._Root_Organization_Location_LOCATION_ATTRIBUTES._SERVICE_TYPE,
								chartOfAccount, countryCode, wipCOADataset, vc);
		
					} catch (OperationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
		
			}
		}
		super.handleNewContext(context);*/
	}

	@Override
	public void handleAfterCreate(AfterCreateOccurrenceContext context) throws OperationException {
		ProcedureContext locAttribContext = context.getProcedureContext();
		Adaptation locAttribAdaptation = context.getAdaptationOccurrence();
		defaultPrimaryFlagforInitLocOfPrCtr(locAttribAdaptation, locAttribContext);
		// if the location was marked as primary, look for other locations under
		// the parent profit center and
		// insure they are all Primary location flag = "N"
		setNewPrimaryLocation(locAttribAdaptation, locAttribContext);
	}

	@Override
	public void handleAfterModify(AfterModifyOccurrenceContext context) throws OperationException {
		ProcedureContext locAttribContext = context.getProcedureContext();
		Adaptation locAttribAdaptation = context.getAdaptationOccurrence();
		ValueChanges valueChanges = context.getChanges();
		ValueChange primaryFlagValueChange = valueChanges
			.getChange(OrganizationPaths._Root_Organization_Location_LocationAttributes._IsPrimary);
		// How can we determine if the primaryLocation flag has been changed to
		// "Y"?
		// if the location was marked as primary, look for other locations under
		// the parent profit center and
		// insure they are all Primary location flag = "N"
		if (primaryFlagValueChange != null) {
			setNewPrimaryLocation(locAttribAdaptation, locAttribContext);
		}
	}

	/*private void DefaultCodes(Path wipCoaPath, Path colName, String defaultCode, Path pkPath, String chartOfAccount,
			String countryCode, Adaptation wipCOADataset, ValueContextForUpdate vc) {
	
		// procedure for Bill Code
		AdaptationTable codeChartTable = wipCOADataset.getTable(wipCoaPath);
		String predicate = Path.parse("./CHART_ID").format() + " = '" + chartOfAccount + "' and "
				+ Path.parse("./COUNTRY_CD").format() + " = '" + countryCode + "' and " + colName.format() + " = '"
				+ defaultCode + "'";
	
		RequestResult cdRequestResult = codeChartTable.createRequestResult(predicate);
		if (cdRequestResult != null) {
			try {
				Adaptation codeChart = null;
				if ((codeChart = cdRequestResult.nextAdaptation()) != null) {
					vc.setValue(codeChart.getOccurrencePrimaryKey().format(), pkPath);
				}
			} finally {
				cdRequestResult.close();
			}
		}
	}*/

	private void setNewPrimaryLocation(Adaptation locAttribAdaptation, ProcedureContext locAttribContext)
			throws OperationException {
		String primaryLocFlg = locAttribAdaptation
			.getString(OrganizationPaths._Root_Organization_Location_LocationAttributes._IsPrimary);
		if (YesNoFlags.YES.equals(primaryLocFlg)) {

			PrimaryKey newPrimLocOrgNodeId = PrimaryKey
					.parseString(locAttribAdaptation.getOccurrencePrimaryKey().format());
			List<Adaptation> childLocHierarchies = getLocationsForProfitCenter(locAttribAdaptation);
			for (Adaptation childlocHierarchy : childLocHierarchies) {
				Adaptation childOrgNode = AdaptationUtil.followFK(childlocHierarchy,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode);
				if (childOrgNode != null) {
					List<Adaptation> childLocAttrs = AdaptationUtil.getLinkedRecordList(childOrgNode,
						OrganizationPaths._Root_Organization_OrgNode._LocationAttributes);
					if (childLocAttrs != null && !childLocAttrs.isEmpty()) {
						// there should be only one location per org node.
						Adaptation childLocAttr = childLocAttrs.get(0);
						PrimaryKey childOrgNodeId = PrimaryKey
								.parseString(childLocAttr.getOccurrencePrimaryKey().format());
						// if the location being modified is a primary location
						// = "Y" then the parent profit center cannot be
						// changed.
						if ((YesNoFlags.YES.equals(childLocAttr.getString(
								OrganizationPaths._Root_Organization_Location_LocationAttributes._IsPrimary))
								&& (!newPrimLocOrgNodeId.equalsToPrimaryKey(childOrgNodeId)))) {
							updatePrimaryFlag(locAttribContext, childLocAttr, YesNoFlags.NO);
						}
					}
				}
			}
		}
	}

	private void defaultPrimaryFlagforInitLocOfPrCtr(Adaptation locAttribAdaptation, ProcedureContext locAttribContext)
			throws OperationException {
		String primaryLocFlg = locAttribAdaptation
			.getString(OrganizationPaths._Root_Organization_Location_LocationAttributes._IsPrimary);
		Adaptation priLocOrgNodeAdaptation = AdaptationUtil.followFK(locAttribAdaptation,
			OrganizationPaths._Root_Organization_Location_LocationAttributes._OrgNode);
		// dafault the primary location flag to Yes since this is the first
		// location.
		List<Adaptation> childLocHierarchies = getLocationsForProfitCenter(locAttribAdaptation);
		if (childLocHierarchies != null && !childLocHierarchies.isEmpty() && childLocHierarchies.size() == 1) {
			Adaptation childlocHierarchy = childLocHierarchies.get(0);
			Adaptation childOrgNode = AdaptationUtil.followFK(childlocHierarchy,
				OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode);
			if (childOrgNode != null && childOrgNode.getOccurrencePrimaryKey().format()
					.equals(priLocOrgNodeAdaptation.getOccurrencePrimaryKey().format())) {
				updatePrimaryFlag(locAttribContext, locAttribAdaptation, YesNoFlags.YES);
			}
		}
	}

	private List<Adaptation> getLocationsForProfitCenter(Adaptation locAttribAdaptation) {
		List<Adaptation> childLocHierarchies = new ArrayList<Adaptation>();
		Adaptation priLocOrgNodeAdaptation = AdaptationUtil.followFK(locAttribAdaptation,
			OrganizationPaths._Root_Organization_Location_LocationAttributes._OrgNode);
		Adaptation prfCtrOrgNodeAdaptation = AdaptationUtil.followFK(priLocOrgNodeAdaptation,
			OrganizationPaths._Root_Organization_OrgNode.__parentPCOrgNode);
		List<Adaptation> parentHierarchies = AdaptationUtil.getLinkedRecordList(prfCtrOrgNodeAdaptation,
			OrganizationPaths._Root_Organization_OrgNode._OrgHierarchy);
		if (parentHierarchies != null && !parentHierarchies.isEmpty()) {
			Adaptation prfCtrOrgHierarchy = null;
			for (Adaptation orgHierarchy : parentHierarchies) {
				String hierarchyType = orgHierarchy
					.getString(OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
				if (hierarchyType != null
						&& AramarkOrganizationConstants.PROFIT_CENTER_IN_SITE_HIER.equals(hierarchyType)) {
					prfCtrOrgHierarchy = orgHierarchy;
				}
			}
			if (prfCtrOrgHierarchy != null) {
				childLocHierarchies = AdaptationUtil.getLinkedRecordList(prfCtrOrgHierarchy,
					OrganizationPaths._Root_Organization_OrgHierarchy._OrgChildHierarchies);
			}
		}
		return childLocHierarchies;
	}

	private void updatePrimaryFlag(ProcedureContext pContext, Adaptation locAttrAdaptation, String primaryFlag)
			throws OperationException {
		HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();
		pathValueMap.put(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._IsPrimary,
			primaryFlag);
		ModifyValuesProcedure.execute(pContext, locAttrAdaptation, pathValueMap, true, true);
	}

}

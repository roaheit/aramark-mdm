package com.aramark.mdm.orgn.trigger;

import com.aramark.mdm.common.enums.YesNoFlags;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.ps.trigger.BaseTableTrigger;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.trigger.BeforeCreateOccurrenceContext;
import com.orchestranetworks.schema.trigger.NewTransientOccurrenceContext;
import com.orchestranetworks.schema.trigger.TriggerSetupContext;
import com.orchestranetworks.service.LoggingCategory;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.ValueContextForUpdate;

public class PCAttributesTableTrigger extends BaseTableTrigger
{

	@Override
	public void setup(TriggerSetupContext context) {
		super.setup(context);
	}

	@Override
	public void handleNewContext(NewTransientOccurrenceContext context)
	{

		ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		Session session = context.getSession();
		try
		{
			getServiceType(vc, session);
			getOffsetServiceType(vc, session);

		}
		catch (Exception e)
		{
			LoggingCategory.getKernel().info(e.getMessage());
		}


	}

	@Override
	public void handleBeforeCreate(BeforeCreateOccurrenceContext context) throws OperationException
	{
		//super.handleBeforeCreate(context);
		ValueContextForUpdate vc = context.getOccurrenceContextForUpdate();
		Session session = context.getSession();
		try
		{
			getServiceType(vc, session);
			getOffsetServiceType(vc, session);

		}
		catch (Exception e)
		{
			LoggingCategory.getKernel().info(e.getMessage());
		}
	}

	private void getServiceType(ValueContextForUpdate vc, Session session)
		throws OperationException
	{
		String orgNodeId = (String) vc.getValue(
			OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes._OrgNode);
		if (orgNodeId != null)
		{
			Adaptation orgNode = AdaptationUtil.followFK(
				vc,
				OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes._OrgNode);

			if (orgNode != null)
			{

				AdaptationTable svcTypeAdaptationTable = orgNode.getContainer().getTable(
					OrganizationPaths._Root_Organization_OrgServiceTypeAssignment
						.getPathInSchema());
				String predicate = OrganizationPaths._Root_Organization_OrgServiceTypeAssignment._OrgNode
					.format() + " = '" + orgNodeId + "' and "
					+ OrganizationPaths._Root_Organization_OrgServiceTypeAssignment._IsDefault
						.format()
					+ " = '" + YesNoFlags.YES + "'";
				RequestResult reqRes = svcTypeAdaptationTable.createRequestResult(predicate);
				if ((reqRes != null) && (reqRes.isSizeGreaterOrEqual(1)))
				{
					Adaptation svcCodeAdaptation = null;

					try
					{
						while ((svcCodeAdaptation = reqRes.nextAdaptation()) != null)
						{
							if (orgNode.getOccurrencePrimaryKey().format().equals(
								svcCodeAdaptation.getOccurrencePrimaryKey().format()))
							{

								String svcTypeCode = svcCodeAdaptation.getString(
									OrganizationPaths._Root_Organization_OrgServiceTypeAssignment._ServiceNode);
								vc.setValue(
									svcTypeCode,
									OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes._DefaultServiceTypeSN);

							}
						}
					}

					finally
					{
						reqRes.close();
					}
				}

			}
				
		}

	}//end of getServiceType method

	private void getOffsetServiceType(ValueContextForUpdate vc, Session session)
		throws OperationException
	{
		String orgNodeId = null;
		String orgNodeValue = (String) vc.getValue(
			OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes._RateAttributes_SbcOffsetPC);
		if (orgNodeValue != null)
		{

			Adaptation orgAdaptation = vc.getAdaptationInstance();
			AdaptationTable orgAdaptationTable = orgAdaptation
				.getTable(OrganizationPaths._Root_Organization_OrgNode.getPathInSchema());
			String predicate = OrganizationPaths._Root_Organization_OrgNode._OrgNodeOrgValue
				.format() + " = '" + orgNodeValue + "'";
			RequestResult reqRes = orgAdaptationTable.createRequestResult(predicate);
			if ((reqRes != null) && (!reqRes.isEmpty()))
			{
				Adaptation orgNodeAdaptation = null;
				try
				{
					while ((orgNodeAdaptation = reqRes.nextAdaptation()) != null)
					{

						orgNodeId = Integer.toString(
							orgNodeAdaptation.get_int(
								OrganizationPaths._Root_Organization_OrgNode._OrgNodeID));
						if (orgNodeId != null)
						{
							setOffsetServiceType(vc, session, orgNodeId);
						}
					}
				}

				finally
				{
					reqRes.close();
				}
			}

		}

	}//end of getOffsetServiceType method

	private void setOffsetServiceType(ValueContextForUpdate vc, Session session, String orgNodeId)
	{
		Adaptation svcTypeAdaptation = vc.getAdaptationInstance();
		AdaptationTable svcTypeAdaptationTable = svcTypeAdaptation.getTable(
			OrganizationPaths._Root_Organization_OrgServiceTypeAssignment
						.getPathInSchema());
		String predicate = OrganizationPaths._Root_Organization_OrgServiceTypeAssignment._OrgNode
					.format() + " = '" + orgNodeId + "' and "
			+ OrganizationPaths._Root_Organization_OrgServiceTypeAssignment._IsDefault
						.format()
					+ " = '" + YesNoFlags.YES + "'";
		RequestResult reqRes = svcTypeAdaptationTable.createRequestResult(predicate);
		if ((reqRes != null) && (reqRes.isSizeGreaterOrEqual(1)))
				{
					Adaptation svcCodeAdaptation = null;

					try
					{
				while ((svcCodeAdaptation = reqRes.nextAdaptation()) != null)
						{

								String svcTypeCode = svcCodeAdaptation.getString(
						OrganizationPaths._Root_Organization_OrgServiceTypeAssignment._ServiceNode);
								vc.setValue(
									svcTypeCode,
						OrganizationPaths._Root_Organization_ProfitCenter_PCAttributes._RateAttributes_SbcOffsetServTypeSN);
						}
					}

					finally
					{
				reqRes.close();
					}
				}
	}//end of setOffsetServiceType


	}


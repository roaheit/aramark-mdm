package com.aramark.mdm.orgn.constraint;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.time.*;

import com.orchestranetworks.instance.*;
import com.orchestranetworks.schema.*;

/**
 * Constraint that can be used on a date or dateTime field to ensure that the value of the date
 * is not in the past.
 */
public class DateNotInPastConstraint implements Constraint
{
	private static final String MESSAGE = "Date must not be in past.";

	@Override
	public void checkOccurrence(Object value, ValueContextForValidation valueContext)
		throws InvalidSchemaException
	{
		if (value != null)
		{
			
			Date valueDate = (Date) value; //TODO : add type check
			
			Calendar cal = Calendar.getInstance();
			SchemaTypeName type = valueContext.getNode().getXsTypeName();
			if (SchemaTypeName.XS_DATE.equals(type))
			{
				cal = DateUtils.truncate(cal, Calendar.DATE);
			}
			Date now = cal.getTime();
			if (valueDate.before(now))
			{
				valueContext.addError(MESSAGE);
			}
		}
	}
	
	
	@Override
	public void setup(ConstraintContext context)
	{
		SchemaTypeName type = context.getSchemaNode().getXsTypeName();
		if (!SchemaTypeName.XS_DATE.equals(type) || SchemaTypeName.XS_DATETIME.equals(type))
		{
			context.addError("Node must be of type Date or DateTime.");
		}
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext valueContext)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}
}

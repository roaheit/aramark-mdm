package com.aramark.mdm.orgn.constraint;

import java.util.Locale;

import com.aramark.mdm.common.enums.ResponsibleStatuses;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.constraint.BaseConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class NodeResponsibleUniqueRespType extends BaseConstraintOnTableWithRecordLevelCheck
{

	private static String MESSAGE = "There should be atleast one active responsible per responsible type.";

	@Override
	public void setup(ConstraintContextOnTable context)
	{
		SchemaNode respType = context.getSchemaNode().getNode(
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType);

		context.addDependencyToInsertDeleteAndModify(respType);
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}

	@Override
	protected String checkValueContext(ValueContext recordContext)
	{
		Adaptation nodeRespAdaptation = AdaptationUtil.getRecordForValueContext(recordContext);
		if (nodeRespAdaptation == null) //need only check this when the record exists
			return null;

		String respType = (String) recordContext.getValue(
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType);
		String respStatus = (String) recordContext.getValue(
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus);
		String orgNodeId = (String) recordContext
			.getValue(OrganizationPaths._Root_Organization_Node_NodeResponsible._OrgNode);
		if (respStatus.equals(ResponsibleStatuses.INACTIVE))
		{
			if (orgNodeId != null)
			{

				AdaptationTable nodeRespTable = nodeRespAdaptation.getContainer().getTable(
					OrganizationPaths._Root_Organization_Node_NodeResponsible.getPathInSchema());

				String predicate = OrganizationPaths._Root_Organization_Node_NodeResponsible._OrgNode
					.format()
					+ " = '" + orgNodeId + "' and "
					+ OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType
						.format()
					+ " = '" + respType + "'" + " and "
					+ OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus
						.format()
					+ " = '" + ResponsibleStatuses.ACTIVE + "'";

				RequestResult reqRes = nodeRespTable.createRequestResult(predicate);
				if (reqRes.isEmpty())
				{
					reqRes.close();
					return MESSAGE;
				}

				reqRes.close();
			}

		}

		return null;
	}

}

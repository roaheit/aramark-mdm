package com.aramark.mdm.orgn.constraint;

import java.util.Locale;

import com.aramark.mdm.common.enums.SubLocationTypes;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.constraint.BaseConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class SubLocationAttribTypeBrandConstraint extends BaseConstraintOnTableWithRecordLevelCheck
{
	private static String MESSAGE = "Brand is required if Sub Location Type is Consumer Outlet.  Brand cannot be specified for Sub Location Types other than Consumer Outlet.";

	@Override
	public void setup(ConstraintContextOnTable context) {
		SchemaNode brand = context.getSchemaNode().getNode(
			OrganizationPaths._Root_Organization_SubLocation_SubLocationAttributes._Brand);

		context.addDependencyToInsertDeleteAndModify(brand);
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context) throws InvalidSchemaException {
		return MESSAGE;
	}

	@Override
	protected String checkValueContext(ValueContext recordContext) {

		String subLocationType = (String) recordContext.getValue(
			OrganizationPaths._Root_Organization_SubLocation_SubLocationAttributes._SubLocationType);
		String brand = (String) recordContext.getValue(
			OrganizationPaths._Root_Organization_SubLocation_SubLocationAttributes._Brand);

		if (subLocationType != null && !subLocationType.equals(SubLocationTypes.CONSUMER_OUTLET)
			&& brand != null)
		{
			return MESSAGE;
		}
		if (subLocationType != null && subLocationType.equals(SubLocationTypes.CONSUMER_OUTLET))
		{
			if (brand == null || brand.trim().equals(""))
			{
				return MESSAGE;
			}
		}
		return null;
	}
}

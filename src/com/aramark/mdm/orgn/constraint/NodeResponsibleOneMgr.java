package com.aramark.mdm.orgn.constraint;

import java.util.Locale;

import com.aramark.mdm.common.enums.OrgNodeTypes;
import com.aramark.mdm.common.enums.ResponsibleStatuses;
import com.aramark.mdm.common.enums.ResponsibleTypes;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.constraint.BaseConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class NodeResponsibleOneMgr extends BaseConstraintOnTableWithRecordLevelCheck
{

	private static String MESSAGE = "Only one responsibility of manager is allowed for org node of type location or site.";

	@Override
	public void setup(ConstraintContextOnTable context) {
		SchemaNode responsibleType = context.getSchemaNode().getNode(
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType);

		context.addDependencyToInsertDeleteAndModify(responsibleType);
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context) throws InvalidSchemaException {
		return MESSAGE;
	}

	@Override
	protected String checkValueContext(ValueContext recordContext) {
		Adaptation nodeResponsibleAdaptation = AdaptationUtil
			.getRecordForValueContext(recordContext);
		if (nodeResponsibleAdaptation == null) //need only check this when the record exists
			return null;

		String responsibilityType = (String) recordContext.getValue(
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType);
		String responsibilityStatus = (String) recordContext.getValue(
			OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus);

		// if an active manager has been designated make sure there is only one active manager for this org node
		if ((responsibilityType != null) && (responsibilityType.equals(ResponsibleTypes.MANAGER))
			&& (responsibilityStatus != null)
			&& (responsibilityStatus.equals(ResponsibleStatuses.ACTIVE)))
		{
			Adaptation orgNodeAdaptation = AdaptationUtil.followFK(
				nodeResponsibleAdaptation,
				OrganizationPaths._Root_Organization_Node_NodeResponsible._OrgNode);
			//  if this is an orphaned node responsible then skip the constraint validation. 
			if (orgNodeAdaptation != null)
			{
				String orgNodeType = orgNodeAdaptation
					.getString(OrganizationPaths._Root_Organization_OrgNode._OrgNodeType);
				if (orgNodeType != null && (OrgNodeTypes.SITE.equals(orgNodeType)
					|| OrgNodeTypes.LOCATION.equals(orgNodeType)))
				{

					String orgNodeId = (String) recordContext.getValue(
						OrganizationPaths._Root_Organization_Node_NodeResponsible._OrgNode);

					AdaptationTable nodeResponsibleTable = nodeResponsibleAdaptation.getContainer()
						.getTable(
							OrganizationPaths._Root_Organization_Node_NodeResponsible
								.getPathInSchema());

					String predicate = OrganizationPaths._Root_Organization_Node_NodeResponsible._OrgNode
						.format() + " = '" + orgNodeId
						+ "' and "
						+ OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleType
							.format()
						+ " = '" + ResponsibleTypes.MANAGER + "'" + " and "
						+ OrganizationPaths._Root_Organization_Node_NodeResponsible._ResponsibleStatus
							.format()
						+ " = '" + ResponsibleStatuses.ACTIVE + "'";

					RequestResult reqRes = nodeResponsibleTable.createRequestResult(predicate);
					if ((reqRes != null) && (reqRes.isSizeGreaterOrEqual(2)))
					{
						return MESSAGE;
					}
					reqRes.close();
				}
			}
		}
		return null;
	}
}

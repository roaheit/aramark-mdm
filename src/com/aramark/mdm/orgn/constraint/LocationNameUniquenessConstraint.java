package com.aramark.mdm.orgn.constraint;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.aramark.mdm.common.constants.AramarkOrganizationConstants;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.PrimaryKey;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.constraint.BaseConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class LocationNameUniquenessConstraint
	extends
	BaseConstraintOnTableWithRecordLevelCheck
{

	private static String MESSAGE = "Name (Location name) must be unique within a profit center.  Go to the basic info tab and to modify the name";

	@Override
	public void setup(ConstraintContextOnTable context)
	{
		SchemaNode node = context.getSchemaNode().getNode(
			OrganizationPaths._Root_Organization_Location_LocationAttributes.__orgName);
		context.addDependencyToInsertDeleteAndModify(node);
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}

	@Override
	protected String checkValueContext(ValueContext recordContext)
	{
		Adaptation locAttrRecord = AdaptationUtil.getRecordForValueContext(recordContext);
		if (locAttrRecord == null) //need only check this when the record exists
			return null;

		String orgNodeId = locAttrRecord.getOccurrencePrimaryKey().format();
		String locationName = (String) recordContext.getValue(
			OrganizationPaths._Root_Organization_Location_LocationAttributes.__orgName);

		AdaptationTable orgHierarchyTable = locAttrRecord.getContainer()
			.getTable(OrganizationPaths._Root_Organization_OrgHierarchy.getPathInSchema());

		String predicate = OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode.format()
			+ " = '" + orgNodeId + "' and "
			+ OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType.format()
			+ " = '" + AramarkOrganizationConstants.LOCATION_IN_SITE_HIER + "'";

		RequestResult reqRes = orgHierarchyTable.createRequestResult(predicate);

		if (reqRes != null)
		{
			try
			{
				// the assumption is, in the Org Hierarchy table, the combination is Org Hierarchy Type and Org Node is unique
				Adaptation orgHierarchy = reqRes.nextAdaptation();
				if (orgHierarchy != null)
				{
					Adaptation orgParentHierarchy = AdaptationUtil.followFK(
						orgHierarchy,
						OrganizationPaths._Root_Organization_OrgHierarchy._ParentOrgHierarchy);

					// orgParentHierarchy should never be null for Location
					// this will be the Profit Center hierarchy record for the Location record
					if (orgParentHierarchy != null)
					{
						// get the child hierarchies for the parent hierarchy - this will return all the Locations for that Profit Center
						List<Adaptation> childHierarchies = AdaptationUtil.getLinkedRecordList(
							orgParentHierarchy,
							OrganizationPaths._Root_Organization_OrgHierarchy._OrgChildHierarchies);

						List<Adaptation> orgNodes = new ArrayList<Adaptation>();
						// For each Location (child hierarchy) get the corresponding OrgNode, which will be the Location Org Node
						for (Adaptation childHierarchy : childHierarchies)
						{
							/*orgNodes.add(
								AdaptationUtil.followFK(
									childHierarchy,
									OrganizationPaths._Root_Organization_ORG_HIERARCHY._ORG_NODE_ID));*/

							Adaptation childOfPrCtrOrgNode = AdaptationUtil.followFK(
								childHierarchy,
								OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode);
							if (childOfPrCtrOrgNode != null)
							{
								orgNodes.add(childOfPrCtrOrgNode);
							}
						}

						List<Adaptation> locationAttributes = new ArrayList<Adaptation>();
						// For each Location Org Node, get the corresponding Location Attribute  
						// this will return a list of Location Attributes for all Locations within its Profit Center
						//  beware that not every org node of type location will have an location attribute row.
						for (Adaptation orgNode : orgNodes)
						{
							PrimaryKey org_node_id = PrimaryKey.parseString(orgNode.getOccurrencePrimaryKey().format()); 
							Adaptation locRecordforPK = locAttrRecord.getContainerTable().lookupAdaptationByPrimaryKey(org_node_id); 
							if (locRecordforPK != null)   
							{
								locationAttributes.add(locRecordforPK);
							}
						/*	locationAttributes.add(
								locAttrRecord.getContainerTable().lookupAdaptationByPrimaryKey(
									PrimaryKey
										.parseString(orgNode.getOccurrencePrimaryKey().format())));   */
						}
						// Iterate through the Location Attributes and check if any other Location Attribute (other than the current record) has the same Short Name 
						for (Adaptation locationAttribute : locationAttributes)
						{
							if (locationName != null
								&& locationName.toLowerCase().equals(
									locationAttribute.getString(
										OrganizationPaths._Root_Organization_Location_LocationAttributes.__orgName)
										.toLowerCase())
								&& !locAttrRecord.equals(locationAttribute))
							{
								return MESSAGE;
							}
						}
					}
				}
			}
			finally
			{
				reqRes.close();
			}
		}
		return null;
	}
}

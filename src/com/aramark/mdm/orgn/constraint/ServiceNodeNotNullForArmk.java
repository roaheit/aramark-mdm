package com.aramark.mdm.orgn.constraint;

import java.util.Locale;

import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.instance.ValueContextForValidation;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.Constraint;
import com.orchestranetworks.schema.ConstraintContext;
import com.orchestranetworks.schema.ConstraintOnNull;
import com.orchestranetworks.schema.InvalidSchemaException;

public class ServiceNodeNotNullForArmk implements ConstraintOnNull, Constraint
{
	private static String MESSAGE = "Service Type is required if Location Type is Aramark (ARMK)";
	String test="";
		
	@Override
	public void checkNull(ValueContextForValidation context) throws InvalidSchemaException
	{
		// Operating Company is required if Vendor has Operating Companies
		Adaptation record = AdaptationUtil.getRecordForValueContext(context);
		String locationTypeCode = record.getString(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._LocationType);
		String srvcTypeCode = record.getString(
			OrganizationPaths._Root_Organization_Location_LocationAttributes._ServiceType);
		
		if (!locationTypeCode.equals("ARMK"))
		{
			return;
		}

		if (locationTypeCode.equals("ARMK"))
		{ 	if (srvcTypeCode==null || srvcTypeCode.trim().equals(""))
			{
				context.addError(MESSAGE);
			}
		}
	}

	@Override
	public void checkOccurrence(Object arg0, ValueContextForValidation arg1)
		throws InvalidSchemaException
	{
		// nothing to do
	}

	@Override
	public void setup(ConstraintContext aContext)
	{
		// nothing to do

	}

	@Override
	public String toUserDocumentation(Locale userLocale, ValueContext aContext)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}

}

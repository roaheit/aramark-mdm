package com.aramark.mdm.orgn.constraint;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang.time.*;

import com.orchestranetworks.instance.*;
import com.orchestranetworks.schema.*;



public class AddressLineOneNotNullConstraint implements Constraint
{
	private static final String MESSAGE = "ADDRESS LINE ONE is required.";

	@Override
	public void checkOccurrence(Object value, ValueContextForValidation valueContext) throws InvalidSchemaException
	{
		if (value == null)
		{
			valueContext.addError(MESSAGE);
		}
	}
	
	
	@Override
	public void setup(ConstraintContext context)
	{
		SchemaTypeName type = context.getSchemaNode().getXsTypeName();
		if (!SchemaTypeName.XS_STRING.equals(type))
		{
			context.addError("Node must be of type String.");
		}
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext valueContext)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}
}

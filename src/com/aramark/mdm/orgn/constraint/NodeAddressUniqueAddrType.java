package com.aramark.mdm.orgn.constraint;

import java.util.Locale;

import com.aramark.mdm.common.enums.ResponsibleStatuses;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.adaptation.RequestResult;
import com.orchestranetworks.instance.ValueContext;
import com.orchestranetworks.ps.constraint.BaseConstraintOnTableWithRecordLevelCheck;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.ConstraintContextOnTable;
import com.orchestranetworks.schema.InvalidSchemaException;
import com.orchestranetworks.schema.SchemaNode;

public class NodeAddressUniqueAddrType extends BaseConstraintOnTableWithRecordLevelCheck
{

	private static String MESSAGE = "There should be atleast one active address per address type.";

	@Override
	public void setup(ConstraintContextOnTable context)
	{
		SchemaNode addrType = context.getSchemaNode().getNode(
			OrganizationPaths._Root_Organization_Node_NodeAddress._AddressType);

		context.addDependencyToInsertDeleteAndModify(addrType);
	}

	@Override
	public String toUserDocumentation(Locale locale, ValueContext context)
		throws InvalidSchemaException
	{
		return MESSAGE;
	}

	@Override
	protected String checkValueContext(ValueContext recordContext)
	{
		Adaptation nodeAddrAdaptation = AdaptationUtil
			.getRecordForValueContext(recordContext);
		if (nodeAddrAdaptation == null) //need only check this when the record exists
			return null;

		String addressType = (String) recordContext
			.getValue(OrganizationPaths._Root_Organization_Node_NodeAddress._AddressType);
		String addressStatus = (String) recordContext
			.getValue(OrganizationPaths._Root_Organization_Node_NodeAddress._AddressStatus);
		String orgNodeId = (String) recordContext
			.getValue(OrganizationPaths._Root_Organization_Node_NodeAddress._OrgNode);
		if (addressStatus.equals(ResponsibleStatuses.INACTIVE))
		{
			if (orgNodeId != null)
				{

				AdaptationTable nodeAddressTable = nodeAddrAdaptation.getContainer()
						.getTable(
						OrganizationPaths._Root_Organization_Node_NodeAddress
								.getPathInSchema());

				String predicate = OrganizationPaths._Root_Organization_Node_NodeAddress._OrgNode
						.format()
						+ " = '" + orgNodeId + "' and "
					+ OrganizationPaths._Root_Organization_Node_NodeAddress._AddressType
							.format()
						+ " = '" + addressType + "'" + " and "
					+ OrganizationPaths._Root_Organization_Node_NodeAddress._AddressStatus
							.format()
					+ " = '" + ResponsibleStatuses.ACTIVE + "'";

				RequestResult reqRes = nodeAddressTable.createRequestResult(predicate);
				if (reqRes.isEmpty())
					{
					reqRes.close();
					return MESSAGE;
					}

				reqRes.close();
			}

		}

		return null;
	}

}

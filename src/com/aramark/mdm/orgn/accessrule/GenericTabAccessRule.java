package com.aramark.mdm.orgn.accessrule;

import com.aramark.mdm.common.type.AccessRuleParms;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ps.constants.CommonConstants;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;

public class GenericTabAccessRule implements AccessRule
{
	protected AccessRuleParms accessRuleParms = null;

	public GenericTabAccessRule()
	{

	}

	public GenericTabAccessRule(AccessRuleParms accessRuleParms)
	{
		this.accessRuleParms = accessRuleParms;
	}

	@Override
	public AccessPermission getPermission(Adaptation aDataSetOrRecord, Session session, SchemaNode aNode) {
		DirectoryHandler directory = session.getDirectory();
		UserReference user = session.getUserReference();
		
		// check if data set is schema instance or user has tech-admin role return read write
		
		if (aDataSetOrRecord.isSchemaInstance() || aDataSetOrRecord.isHistory()
			|| directory.isUserInRole(user, Role.forSpecificRole(CommonConstants.ROLE_TECH_ADMIN)))
		{
			return AccessPermission.getReadWrite();
		}
		// and record is the right org node type
		// if the record's org node type = access rule org node type (check this first) 
		//   if  tracking info is the right workflow type && my user have the update role for that workflow
		//   then -> return read write
		//   else if user has an update role or read role 
		//   then return read only
		//    else return hidden
		//    else to line 42 return read write
		if (aDataSetOrRecord.isTableOccurrence())
		{
			if (session.getInteraction(true) != null && // within the context of a workflow for manage {org_node_type}, Manage District for an example of the possible tracking info	
				session.getTrackingInfo() != null
				&& session.getTrackingInfo().equals(accessRuleParms.getTrackingInfo())) 
			{
			
				String orgNodeTypeId = null;
				orgNodeTypeId = aDataSetOrRecord
						.getString(accessRuleParms.getNodeTypePath());

				
				if (orgNodeTypeId != null
					&& orgNodeTypeId.equals(this.accessRuleParms.getForOrgNodeType())) 						
				{
					// if session.isUserInRole(profile) this is more efficient.  				
					if (session.isUserInRole(accessRuleParms.getRoleRequiredForUpdate()))
					{
						return AccessPermission.getReadWrite();
					}
					//else if (directory.isUserInRole(user, accessRuleParms.getRoleRequiredForRead()))
					else if (session.isUserInRole(accessRuleParms.getRoleRequiredForRead()))
					{
						return AccessPermission.getReadOnly();
					}
					else
						{
						return AccessPermission.getHidden();
						}
				
				}
				else
				{
					// if the user has navigated to an org node type that is outside the type the workflow is intended for, the permissions s/b read only				
					return AccessPermission.getReadOnly();
				}
			}

		} // If conditions above were not met return read write so that this access rule does not impede the authorities that other mechanisms will grant. 	
		return AccessPermission.getReadWrite();
	}
}

package com.aramark.mdm.orgn.accessrule;

import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.service.UserReference;
import com.orchestranetworks.service.directory.DirectoryHandler;

public class ShowMappingTabAccessRule implements AccessRule {

	@Override
	public AccessPermission getPermission(Adaptation aDataSetOrRecord, Session session, SchemaNode aNode) {
		DirectoryHandler directory = session.getDirectory();
		UserReference user = session.getUserReference();
		if (directory.isUserInRole(user, Role.forSpecificRole("ORGN - System Mapping"))) {
			return AccessPermission.getReadWrite();
		} else {
			return AccessPermission.getHidden();
		}
	}

}

package com.aramark.mdm.orgn.accessrule;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Session;
public class OrgNodeTypeAccessRule implements AccessRule {
	
	protected String orgNodeTypePath;
	protected AccessPermission accessPermission;
	
	public OrgNodeTypeAccessRule()
	{
		
	}

	public OrgNodeTypeAccessRule(String orgNodeTypePath)
	{
		this.orgNodeTypePath = orgNodeTypePath;
	}

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node)
	{
		// Will be data set when called for column permissions
		// or a record not yet saved
		if (adaptation.isSchemaInstance() || adaptation.isHistory())
		{
			return AccessPermission.getReadWrite();
		}
		
		return isTabVisbibleReadWrite(adaptation) ? AccessPermission.getReadWrite()
			: AccessPermission.getHidden();
	}

	protected boolean isTabVisbibleReadWrite(Adaptation adaptation )
	{
		if (orgNodeTypePath.equals(
			adaptation.getString(OrganizationPaths._Root_Organization_OrgNode._OrgNodeType)))
		{
			
			return true;
			
		}
		return false;
	}


}

/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.aramark.mdm.orgn.accessrule;

import com.aramark.mdm.orgn.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.service.directory.*;

/**
 */
public class ShowOnlyNodeTypeAccessRule implements AccessRule
{

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node)
	{

		if (adaptation.isSchemaInstance() || adaptation.isHistory())
		{
			DirectoryHandler directory = session.getDirectory();
			UserReference user = session.getUserReference();
			if (directory.isUserInRole(user, Role.forSpecificRole("ORGN - Data Entry"))){
				
				return AccessPermission.getReadWrite();
				
			}
			return AccessPermission.getReadOnly();
		}

		// If the user is within a workflow
		if (session.getInteraction(true) != null)			
		{
			if (session.getTrackingInfo() != null && session.getTrackingInfo().equals("ORGN - Data Entry"))
			{
				
				return AccessPermission.getReadOnly();
				
							
			}
			else{

				return AccessPermission.getReadOnly();
			}
		}

		return AccessPermission.getReadWrite();
	}
}

package com.aramark.mdm.orgn.accessrule;

import com.aramark.mdm.orgn.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.service.directory.*;

public class ShowLocationTabAccessRule implements AccessRule {

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node)
	{

		if (adaptation.isSchemaInstance() || adaptation.isHistory())
		{
			return AccessPermission.getReadWrite();
		}

		// If the user is within a workflow
		if (session.getInteraction(true) != null)
		{
			// If the session user has the orgnDe role
			if (session.getTrackingInfo() != null && session.getTrackingInfo().equals("ORGN-Location-DataEntry"))
			{
				return AccessPermission.getReadWrite();
			}
			return AccessPermission.getHidden();
		}

		return AccessPermission.getReadWrite();
	}
	
}

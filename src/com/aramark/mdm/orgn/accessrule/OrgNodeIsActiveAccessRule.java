package com.aramark.mdm.orgn.accessrule;

import java.util.List;

import com.aramark.mdm.common.enums.YesNoFlags;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Session;

public class OrgNodeIsActiveAccessRule implements AccessRule {

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node) {

		if (adaptation.isSchemaInstance() || adaptation.isHistory()) {
			return AccessPermission.getReadWrite();
		}

		// If the user is within a workflow
		if (session.getInteraction(true) != null) {
			if (session.getTrackingInfo() != null && session.getTrackingInfo().equals("ORGN-Location-DataEntry")) {
				List<Adaptation> locations = AdaptationUtil.getLinkedRecordList(adaptation,
					OrganizationPaths._Root_Organization_OrgNode._LocationAttributes);
				if (locations != null && !locations.isEmpty()) {
					// there should be only one location per org node.
					Adaptation location = locations.get(0);
					// if the location being modified is a primary location =
					// "Y"
					// then the parent profit center cannot be changed.
					if (YesNoFlags.YES.equals(location.getString(
							OrganizationPaths._Root_Organization_Location_LocationAttributes._IsPrimary)))
					{
						return AccessPermission.getReadOnly();
					}
				}
			}
			return AccessPermission.getReadWrite();
		}

		return AccessPermission.getReadWrite();
	}

}

package com.aramark.mdm.orgn.accessrule;

import com.aramark.mdm.common.enums.YesNoFlags;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.SchemaNode;
import com.orchestranetworks.service.AccessPermission;
import com.orchestranetworks.service.AccessRule;
import com.orchestranetworks.service.Session;

public class PrimaryLocationAccessRule implements AccessRule {

	@Override
	public AccessPermission getPermission(Adaptation adaptation, Session session, SchemaNode node) {

		if (adaptation.isSchemaInstance() || adaptation.isHistory()) {
			return AccessPermission.getReadWrite();
		}

		// if primary loc flag is currently true then make it read only or
		// if inactive or deleted make it read only
		Adaptation orgNode = AdaptationUtil.followFK(adaptation,
			OrganizationPaths._Root_Organization_Location_LocationAttributes._OrgNode);
		if (orgNode != null) // sometimes with bulk loads, location_attributes rows exist that don't corresponding have org nodes
		{
			if (YesNoFlags.YES.equals(
				adaptation.getString(
					OrganizationPaths._Root_Organization_Location_LocationAttributes._IsPrimary))
				|| YesNoFlags.NO.equals(
					orgNode.getString(OrganizationPaths._Root_Organization_OrgNode._IsEnabled))
				|| YesNoFlags.YES.equals(
					orgNode.getString(OrganizationPaths._Root_Organization_OrgNode._IsDeleted)))
			{

				return AccessPermission.getReadOnly();
			}
		}
		return AccessPermission.getReadWrite();
	}

}
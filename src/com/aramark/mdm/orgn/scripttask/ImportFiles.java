package com.aramark.mdm.orgn.scripttask;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
// import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.log4j.Logger;

import com.aramark.mdm.config.EBXAramarkEnvironment;
import com.aramark.mdm.job.ImportParameter;
import com.aramark.mdm.util.path.UtilityPaths;
import com.aramark.mdm.geography.GeographyPaths;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.onwbp.adaptation.AdaptationTable;
import com.onwbp.base.schema.definition.XsFormats;
import com.orchestranetworks.instance.HomeCreationSpec;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.ps.constants.CommonConstants;
import com.orchestranetworks.ps.procedure.CreateRecordProcedure;
import com.orchestranetworks.ps.procedure.ProcedureExecutor;
import com.orchestranetworks.schema.Path;
import com.orchestranetworks.schema.PathAccessException;
import com.orchestranetworks.service.ExportImportCSVSpec;
import com.orchestranetworks.service.ExportImportCSVSpec.Header;
import com.orchestranetworks.service.ImportSpec;
import com.orchestranetworks.service.ImportSpecMode;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProcedureResult;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Role;
import com.orchestranetworks.workflow.ProcessInstance;
import com.orchestranetworks.workflow.ProcessInstanceKey;
import com.orchestranetworks.workflow.PublishedProcessKey;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;


public class ImportFiles extends ScriptTask
{
	private String parentDataSpace;
	private String processingDateString; 
	private String childDataSpace;
	private String dataSet; 
	private String loadErrorsEncountered = "N"; 
	private String loadStatus; 
	private String fileHeaderType; 
	
	final static Logger logger = Logger.getLogger(ImportFiles.class);
	
	//Map<String, String> map  = EBXAramarkEnvironment.loadFiles;
	
	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException
	{		

	
		final Repository repository = Repository.getDefault();
		Integer stepNum = 0; 
		//final Adaptation adaptation = context.getAdaptationOccurrence();
		// File import procedure
		parentDataSpace = context.getVariableString("DataSpace");
		childDataSpace = context.getVariableString("ChildDataSpace");
		dataSet = context.getVariableString("DataSet");
		fileHeaderType = context.getVariableString("ImportFileHeaderType").toUpperCase();
		
		
		ProcessInstance pi = context.getProcessInstance();
		PublishedProcessKey ppk =  pi.getPublishProcessKey();
		String publishedProcessKey = ppk.getName();
		
		final ImportSpec importSpecForCSV = new ImportSpec();
		final ExportImportCSVSpec csvSpec = new ExportImportCSVSpec();
		if (fileHeaderType.equals("LABEL"))
		{
			csvSpec.setHeader(Header.LABEL);
		} 
		if (fileHeaderType.equals("XPATH"))
		{
			csvSpec.setHeader(Header.PATH_IN_TABLE);
		} 
		
		csvSpec.setFieldSeparator(',');
		importSpecForCSV.setCSVSpec(csvSpec);
		
		
		// Create a child data space - The import of the file will happen in
		// that dataspace
		childDataSpace = childDataSpace + XsFormats.SINGLETON.formatDateTime(new Date());
		// set the workflow variable value with the actual name of the child data space 
		context.setVariableString("ChildDataSpace", childDataSpace);
		final AdaptationHome parentAdaptationHome = repository.lookupHome(HomeKey.forBranchName(parentDataSpace));
		final HomeCreationSpec homeCreationSpec = new HomeCreationSpec();
		homeCreationSpec.setKey(HomeKey.forBranchName(childDataSpace));
		homeCreationSpec.setOwner(context.getSession().getUserReference());
		homeCreationSpec.setParent(parentAdaptationHome);
		homeCreationSpec.setOwner(Role.forSpecificRole(CommonConstants.ROLE_TECH_ADMIN));
		//
	    // create child dataspace here
		//
		final AdaptationHome workingHome = repository.createHome(
				homeCreationSpec,
				context.getSession()); 
				
		//  Problems creating the child dataspace?  Find them here
		if (workingHome == null)
		{
			throw OperationException.createError("Child DataSpace not found");
		}

		final Adaptation dataSetContainer = workingHome.findAdaptationOrNull(AdaptationName.forName(dataSet));
		if (dataSetContainer == null)
		{
			throw OperationException.createError("Dataset not found");
		}
		//
		//  setup the logging to a Utility data set table called DataLoadLog.  
		//  
		AdaptationHome loggingDataSpace = repository.lookupHome(HomeKey.forBranchName("Utility"));
		Adaptation loggingDataSet = loggingDataSpace.findAdaptationOrNull(AdaptationName.forName("UtilityDataSet"));
		AdaptationTable loggingTable = loggingDataSet.getTable(UtilityPaths._Root_DataLoadLog.getPathInSchema());
		// get the processing date and set up replacement of the date mask in the file name.
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd"); 
		Date date = new Date(); 
		processingDateString = dateFormat.format(date).toString();
				

		List<ImportParameter> importParams =  EBXAramarkEnvironment.importJobs.get(publishedProcessKey);
 
		// iterate through the list of files and table paths
		for (ImportParameter ip : importParams) {
			loadStatus = "Success"; 
			stepNum++; 
			//
		    // purge the dashes out of the date and substitute current date into the file name 	
			// 
			processingDateString = processingDateString.replace("-", "");  
			String fileName = ip.getFileName().replace("YYYYMMDD", processingDateString);  
			logger.debug("loading data file " + fileName + " for path " + ip.getSchemaPath() );
			//
		    // problems with file to be read? 	
			// 
		
			AdaptationTable targetTable = null;
			
	
			try {
				targetTable = dataSetContainer.getTable(Path.parse(ip.getSchemaPath()));
				} catch (PathAccessException e) {
				logger.error("Error in accessing path at " + fileName + " for path " + ip.getSchemaPath() );
			}
					
			final File file = new File(fileName);
			if (!file.exists() || !file.canRead() || !file.isFile())
			{
				loadErrorsEncountered = "Y";
				loadStatus = "Path " + fileName + " is not a readable file"; 
				logger.error("Path " + fileName + " is not a readable file");
				//throw OperationException.createError("Path " + fileName + " is not a readable file");	

			}
			else
			{
	
				//  run the import 						
				importSpecForCSV.setImportMode(ImportSpecMode.UPDATE_OR_INSERT);
				importSpecForCSV.setSourceFile(file);
				importSpecForCSV.setTargetAdaptationTable(targetTable);
										
				final Procedure proc = new Procedure()
				{
					@Override
					public void execute(final ProcedureContext pContext) throws Exception
					{
						pContext.setAllPrivileges(true);
						pContext.setCommitThreshold(2000);
						pContext.setHistoryActivation(false);	
						pContext.doImport(importSpecForCSV);
						pContext.setAllPrivileges(false);
					}
				};
		
				// File Import execution
				final ProgrammaticService srv = ProgrammaticService.createForSession(
					context.getSession(),
					workingHome);
				final ProcedureResult result = srv.execute(proc);
				final OperationException resultException = result.getException();
				if (resultException != null)
				{
					// throw resultException;
					loadStatus = resultException.getMessage(); 
					loadErrorsEncountered = "Y";
				}
				if (result.hasFailed())
				{
					//throw OperationException.createError("File Import execution failed.");
					logger.error("File Import execution failed for" + fileName);
					loadErrorsEncountered = "Y";				
				}
			}
			WriteLogProcedure writeLogProcedure = new WriteLogProcedure(context, loggingTable, fileName, stepNum, loadStatus);
			ProcedureExecutor.executeProcedure(writeLogProcedure, context.getSession(), loggingDataSpace);	
		}
		// update parms to pass on to downstream workflow steps 
		context.setVariableString("LoadErrorsEncountered", loadErrorsEncountered); 
		
		}
	
	public class WriteLogProcedure implements Procedure
	{
		private ScriptTaskContext context;
		private AdaptationTable   loggingTable;
		private Adaptation        logDetails;
		private String            loadFilePathName; 
		private Integer           stepNum; 
		private String			  errMsg; 

		public WriteLogProcedure(ScriptTaskContext context, AdaptationTable loggingTable, String loadFilePathName, Integer stepNum, String errMsg)
		{
			this.context = context;
			this.loggingTable = loggingTable;
			this.loadFilePathName = loadFilePathName; 
			this.stepNum = stepNum; 
			this.errMsg = errMsg; 
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();		
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_WFProcessInstanceID, context.getProcessInstance().getLabel().formatMessage(Locale.getDefault()).toString()); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_StepId, stepNum); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_ParentDataspace, context.getVariableString("DataSpace")); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_ChildDataSpace, context.getVariableString("ChildDataSpace"));			
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_StepDescription, "Source File Load into child dataspace"); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_SourceFilePathName, loadFilePathName);  
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_ErrorMessage, errMsg); 
			// in the status column log 0 = success, 1 = failure
			if (errMsg != "Success") 
			{ pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_Status, 1); }
			else
			{ pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_Status, 0); }
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_TimeStamp, context.getProcessInstance().getCreationDate()); 
			logDetails = CreateRecordProcedure.execute(pContext, loggingTable, pathValueMap, true, true);
		}

		public Adaptation getLogDetails()
		{
			return logDetails;
		}

		public void setLogDetails(Adaptation logDetails)
		{
			this.logDetails = logDetails; 
		}
	}
	
}

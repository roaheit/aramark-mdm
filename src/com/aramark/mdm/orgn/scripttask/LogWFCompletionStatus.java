package com.aramark.mdm.orgn.scripttask;
import java.util.*;


import com.aramark.mdm.util.path.UtilityPaths;
import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.ps.procedure.CreateRecordProcedure;
import com.orchestranetworks.ps.procedure.ProcedureExecutor;
import com.orchestranetworks.schema.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;


public class LogWFCompletionStatus extends ScriptTask
{
	private String parentDataSpace;
	private String childDataSpace;
	private String dataSet; 
	private String overallStatus; 

	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException
	{		
		final Repository repository = Repository.getDefault();
		parentDataSpace = context.getVariableString("DataSpace");
		childDataSpace = context.getVariableString("ChildDataSpace");
		overallStatus = context.getVariableString("OverallStatus");
		dataSet = context.getVariableString("DataSet");		
		//
		//  setup the logging to a Utility data set table called DataLoadLog.  
		//  
		AdaptationHome loggingDataSpace = repository.lookupHome(HomeKey.forBranchName("Utility"));
		Adaptation loggingDataSet = loggingDataSpace.findAdaptationOrNull(AdaptationName.forName("UtilityDataSet"));
		AdaptationTable loggingTable = loggingDataSet.getTable(UtilityPaths._Root_DataLoadLog.getPathInSchema());
	
		WriteLogProcedure writeLogProcedure = new WriteLogProcedure(context, loggingTable, 999, overallStatus);
		ProcedureExecutor.executeProcedure(writeLogProcedure, context.getSession(), loggingDataSpace);	
	}
	
	public class WriteLogProcedure implements Procedure
	{
		private ScriptTaskContext context;
		private AdaptationTable   loggingTable;
		private Adaptation        logDetails;
		private Integer           stepNum; 
		private String			  errMsg; 

		public WriteLogProcedure(ScriptTaskContext context, AdaptationTable loggingTable, Integer stepNum, String errMsg)
		{
			this.context = context;
			this.loggingTable = loggingTable;
			this.stepNum = stepNum; 
			this.errMsg = errMsg; 
		}

		@Override
		public void execute(ProcedureContext pContext) throws Exception
		{
			HashMap<Path, Object> pathValueMap = new HashMap<Path, Object>();		
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_WFProcessInstanceID, context.getProcessInstance().getLabel().formatMessage(Locale.getDefault()).toString()); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_StepId, stepNum); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_ParentDataspace, context.getVariableString("DataSpace")); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_ChildDataSpace, context.getVariableString("ChildDataSpace"));			
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_StepDescription, "overall inbound load status"); 
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_ErrorMessage, errMsg); 
			if (errMsg.equals("Success"))					
			{ pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_Status, 0); }			
			else		
			{ pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_Status, 1); }
			pathValueMap.put(UtilityPaths._Root_DataLoadLog._Root_DataLoadLog_TimeStamp, context.getProcessInstance().getCreationDate()); 
			logDetails = CreateRecordProcedure.execute(pContext, loggingTable, pathValueMap, true, true);
		}

		public Adaptation getLogDetails()
		{
			return logDetails;
		}

		public void setLogDetails(Adaptation logDetails)
		{
			this.logDetails = logDetails; 
		}
	}
}

package com.aramark.mdm.orgn.valuefunction;

import java.util.List;

import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class ChartOfAccountForProfitCenterValueFunction implements ValueFunction {

	@Override
	public Object getValue(Adaptation adaptation) {

		Adaptation profitCenterAdaptation = AdaptationUtil.followFK(adaptation,
			OrganizationPaths._Root_Organization_OrgNode.__parentPCOrgNode);
		if (profitCenterAdaptation != null) 
		{
			List<Adaptation> orgCharts = AdaptationUtil.getLinkedRecordList(
					profitCenterAdaptation,
				OrganizationPaths._Root_Organization_OrgNode._OrgChart);
				if (orgCharts != null && !orgCharts.isEmpty())
				{
					Adaptation orgChart = orgCharts.get(0); 
				return AdaptationUtil
					.followFK(orgChart, OrganizationPaths._Root_Organization_OrgChart._CoaChart)
					.getOccurrencePrimaryKey()
					.format();
				}
		}
		return null;
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub

	}

}

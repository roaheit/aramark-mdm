package com.aramark.mdm.orgn.valuefunction;

import java.util.List;

import com.aramark.mdm.common.constants.AramarkOrganizationConstants;
import com.aramark.mdm.orgn.path.OrganizationPaths;
import com.onwbp.adaptation.Adaptation;
import com.orchestranetworks.ps.util.AdaptationUtil;
import com.orchestranetworks.schema.ValueFunction;
import com.orchestranetworks.schema.ValueFunctionContext;

public class ProfitCenterForLocationOrgNodeValueFunction implements ValueFunction {

	@Override
	public Object getValue(Adaptation adaptation) {

		List<Adaptation> orgHierarchies = AdaptationUtil.getLinkedRecordList(adaptation,
			OrganizationPaths._Root_Organization_OrgNode._OrgHierarchy);
		if (orgHierarchies != null && !orgHierarchies.isEmpty()) {
			Adaptation locationHierarchy = null;
			for (Adaptation orgHierarchy : orgHierarchies) {
				String hierarchyType = orgHierarchy
					.getString(OrganizationPaths._Root_Organization_OrgHierarchy._OrgHierarchyType);
				if (hierarchyType != null
					&& hierarchyType.equals(AramarkOrganizationConstants.LOCATION_IN_SITE_HIER))
				{
					locationHierarchy = orgHierarchy;
				}
			}
			if (locationHierarchy != null) {
				Adaptation profitCenterHierarchy = AdaptationUtil.followFK(locationHierarchy,
					OrganizationPaths._Root_Organization_OrgHierarchy._ParentOrgHierarchy);
				if (profitCenterHierarchy != null) {
					Adaptation profitCenterAdaptation = AdaptationUtil.followFK(profitCenterHierarchy,
						OrganizationPaths._Root_Organization_OrgHierarchy._OrgNode);
					if (profitCenterAdaptation != null) 
					{
						return profitCenterAdaptation.getOccurrencePrimaryKey().format(); 
					}
				}
			}
		}
		return null;
	}

	@Override
	public void setup(ValueFunctionContext arg0) {
		// TODO Auto-generated method stub

	}

}

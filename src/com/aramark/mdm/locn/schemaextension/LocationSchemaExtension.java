/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.aramark.mdm.locn.schemaextension;

import com.aramark.mdm.locn.path.*;
import com.orchestranetworks.ps.accessrule.*;
import com.orchestranetworks.schema.*;

/**
 */
public class LocationSchemaExtension implements SchemaExtensions
{
	@Override
	public void defineExtensions(SchemaExtensionsContext context)
	{
		AccessRulesManager manager = new AccessRulesManager(context);
		manager.setAccessRuleOnNodeAndAllDescendants(
			LocationPaths._Root,
			false,
			new WorkflowAccessRule());
	}
}

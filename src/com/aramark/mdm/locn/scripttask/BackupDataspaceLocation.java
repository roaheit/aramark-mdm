package com.aramark.mdm.locn.scripttask;

import com.aramark.mdm.config.EBXAramarkEnvironment;
import com.onwbp.adaptation.Adaptation;
import com.onwbp.adaptation.AdaptationHome;
import com.onwbp.adaptation.AdaptationName;
import com.orchestranetworks.instance.HomeKey;
import com.orchestranetworks.instance.Repository;
import com.orchestranetworks.service.Archive;
import com.orchestranetworks.service.ArchiveExportSpec;
import com.orchestranetworks.service.OperationException;
import com.orchestranetworks.service.Procedure;
import com.orchestranetworks.service.ProcedureContext;
import com.orchestranetworks.service.ProgrammaticService;
import com.orchestranetworks.service.Session;
import com.orchestranetworks.workflow.ScriptTask;
import com.orchestranetworks.workflow.ScriptTaskContext;
public class BackupDataspaceLocation extends ScriptTask
{
	
	//final static Logger logger = Logger.getLogger(BackupDataspaceLocation.class);
	
	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException
	{
		final String dataSpaceName = "LocationMasterDataSpace"; 
		final String dataSetName = "LocationDataSet";        
				
		String serviceUser = EBXAramarkEnvironment.exbServiceUser;
		String servicePassword = EBXAramarkEnvironment.exbServicePassword;
		
		Repository repository = context.getRepository();		
		
		AdaptationHome dataSpace = repository.lookupHome(HomeKey.forBranchName(dataSpaceName)); 
		if (dataSpace == null)
		{
			throw OperationException.createError("Data space " + dataSpaceName + " does not exist.");
		}
		Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName(dataSetName));  
		if (dataSet == null)
		{
			throw OperationException.createError("Data set " + dataSetName
				+ " does not exist in the dataspace ." + dataSpaceName);
		}  
		
		final ArchiveExportSpec archiveExportSpec = new ArchiveExportSpec();
		/*  export file name will be the dataspace name.  It is intended to be a statically named file.  */
		
		String fileName = dataSpaceName + "_Backup.ebx"; 
		archiveExportSpec.setArchive(Archive.forFileInDefaultDirectory(fileName));
		archiveExportSpec.addInstance(AdaptationName.forName(dataSetName), true);
		Session session = repository.createSessionFromLoginPassword(serviceUser,servicePassword); 	
		
		ProgrammaticService.createForSession(session, dataSpace).execute(
			new Procedure()
			{
				@Override
				public void execute(final ProcedureContext procedureContext) throws Exception
				{
					procedureContext.doExportArchive(archiveExportSpec);
				}
			});
	}	

}
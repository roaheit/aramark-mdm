package com.aramark.mdm.locn.valuefunction;

import java.util.*;

import com.aramark.mdm.common.path.*;
import com.aramark.mdm.locn.path.*;
import com.onwbp.adaptation.*;
import com.orchestranetworks.ps.util.*;
import com.orchestranetworks.schema.*;

public class SiteApplicationRolesValueFunction implements ValueFunction
{

	@Override
	public Object getValue(Adaptation adaptation)
	{
		@SuppressWarnings("unchecked")
		List<List<String>> allowedApplicationRoleLists = (List<List<String>>) AdaptationUtil.getLinkedRecordList(
			adaptation,
			LocationPaths._Root_Site._SiteApplications,
			CommonReferencePaths._Root_Application.__applicationRoleList);

		return CollectionUtils.flatten(allowedApplicationRoleLists);
	}

	@Override
	public void setup(ValueFunctionContext arg0)
	{
		// TODO Auto-generated method stub

	}

}

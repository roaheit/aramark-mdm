package com.aramark.mdm.user.scripttask;

import com.onwbp.adaptation.*;
import com.orchestranetworks.instance.*;
import com.orchestranetworks.service.*;
import com.orchestranetworks.workflow.*;

public class BackupDataspaceUser extends ScriptTask {

	@Override
	public void executeScript(ScriptTaskContext context) throws OperationException
	{
		final String dataSpaceName = "UserMasterDataSpace"; 
		final String dataSetName = "UserDataSet";        
		
		Repository repository = context.getRepository();
			
		AdaptationHome dataSpace = repository.lookupHome(HomeKey.forBranchName(dataSpaceName)); 
		if (dataSpace == null)
		{
			throw OperationException.createError("Data space " + dataSpaceName + " does not exist.");
		}
		Adaptation dataSet = dataSpace.findAdaptationOrNull(AdaptationName.forName(dataSetName));  
		if (dataSet == null)
		{
			throw OperationException.createError("Data set " + dataSetName
				+ " does not exist in the dataspace ." + dataSpaceName);
		}  
		
		final ArchiveExportSpec archiveExportSpec = new ArchiveExportSpec();
		/*  export file name will be the dataspace name.  It is intended to be a statically named file.  */
		
		String fileName = dataSpaceName + "_Backup.ebx"; 
		archiveExportSpec.setArchive(Archive.forFileInDefaultDirectory(fileName));
		archiveExportSpec.addInstance(AdaptationName.forName(dataSetName), true);
		Session session = repository.createSessionFromLoginPassword("Svc_MDG", "KaleAndCollards"); 	
		
		ProgrammaticService.createForSession(session, dataSpace).execute(
			new Procedure()
			{
				@Override
				public void execute(final ProcedureContext procedureContext) throws Exception
				{
					procedureContext.doExportArchive(archiveExportSpec);
				}
			});
	}	
}

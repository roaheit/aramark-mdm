/*
 * Copyright Orchestra Networks 2000-2012. All rights reserved.
 */
package com.aramark.mdm.user.schemaextension;

import com.aramark.mdm.user.path.*;
import com.orchestranetworks.ps.accessrule.*;
import com.orchestranetworks.schema.*;

/**
 */
public class UserReferenceSchemaExtension implements SchemaExtensions
{
	@Override
	public void defineExtensions(SchemaExtensionsContext context)
	{
		AccessRulesManager manager = new AccessRulesManager(context);
		manager.setAccessRuleOnNodeAndAllDescendants(
			UserReferencePaths._Root,
			false,
			new WorkflowAccessRule());
	}
}

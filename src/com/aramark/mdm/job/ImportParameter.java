package com.aramark.mdm.job;

public class ImportParameter {
	
	public ImportParameter(String fileName, String schemaPath) {
		super();
		this.fileName = fileName;
		this.schemaPath = schemaPath;
	}

	private String fileName;

	private String schemaPath;
	
	public String getFileName() {
		return fileName;
	}
	
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
	public String getSchemaPath() {
		return schemaPath;
	}
	
	public void setSchemaPath(String schemaPath) {
		this.schemaPath = schemaPath;
	}
	
	

}
